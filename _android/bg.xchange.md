---
wsId: 
title: xChange.bg - Buy Bitcoin
altTitle: 
authors:
- danny
users: 5000
appId: bg.xchange
appCountry: 
released: 2020-12-22
updated: 2023-04-27
version: 2.2.1
stars: 
ratings: 
reviews: 
size: 
website: https://xchange.bg
repository: 
issue: 
icon: bg.xchange.png
bugbounty: 
meta: stale
verdict: nowallet
date: 2024-04-24
signer: 
reviewArchive: 
twitter: xchangebg
social:
- https://www.facebook.com/xchangebg
redirect_from: 
developerName: Global xChange LTD
features: 

---

## App Description

>  Buy, sell, and exchange Bitcoin and the most popular altcoins and stablecoins

## The App

The app only requires ID verification if you are converting from or to fiat currency. It is similar to:

{% include walletLink.html wallet='android/com.changelly.app' verdict='true' %}

The user has to input a receiving address and send coins to xchangebg's own address for conversions or swaps to happen. No funds are stored on the app.

## Verdict

This exchange is **not a wallet**.

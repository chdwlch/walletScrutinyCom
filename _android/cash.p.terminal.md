---
wsId: 
title: P.CASH
altTitle: 
authors:
- danny
users: 1000
appId: cash.p.terminal
appCountry: 
released: 2023-02-10
updated: 2024-01-28
version: 0.37.2
stars: 
ratings: 
reviews: 
size: 
website: https://p.cash/
repository: 
issue: 
icon: cash.p.terminal.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-12-02
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: PirateCash and Cosanta foundation
features: 

---

## App Description from Google Play

A powerful non-custodial multi-wallet for PirateCash, Cosanta, Bitcoin, Ethereum, Binance Smart Chain, Avalanche, Solana and other blockchains. Non-custodial crypto and NFT storage, onchain decentralized exchange, institutional grade analytics for cryptcurrency and NFT markets, extensive privacy controls and human oriented design.

## Analysis 

- A BTC wallet is available
- A 12-word mnemonic phrase is provided during startup. 
- We've verified their claim that this app is non-custodial. 
- The provider does not make any claims regarding source-availability
- A [search on GitHub](https://github.com/search?q=cash.p.terminal&type=code) for the app ID does not show any results.
---
wsId: 
title: Icynote
altTitle: 
authors:
- danny
users: 100
appId: ch.smd.icynote
appCountry: 
released: 2021-07-11
updated: 2023-09-22
version: 1.0.2
stars: 3.8
ratings: 
reviews: 
size: 
website: https://icynote.ch
repository: 
issue: 
icon: ch.smd.icynote.png
bugbounty: 
meta: ok
verdict: fewusers
date: 2023-09-24
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Martin Demierre
features: 

---

This is the companion app to the bearer token: {% include walletLink.html wallet='bearer/icynote' verdict='true' %}


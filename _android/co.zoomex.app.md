---
wsId: zoomexCrypto
title: ZOOMEX - Trade&Invest Bitcoin
altTitle: 
authors:
- danny
users: 500000
appId: co.zoomex.app
appCountry: 
released: 2021-12-14
updated: 2024-04-26
version: 3.7.4
stars: 3.2
ratings: 
reviews: 16
size: 
website: 
repository: 
issue: 
icon: co.zoomex.app.png
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-01
signer: 
reviewArchive: 
twitter: zoomexofficial
social:
- https://www.zoomex.com
- https://t.me/zoomex_com
redirect_from: 
developerName: Zoomex
features: 

---

## App Description from Google Play

> Zoomex is a cryptocurrency trading platform
>
> Zoomex supports a number of altcoin trading pairs, including Bitcoin/USDT, Ethereum/USDT
>
> Zoomex adapts multi-signature as a top security financial system together with the cold/hot wallet system to protect your crypto asset securely.

## Analysis

- As the description states, the platform is an exchange which supports BTC and employs cold-storage.
- This app is therefore, **custodial**.

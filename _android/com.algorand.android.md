---
wsId: 
title: Pera Algo Wallet
altTitle: 
authors: 
users: 100000
appId: com.algorand.android
appCountry: 
released: 2019-06-07
updated: 2024-04-05
version: 5.10.8
stars: 4.6
ratings: 14091
reviews: 1171
size: 
website: https://perawallet.app
repository: 
issue: 
icon: com.algorand.android.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2020-12-06
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from:
- /com.algorand.android/
developerName: Pera Wallet
features: 

---


---
wsId: ancryptoWallet
title: 'AnCrypto: Safest Web3 Wallet'
altTitle: 
authors:
- danny
users: 100000
appId: com.ancryptoWallet
appCountry: 
released: 2022-09-27
updated: 2024-04-03
version: 1.33.25
stars: 4.5
ratings: 
reviews: 27
size: 
website: https://www.ancrypto.io/
repository: 
issue: 
icon: com.ancryptoWallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-01
signer: 
reviewArchive: 
twitter: AnCryptoWallet
social:
- https://www.linkedin.com/company/ancrypto
- https://www.facebook.com/ancrypto.io
- https://t.me/+rLAv56hhJN1hZTM1
- https://www.instagram.com/ancrypto.io
- https://discord.com/invite/pN3NXfpJgF
- https://www.youtube.com/@ancryptowallet
redirect_from: 
developerName: AnCrypto
features: 

---

## App Description from Google Play

> AnCrypto is a non-custodial, multi-chain, free, secure and safest digital crypto wallet that enables you to buy, sell, store and swap cryptocurrency across the different blockchain networks. At present, AnCrypto supports 9 blockchains including Bitcoin, Ethereum, Binance, Polygon (Matic), Doge, Tezos, Litecoin, Bitcoin Cash Tron & others.

## Analysis

- We installed the app and it supports a Bech32 BTC wallet that can send and receive.
- The seed phrases were provided during startup.
- There are no claims that the project is source-available. A code search on GitHub [does not yield any results](https://github.com/search?q=com.ancryptoWallet&type=code).
- This app is **not source-available**.

---
wsId: 
title: Bitcoin Cash Register (BCH)
altTitle: 
authors: 
users: 100000
appId: com.bitcoin.merchant.app
appCountry: 
released: 2019-04-13
updated: 2023-01-24
version: 5.3.18
stars: 4.3
ratings: 504
reviews: 31
size: 
website: https://www.bitcoin.com/bitcoin-cash-register
repository: 
issue: 
icon: com.bitcoin.merchant.app.png
bugbounty: 
meta: stale
verdict: nowallet
date: 2024-01-19
signer: 
reviewArchive: 
twitter: bitcoincom
social:
- https://www.facebook.com/buy.bitcoin.news
- https://www.reddit.com/r/btc
redirect_from:
- /com.bitcoin.merchant.app/
- /posts/com.bitcoin.merchant.app/
developerName: Bitcoin.com
features: 

---

This is a watch-only wallet according to their description:

> Just enter either a standard Bitcoin Cash address or an “extended public key”
(aka an “xpub”) from your Bitcoin Cash wallet to start accepting instant and
secure Bitcoin Cash payments at your business.

As it doesn't manage private keys, you cannot spend with it and consequently
neither the provider can steal or lose your funds.

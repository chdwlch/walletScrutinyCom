---
wsId: blofin
title: BloFin
altTitle: 
authors:
- danny
users: 50000
appId: com.blofin.android
appCountry: 
released: 2022-05-13
updated: 2024-04-28
version: 3.1.4
stars: 3.7
ratings: 
reviews: 15
size: 
website: https://blofin.com
repository: 
issue: 
icon: com.blofin.android.png
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-03-29
signer: 
reviewArchive: 
twitter: Blofin_Official
social: 
redirect_from: 
developerName: Blofin Inc.
features: 

---

## [Terms](https://blofin.com/terms) 

> Blofin.com maintains full custody of the Digital Assets, funds and User data/information which may be turned over to governmental authorities in the event of Blofin.com Accounts’ suspension/closure arising from fraud investigations, investigations of violation of law or violation of these Terms.

## Analysis 

We installed the app and found that we could only withdraw or deposit USDT. However, since this is an exchange where it is possible to buy BTC/USDT, we are assuming that it is possible to 'hold bitcoin' in this custodial service. But cannot send and receive it, as it has to be converted back to USDT.
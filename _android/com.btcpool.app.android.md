---
wsId: BTCcomPool
title: BTC.com - Leading Mining Pool
altTitle: 
authors:
- danny
users: 50000
appId: com.btcpool.app.android
appCountry: us
released: 2020-02-25
updated: 2023-04-26
version: 2.3.2
stars: 3.9
ratings: 261
reviews: 21
size: 
website: https://btc.com
repository: 
issue: 
icon: com.btcpool.app.android.png
bugbounty: 
meta: stale
verdict: nowallet
date: 2024-04-21
signer: 
reviewArchive: 
twitter: btccom_official
social:
- https://www.linkedin.com/company/btc.com
- https://www.facebook.com/btccom
redirect_from: 
developerName: FINE BRAND LIMITED
features: 

---

## App Description

This is meant to be a tool for managing mining pools. The wallet is a seperate app.

> BTC Pool is coming! BTC pool is a whole new choice for miners! With much more stable architecture, much better user experience, much lower fees and much stronger service, you will never find that mining could be in this way!

## The Site

Their site [links](https://wallet.btc.com/#/setup/register) to this separate wallet app: {% include walletLink.html wallet='android/com.blocktrail.mywallet' verdict='true' %}

## Verdict

This specific app is meant as a tool for managing and/or monitoring mining pools. It is **not a bitcoin wallet.**

---
wsId: liquidityX
title: 'LiquidityX: CFD Online Trading'
altTitle: 
authors:
- danny
users: 10000
appId: com.capitalsecurities.liqx
appCountry: 
released: 
updated: 2024-04-03
version: 2.6.8
stars: 2.3
ratings: 
reviews: 
size: 
website: https://www.liquidityx.com/eu/
repository: 
issue: 
icon: com.capitalsecurities.liqx.png
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-05-13
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Capital Securities S.A.
features: 

---

## App Description from Google Play 

> 300+ Global Instruments from 6 asset classes to trade CFDs on (i.e: forex, commodities, cryptocurrencies, shares, indices and futures)

This is a cryptocurrency CFD trading app. 

## Analysis 

[Deposit](https://www.liquidityx.com/eu/faqs/deposit/) and [withdraw](https://www.liquidityx.com/eu/faqs/withdrawal/) options are confined to banking instruments and e-wallets such as neteller and skrill. This means that app **cannot send/receive Bitcoin.**
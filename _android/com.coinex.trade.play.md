---
wsId: CoinEx
title: 'CoinEx: Buy Bitcoin & Crypto'
altTitle: 
authors:
- leo
users: 1000000
appId: com.coinex.trade.play
appCountry: 
released: 2019-12-27
updated: 2024-03-24
version: 3.29.2
stars: 4.1
ratings: 60269
reviews: 1408
size: 
website: https://www.coinex.com/
repository: 
issue: 
icon: com.coinex.trade.play.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-04-15
signer: 
reviewArchive: 
twitter: coinexcom
social:
- https://www.facebook.com/TheCoinEx
- https://www.reddit.com/r/Coinex
redirect_from:
- /com.coinex.trade.play/
- /posts/com.coinex.trade.play/
developerName: CoinEx
features: 

---

The description on Google Play starts not so promising:

> Meet the top cryptocurrency trading app！

Trading apps are usually custodial. Unfortunately there is no easily accessible
information on their website about the app neither. For now we assume it is
custodial and thus **not verifiable**.

---
wsId: crypterium
title: 'Сhoise.com: Crypto Wallet. NFT'
altTitle: 
authors:
- leo
users: 500000
appId: com.crypterium
appCountry: 
released: 2018-01-21
updated: 2023-08-22
version: 3.7.0
stars: 3.6
ratings: 8728
reviews: 426
size: 
website: https://crypterium.com
repository: 
issue: 
icon: com.crypterium.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-20
signer: 
reviewArchive: 
twitter: crypterium
social:
- https://www.facebook.com/crypterium.org
redirect_from:
- /com.crypterium/
developerName: Crypterium AS
features: 

---

Judging by what we can find on the [wallet site](https://wallet.crypterium.com/):

> **Store**<br>
  keep your currencies<br>
  safe & fully insured

this is a custodial app as a self-custody wallet cannot ever have funds insured.

As a custodial app it is **not verifiable**.

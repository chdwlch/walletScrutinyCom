---
wsId: cryptoKara
title: CryptoKara
altTitle: 
authors:
- danny
users: 500000
appId: com.cryptokara
appCountry: 
released: 2021-09-28
updated: 2024-04-08
version: 1.7.5
stars: 4.3
ratings: 
reviews: 126
size: 
website: https://www.cryptokara.com/
repository: 
issue: 
icon: com.cryptokara.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-06-24
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: 'Eagle Network : Digital Currency For Phone'
features: 

---

This self-custodial app states that the private keys never leave the device. It also has a Dapp browser and can exchange multiple cryptocurrencies. Once the app is installed the user can either create a wallet or restore from a seed phrase. The seed phrases provided consists of 12 words. 

The listed website does not have a lot of content. However, we were able to find a secondary website related to the app. 

It is possible to export the private keys for individual cryptocurrency wallets. It will then display the keys in plain text. 

We were not able to find any links to a repository.

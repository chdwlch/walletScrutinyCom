---
wsId: HODLCryptoTracker
title: HODL Real-Time Crypto Tracker
altTitle: 
authors:
- danny
users: 100000
appId: com.flashratings.hodl
appCountry: 
released: 2018-07-24
updated: 2023-09-16
version: '9.11'
stars: 4.4
ratings: 
reviews: 469
size: 
website: http://www.hodlfinance.com
repository: 
issue: 
icon: com.flashratings.hodl.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2022-06-24
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: HODL Media Inc.
features: 

---

## App Description

This app is a tool for tracking crypto prices:

> Track your cryptocurrency in this crypto portfolio tracker & watch real-time crypto live charts & prices from all global exchanges in this crypto tracker app. Filtered tweets & breaking headlines from all top news sources. Simple & intuitive cryptocurrency tracker interface & all-in-one watchlist. Built for HODLERS, traders & cryptocurrency lovers.
>
> Use this cryptocurrency tracker for in-depth crypto chart analysis. Live coin watch 24/7 in the HODL app.

According to the website, it is also possible to sync your portfolio with exchanges or wallets.

This app is **not a wallet** and has no access to your private keys.


---
wsId: trustPoolFrostWallet
title: 'FROST Wallet: Buy BTC & Crypto'
altTitle: 
authors:
- danny
users: 5000
appId: com.frostwallet
appCountry: 
released: 2022-02-01
updated: 2024-04-17
version: 2.5.7
stars: 4.5
ratings: 
reviews: 
size: 
website: https://frostwallet.cc
repository: 
issue: 
icon: com.frostwallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-19
signer: 
reviewArchive: 
twitter: 
social:
- https://t.me/frost_wallet
- https://www.instagram.com/frostwallet.cc
redirect_from: 
developerName: Trustpool
features: 

---

## App Description from Google Play

> Frost Wallet is a secure, decentralised cryptocurrency wallet from the TrustPool mining pool team. The app is non-custodial - your data is stored on the device where the app is installed, not on a shared server.

## Analysis 

- There is an option to back up the private keys.
- A legacy BTC address is available with send/receive functions.
- We [did not find any results](https://github.com/search?q=com.frostwallet&type=code) when searching the code for the app on GitHub.
- There were no claims regarding source-availability
- This app is **not source-available.**
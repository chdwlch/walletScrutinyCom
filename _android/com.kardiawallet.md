---
wsId: kardiawallet
title: KardiaChain Wallet
altTitle: 
authors:
- danny
users: 100000
appId: com.kardiawallet
appCountry: vn
released: 2021-01-13
updated: 2023-03-20
version: 2.4.10
stars: 4.3
ratings: 1064
reviews: 125
size: 
website: https://kardiachain.io/
repository: 
issue: 
icon: com.kardiawallet.jpg
bugbounty: 
meta: stale
verdict: nobtc
date: 2024-03-15
signer: 
reviewArchive: 
twitter: KardiaChain
social:
- https://www.facebook.com/KardiaChainFoundation
redirect_from: 
developerName: Kardia
features: 

---

The KardiaChain wallet is specifically for use with the KAI token. 

From its [documentation](https://docs.kardiachain.io/docs/)

>KardiaChain is an accessible blockchain for millions and is a decentralized public blockchain which provides the dual Master node to facilitate inter-chain operations among both existing and upcoming blockchain platforms

We downloaded the app and verified that it **does not have a bitcoin wallet**. 


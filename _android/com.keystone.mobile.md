---
wsId: Keystone
title: Keystone Hardware Wallet
altTitle: 
authors:
- danny
users: 10000
appId: com.keystone.mobile
appCountry: 
released: 2021-06-01
updated: 2023-04-17
version: 1.3.3
stars: 3.4
ratings: 
reviews: 8
size: 
website: https://keyst.one/
repository: 
issue: 
icon: com.keystone.mobile.png
bugbounty: 
meta: stale
verdict: nosource
date: 2024-04-13
signer: 
reviewArchive: 
twitter: KeystoneWallet
social:
- https://github.com/KeystoneHQ
redirect_from: 
developerName: Yanssie HK Limited
features: 

---

**Update 2022-01-07**

It has been mentioned in [Issue 380](https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/380#note_799019002)

> Keystone requires signing an NDA for both the wallet app and the modified Android this app runs under. This product is clearly nosource.

**Previous Analysis**

Formerly known as Cobo Vault, this is the companion app to the hardware wallet {% include walletLink.html wallet='hardware/cobovaultessential' verdict='true' %}

---
wsId: makeDelta
title: 코인미어캣 - 보조지표 알림과 스크리너
altTitle: 
authors:
- danny
users: 10000
appId: com.makedelta.slying
appCountry: 
released: 2021-08-07
updated: 2024-01-22
version: 7.2.1
stars: 
ratings: 
reviews: 
size: 
website: https://traderkat.io/
repository: 
issue: 
icon: com.makedelta.slying.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-06-14
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/profile.php?id=100083754173968
- https://www.youtube.com/channel/UCLC_CKhMggklpoHowc6TvNA
redirect_from: 
developerName: MakeDelta
features: 

---

## App Description from Google Play 

> We will notify you of exchange announcements including Upbit, Bithumb, and Binance Futures.
> - Conditional search is also possible with coins.
- RSI and sub-indicator RBI notification are supported.
- Recommended if you are uncomfortable using TradingView Alert.
- You can respond to major indices, premiums, and market prices for each exchange.

## Analysis 

- The website is mostly in the Korean language
- With some difficulty, we weren't able to find any wallet icons or anything that resembles the bitcoin logo or denomination. It appears to be a trading-training app with the possibility of connecting to some exchanges via API. 
- This app **does not have a wallet**.
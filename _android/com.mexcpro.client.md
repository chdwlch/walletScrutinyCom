---
wsId: 
title: MEXC-Buy & Sell Bitcoin
altTitle: 
authors:
- danny
users: 1000000
appId: com.mexcpro.client
appCountry: 
released: 2020-07-23
updated: 2024-04-26
version: VARY
stars: 3.8
ratings: 12371
reviews: 729
size: 
website: https://www.mexc.com
repository: 
issue: 
icon: com.mexcpro.client.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive: 
twitter: MEXC_Global
social:
- https://www.linkedin.com/company/mexcglobal
- https://www.facebook.com/mexcglobal
- https://www.reddit.com/r/MXCexchange
redirect_from: 
developerName: MEXC
features: 

---

> As a world’s leading digital asset trading platform, MEXC Exchange is committed to providing users safer, smarter and more convenient digital-asset trading and management services.

So it's an exchange. There's no word on self-custody or wallets though, but these exchanges usually have 'wallets' where users can trade BTC.

Due to this being a trading platform, we can assume the wallets are **custodial** therefore **not verifiable.**

Their website wasn't linked, but clicking the privacy policy will redirect you to the MEXC homepage.

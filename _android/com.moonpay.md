---
wsId: moonPayBuyBitcoin
title: 'MoonPay: Buy Bitcoin, Ethereum'
altTitle: 
authors:
- danny
users: 500000
appId: com.moonpay
appCountry: 
released: 2023-04-18
updated: 2024-04-26
version: 1.14.28
stars: 4.1
ratings: 
reviews: 168
size: 
website: https://www.moonpay.com/
repository: 
issue: 
icon: com.moonpay.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-07-18
signer: 
reviewArchive: 
twitter: moonpay
social:
- https://www.linkedin.com/company/moonpay
- https://www.instagram.com/moonpay
- https://www.facebook.com/officialmoonpay
redirect_from: 
developerName: MoonPay
features: 

---

## App Description from Google Play

> - Manage all your wallets in one place with wallet integration and detailed purchase history.
> - Buy crypto in under a minute using payment methods like debit and credit cards, local bank transfer, Apple Pay, Google Pay, and more.

## Analysis

- We installed the app and buying Bitcoin involved putting in a separate bitcoin wallet address. 
- The other notice states that the bitcoin wallet is "coming soon". 
- Tentatively, while the bitcoin wallet is not yet available, this app **does not have a bitcoin wallet.**

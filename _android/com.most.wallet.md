---
wsId: 
title: MOST Wallet
altTitle: 
authors:
- danny
users: 10000
appId: com.most.wallet
appCountry: 
released: 2022-02-04
updated: 2024-04-27
version: 1.5.36
stars: 
ratings: 
reviews: 
size: 
website: https://scitechnologyinc.com/mostwallet
repository: 
issue: 
icon: com.most.wallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-04-28
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: SCI Technology Inc DE
features: 

---

## App Description 

> MOST Wallet - multiple currencies mobile app for
- Bitcoin
- Ethereum
- Binance Smart Chain
- Tron
- Lots of Tokens

## Analysis 

The app provided the seed phrases and had a BTC wallet that can send and receive. 

However, we did not find in its app description on Google Play and on its website any information pertinent to the availability of its source code. 

We were also not able to find any repository on GitHub containing the app ID of com.most.wallet.


---
wsId: mycelium
title: Mycelium Bitcoin Wallet
altTitle: 
authors:
- leo
users: 1000000
appId: com.mycelium.wallet
appCountry: 
released: 2013-07-01
updated: 2023-12-26
version: 3.16.2.0
stars: 3.7
ratings: 11650
reviews: 1155
size: 
website: https://wallet.mycelium.com
repository: https://github.com/mycelium-com/wallet-android
issue: 
icon: com.mycelium.wallet.jpg
bugbounty: 
meta: ok
verdict: reproducible
date: 2024-01-22
signer: b8e59d4a60b65290efb2716319e50b94e298d7a72c76c2119eb7d8d3afac302e
reviewArchive:
- date: 2023-09-24
  version: 3.16.1.0
  appHash: f03d86c61f6d4a1ba071cb509537f6e855045c58ab9e705a50b9b1cc182ed606
  gitRevision: 149983814cf885544c7eedc3435dbd842e36cf42
  verdict: reproducible
- date: 2023-04-27
  version: 3.16.0.16
  appHash: 2f40ae3f5aed3be3a4791664767d10118ee7b6fbc2033add96107e78a20a28b3
  gitRevision: 02efce0be192c630f747855adbd5b5f81661bf0a
  verdict: reproducible
- date: 2023-01-12
  version: 3.16.0.15
  appHash: 50534bbe29e778e6b8d0ade5cb2c2524c6333b27446bb06e1880771c6009ec99
  gitRevision: 48c4143403d94cf29a968f1510d714fd7c49efcf
  verdict: reproducible
- date: 2022-11-04
  version: 3.16.0.13
  appHash: 76a431f30c9257c478c6d1072aeaaaddc4807f2a9f9379791c768866ec4bdec7
  gitRevision: 06e9a98aaea55673fd8894e4bede8423461d6f0f
  verdict: nonverifiable
- date: 2022-05-08
  version: 3.14.1.0
  appHash: 51b5a576f1e7a7580e15a7e3fffe43d3920584b319e8cfe3ad7f7ba091056ffa
  gitRevision: 24c873b9ba2f55ad0ccf495f823a00d23ea09b19
  verdict: reproducible
- date: 2022-04-15
  version: 3.14.1.0
  appHash: a06c4711f34abf0d3fb3c9648f55e6879953b552cb08013c0eb5b738dd6e85ad
  gitRevision: b87bc633a27569f90a012614c792e1a3c6d400c6
  verdict: reproducible
- date: 2022-03-21
  version: 3.14.0.0
  appHash: c08c972527244350dfa2ed79db34d10dd64603798552b3f8f04b8ae5cded0de2
  gitRevision: eea48a17393717f715185a6874d3e9dc7ec7c0ed
  verdict: reproducible
- date: 2022-03-10
  version: 3.13.9.0
  appHash: 52a78db950f41fb53908b35fba38f3504e423559b3905108ef0b5bae600b92a1
  gitRevision: cf522558f6ac756a97d6c491488cbb13f4130a0e
  verdict: reproducible
- date: 2022-02-17
  version: 3.13.6.0
  appHash: cda50955219b1f09362870a25115722b947cadf90a0fb8ac74ec23d3270763fa
  gitRevision: a59c7684f6cb3813419147f40d8180fd461a27a9
  verdict: reproducible
- date: 2022-02-01
  version: 3.13.6.0
  appHash: 6c98fdf455ad08639581f856446f157905e96c9f87aa934dd9ddd3366919eb21
  gitRevision: 80c78c180314ea7425e28a952af6872f4882c4a8
  verdict: reproducible
- date: 2022-01-31
  version: 3.13.4.0
  appHash: 5f71ad4a3d25538b8a747b37f46d2a4bec6abd4e1df9f307adb147fb05ca7826
  gitRevision: c03fe880418fda2c8c53cfb86c4b7dfce970f562
  verdict: reproducible
- date: 2022-01-25
  version: 3.13.3.0
  appHash: 09505002bcbe9c8d82ec543e609017e7ab75ba0c377c0bb52c4c1ebda5678f63
  gitRevision: 7fb8a183fee60571885418fcdf05478698687b0f
  verdict: reproducible
- date: 2022-01-24
  version: 3.13.2.0
  appHash: 0ce3ebc68ab2bff0b0876b2488c8bd7781f0b8cdf370894321fefb36234ac71a
  gitRevision: 0c3b4e73edb87aa9f8f1d9b4bfd006d43588ad39
  verdict: reproducible
- date: 2021-12-20
  version: 3.12.8.0
  appHash: 1f2f2bc999e8e6dd89a4ff84ffe1d8526f46be585d53d66a7729b4f1591f78eb
  gitRevision: fad2993685cb2e02ee46ba54d5eeb2f1fab25967
  verdict: reproducible
- date: 2021-11-14
  version: 3.12.2.0
  appHash: 8ebca59a3b86d3364e83fa549b23b842c63bcf1bbade528849b21d40573f4f8a
  gitRevision: 
  verdict: reproducible
- date: 2021-11-14
  version: 3.12.2.0
  appHash: f4a163ba974e57d32345ef27261c159f6b396df4ea5a7cdf2c2ab9b0c49b5a4c
  gitRevision: 
  verdict: reproducible
- date: 2021-10-21
  version: 3.11.0.11
  appHash: 30c5903e364fa9b7f2fafd8f0e1332867025338209469e8a9b32e2f841c7e512
  gitRevision: 9a4ffd99428ebf9a8135f53771d4aa977bc9b837
  verdict: reproducible
- date: 2021-10-19
  version: 3.11.0.9
  appHash: 72cfd43e8c48800b2b9201c5461d870f0948999ace492199528dd07c96738b1b
  gitRevision: 80c668c4ff00a097e16d384c3fa8025418aa5f2c
  verdict: reproducible
- date: 2021-09-22
  version: 3.11.0.8
  appHash: b9128a48550a3aeff64b1aa7ad48b8b9a53877aab16f1129f0357f0c5ad267ee
  gitRevision: 9c3e5354adbf260f11e947f9231e2b24df32cbd6
  verdict: reproducible
- date: 2021-08-16
  version: 3.11.0.4
  appHash: 81417faf98334de319827d92bf0dafec6b43cfca7b2936876bdfa68e66445026
  gitRevision: 686ed96101658792fc253fb7e372c8d98a64034e
  verdict: reproducible
- date: 2021-07-09
  version: 3.10.0.3
  appHash: 7532f6d0cef440cfc3a09d48d8ef099a96c093f9895ad21aa069aa60be43a06d
  gitRevision: 4b5ca372266f768737229012a773b01c500285e2
  verdict: reproducible
- date: 2021-05-28
  version: 3.10.0.1
  appHash: d5c73ea4965e986101f751376f0aad74590b6ccb1a5623349dad03e1b6d5025e
  gitRevision: f10b413459affc777817652651e45a1ba439de71
  verdict: reproducible
- date: 2021-05-16
  version: 3.9.0.0
  appHash: bd8a7d6f8c27116fe1dbba0172864e95ae753f7403a147729920b816208f196a
  gitRevision: 20646878724bc765795bd9044303bf6e4a50a81f
  verdict: reproducible
- date: 2021-04-01
  version: 3.8.9.0
  appHash: 296a97308aa10a98bc678e9b0ffd94c7daf120c24fa07cdaa7ad9179ace7416c
  gitRevision: f05973b1d8059c86c43aba2366c824a6f3deadb3
  verdict: reproducible
- date: 2021-01-20
  version: 3.8.6.1
  appHash: 7184f13e4f45df3dadf4f3bd6c3a1f9cde0375dbd81f784535b4884f55101048
  gitRevision: 00d3b30ee0b435990d6608c7d224816146fd1ef3
  verdict: nonverifiable
- date: 2020-11-17
  version: 3.7.0.1
  appHash: f504ec63d60584a7d850dfe79ce586fd97a72a5c844967d83c642fb9e1bf0c0c
  gitRevision: 270462bfed32768ce4dcce4dd23d93e8588caf0f
  verdict: reproducible
- date: 2020-07-13
  version: 3.5.3.0
  appHash: 847f61d6d6f24a459cba720adf0d9e1fd052431a2b49565ca424c13860a29f59
  gitRevision: 2caf243a5a30d549f351017369501795825d9759
  verdict: reproducible
- date: 2020-07-13
  version: 3.5.2.0
  appHash: 4e5cd2d3d13a06e11a0d4ecf4c4528bfea8f232dc60a1a344124cedae86b8430
  gitRevision: 7d140e2e8f72e0fbe5a2bc325219432c4f9791e6
  verdict: reproducible
- date: 2020-07-13
  version: 3.5.1.0
  appHash: bd968366d76267434fa067d4ef44f1fd3efab6144858fe752873be697633502f
  gitRevision: a1d83d77193224c49e56828022767399d2de8968
  verdict: reproducible
- date: 2020-06-17
  version: 3.5.0.1
  appHash: 7fcf353ca112f61ebc16f4a5ab1243ffeb012a87cca8d143fb708bdf5f8de559
  gitRevision: dcfda3c88223331de00a6d87dcc43dc2be8240e9
  verdict: reproducible
- date: 2020-06-17
  version: 3.5.0.0
  appHash: e8279deb438df1dfac458a2808073aafd0d10f54b11bb4e867561e71dc852bcc
  gitRevision: 6f2c91e407487ce1b31a6096f1a57fd734c88f38
  verdict: reproducible
- date: 2020-06-17
  version: 3.4.0.3
  appHash: 275053f9910e1402eb63f105c5791d6cc1b3f2c28f281602da629b256ab115e6
  gitRevision: 79aea7a92b35c1344c4262b5496f2483425dc06b
  verdict: reproducible
- date: 2020-05-06
  version: 3.4.0.1
  appHash: 7c6bbc62fdd429b60ac0d1877201fb1f34287c176a84f7d130f466d3c4947777
  gitRevision: b5f335a161074d58b6b94aec9c3a0056de79cd50
  verdict: reproducible
- date: 2020-05-06
  version: 3.4.0.0
  appHash: 35d7eeafa87ce88d527c9a41865eaa4cdcd158be8ea190c84133fbb02bfb6c46
  gitRevision: ca8600b612168fbac4e5a6a297664e72c82fe0c6
  verdict: reproducible
- date: 2020-04-27
  version: 3.4.0.0
  appHash: 35d7eeafa87ce88d527c9a41865eaa4cdcd158be8ea190c84133fbb02bfb6c46
  gitRevision: 73e728868398ecc3c370d0d12ff00f24c6c97e30
  verdict: nonverifiable
- date: 2020-03-20
  version: 3.3.3.1
  appHash: f35760dbc40959142c98abf923e70681e75cd6644892be15a7c3d3a689e11af8
  gitRevision: a6b2771dbc314160ba304573fd0a6cc5d6d1ccb9
  verdict: reproducible
- date: 2020-03-20
  version: 3.3.2.1
  appHash: b47ea1f72443281aaf6405c52fac0c7747b064dae8f282307a5dfae737e6328b
  gitRevision: 53d2766a0d74bd19375c451e81584c85381a4435
  verdict: reproducible
- date: 2020-02-17
  version: 3.3.1.1
  appHash: 6549d022684cac8521ff4c97cdd205ef289c58b58a589e092f1a5439aaf06a59
  gitRevision: b15e44f34278affacdaefaa6bc77d65cb75fba95
  verdict: reproducible
- date: 2020-02-16
  version: 3.3.1.1
  appHash: 6549d022684cac8521ff4c97cdd205ef289c58b58a589e092f1a5439aaf06a59
  gitRevision: a1e65039b8ae5efa874a3179e979451fc8b83e44
  verdict: nonverifiable
- date: 2020-01-07
  version: 3.2.0.17
  appHash: d0e943c9a974ddcfeb96baa06483b92f22e24e8ed7acada169a17679fcf28ac4
  gitRevision: 26054c022045897e0458d6abe7983e8430c7ae80
  verdict: reproducible
- date: 2019-11-16
  version: 3.0.0.23
  appHash: 27e236a723598d058ee3e1c72d54c84983489c8e80f7edee3df52235a0231c8c
  gitRevision: 3d972d9773b0fd2fb1602d31117a50be01d48610
  verdict: reproducible
twitter: MyceliumCom
social:
- https://www.linkedin.com/company/mycelium
- https://www.facebook.com/myceliumcom
- https://www.reddit.com/r/mycelium
redirect_from:
- /mycelium/
- /com.mycelium.wallet/
- /posts/2019/11/mycelium/
- /posts/com.mycelium.wallet/
developerName: Mycelium Developers
features: 

---

Here we test if the latest version can be reproduced, following the known
procedure expressed in our {% include testScript.html %}:

```
===== Begin Results =====
appId:          com.mycelium.wallet
signer:         b8e59d4a60b65290efb2716319e50b94e298d7a72c76c2119eb7d8d3afac302e
apkVersionName: 3.16.2.0
apkVersionCode: 3160200
verdict:        reproducible
appHash:        14da64f846aa54d80deccb30cf09f3193c86e4e9e8962beb2e03547bbca392f3
commit:         c2476d95257924b4f2fdd8f25ca2d97e210950e9

Diff:
Files /tmp/fromPlay_com.mycelium.wallet_3160200/META-INF/CERT.RSA and /tmp/fromBuild_com.mycelium.wallet_3160200/META-INF/CERT.RSA differ
Files /tmp/fromPlay_com.mycelium.wallet_3160200/META-INF/CERT.SF and /tmp/fromBuild_com.mycelium.wallet_3160200/META-INF/CERT.SF differ

Revision, tag (and its signature):
object c2476d95257924b4f2fdd8f25ca2d97e210950e9
type commit
tag v3.16.2.0
tagger AlexanderPavlenko <AlexanderPavlenko@users.noreply.github.com> 1704627896 +0400

Mycelium Bitcoin Wallet v3.16.2.0

0bb6689d5452274c77bbe0ecd5e43cac5c7a5613f9391f7f3b7d2d1108a9a6dd71fb355442264283133db6a7f7a213f354f7d205fe720d31fcce44358d8da181  prodnet/release/mbw-prodnet-release.apk
040bd3ea260a4e10a3106a578c39ce2acbfa4e21f8c13e16cccdb206f080dc31640338ecd01501365de24a8e5b8e9446bcabb78535032836b3954c3a3da45cf5  btctestnet/release/mbw-btctestnet-release.apk
ceabe3cdab94db423538ea92f3906a11d5a8acd2ed327c6f1474443f2bd3401c711f1a77902166befff8ef5408d3f654df864ebaf12d650bdc16ae590da69789  prodnet/debug/mbw-prodnet-debug.apk
bd5483140ba50fb54cde81a2202ba0bb6d646a4fdaf36e1b6f2e1ce15454cbde8598bb488d3cc7268d79dbfafca1bb1785dc0e35ffd7f60a79a09a0e5c9c2383  btctestnet/debug/mbw-btctestnet-debug.apk
===== End Results =====
```

This version is **reproducible**.

A recording of the test:

{% include asciicast %}

**Disclaimer**: Authors of this project have contributed to Mycelium.

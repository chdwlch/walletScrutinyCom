---
wsId: mewEthereum
title: 'MEW crypto wallet: DeFi Web3'
altTitle: 
authors:
- danny
users: 1000000
appId: com.myetherwallet.mewwallet
appCountry: us
released: 2020-03-11
updated: 2024-03-18
version: 2.7.3
stars: 4.4
ratings: 7093
reviews: 597
size: 
website: http://mewwallet.com
repository: 
issue: 
icon: com.myetherwallet.mewwallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-02-05
signer: 
reviewArchive: 
twitter: myetherwallet
social:
- https://www.linkedin.com/company/myetherwallet
- https://www.facebook.com/MyEtherWallet
- https://www.reddit.com/r/MyEtherWallet
redirect_from: 
developerName: MyEtherWallet
features: 

---

Supports 3 chains: Ethereum, Binance and Polygon. 

Does not support BTC.
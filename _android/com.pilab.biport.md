---
wsId: 
title: Biport Multichain CryptoWallet
altTitle: 
authors:
- danny
users: 5000
appId: com.pilab.biport
appCountry: 
released: 2023-02-09
updated: 2023-11-08
version: '0.41'
stars: 4.2
ratings: 
reviews: 7
size: 
website: https://pilab.co
repository: 
issue: 
icon: com.pilab.biport.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-07
signer: 
reviewArchive: 
twitter: 
social:
- https://t.me/Bifrost_Global
- https://www.youtube.com/c/Bifrost_BFC
- https://medium.com/bifrost/how-to-buy-bfc-and-bifi-a21bcb9749a9
redirect_from: 
developerName: PiLab Technology
features: 

---

## App Description from Google Play

> Biport Wallet aims to be your go-to-app to access to everything about blockchain. Your crypto assets like Bitcoin(BTC), Ethereum(ETH), Dogecoin(DOGE) and many other ERC20, BEP20, and ERC721 tokens are managed in a single page so that you do not have to switch networks to keep track of your cryptocurrencies scattered far and wide across multiple chains. If you are into NFTs, your Bored Ape Club, Cryptopunk or the equivalent are safe with us on Biport Wallet.

## Analysis 

- There was a Bech32 bitcoin address that can send/receive
- The app provided the mnemonics during startup. The option to back up the private keys is also present in the security settings.
- We found a GitHub link from their main website pointing to their [organization](https://github.com/orgs/bifrost-platform/repositories?type=all) account.
- We searched the app ID, on GitHub Code, and found [0 results.](https://github.com/search?q=com.pilab.biport&type=code)
- This app is **not source-available**.
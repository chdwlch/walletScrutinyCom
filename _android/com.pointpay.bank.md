---
wsId: pointpay
title: 'PointPay: Blockchain Wallet'
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.pointpay.bank
appCountry: 
released: 2020-07-21
updated: 2024-04-11
version: 8.8.9
stars: 4.2
ratings: 3831
reviews: 135
size: 
website: https://pointpay.io/
repository: 
issue: 
icon: com.pointpay.bank.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-04-26
signer: 
reviewArchive: 
twitter: PointPay1
social:
- https://www.linkedin.com/company/pointpay
- https://www.facebook.com/PointPayLtd
- https://www.reddit.com/r/PointPay
redirect_from: 
developerName: Point Pay OÜ
features: 

---

The PointPay website has very little information about how they manage private keys of the user.
The only basic statement is...

> We use strong military-grade encryption to store private keys

we will have to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.


---
wsId: 
title: Flipster - Trade BTC & Crypto
altTitle: 
authors:
- danny
users: 100000
appId: com.prestolabs.android.prex
appCountry: 
released: 2022-11-18
updated: 2024-04-17
version: 1.49.8
stars: 3.9
ratings: 
reviews: 6
size: 
website: https://flipster.io
repository: 
issue: 
icon: com.prestolabs.android.prex.png
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-14
signer: 
reviewArchive: 
twitter: AQXofficial
social:
- https://www.linkedin.com/company/aqx-official
- https://www.instagram.com/aqx_official
- https://www.facebook.com/AQXofcl
- https://www.youtube.com/channel/UC_3J-wzFgDu2P8NF_CrjAYg/featured
- https://t.me/aqxannouncement
redirect_from: 
developerName: flipster
features: 

---

## App Description from Google Play 

> Trade fast and earn more with the AQX crypto trading app. Built from the ground up for mobile, an industry first in the derivatives trading space, the AQX app allows users to deposit and withdraw from multiple networks and trade with zero fees.

## Analysis 

- Section 11.8 of the [Terms](https://aqx.com/policies/terms) explicitly states that the service is a **custodial** service that keeps users' funds in their care.
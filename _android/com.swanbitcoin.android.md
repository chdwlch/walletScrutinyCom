---
wsId: swanBitcoin
title: 'Swan Bitcoin: Buy & Save'
altTitle: 
authors:
- danny
users: 50000
appId: com.swanbitcoin.android
appCountry: 
released: 2022-03-25
updated: 2024-04-10
version: 1.7.1
stars: 4.3
ratings: 
reviews: 123
size: 
website: https://www.swanbitcoin.com
repository: 
issue: 
icon: com.swanbitcoin.android.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-17
signer: 
reviewArchive: 
twitter: swan
social:
- https://www.instagram.com/swanbitcoin
- https://www.linkedin.com/company/swanbitcoin
redirect_from: 
developerName: Swan Bitcoin
features: 

---

## App Description from Google Play

> Buy Bitcoin anytime - purchases are instantly added to your Bitcoin balance.
>
> Create a Bitcoin savings plan for set-it-and-forget-it accumulation.

## Analysis

- [Custody](https://www.swanbitcoin.com/security/) is with Swan's partner, Fortress.
- This is a **custodial** provider.

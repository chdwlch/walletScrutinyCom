---
wsId: uFundFundRaising
title: 'UFUND: Fundraising, Investing'
altTitle: 
authors:
- danny
users: 1000
appId: com.ufund.app
appCountry: 
released: 2021-10-21
updated: 2024-04-22
version: 1.1.5
stars: 
ratings: 
reviews: 
size: 
website: https://web.ufund.online
repository: 
issue: 
icon: com.ufund.app.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-04-24
signer: 
reviewArchive: 
twitter: UFUND1
social:
- https://www.facebook.com/ufundrealbusiness
- https://www.youtube.com/channel/UCv9h2Dkr3ZJvETF9jI_1Kcg
- https://www.instagram.com/ufundonline
- https://www.linkedin.com/company/ufund1
redirect_from: 
developerName: Actiwires LLC
features: 

---

## App Description from Google Play 

> UFUND is an innovative investment app that allows users to have full control of their investments in real economy and campaigns in a secure and easy environment. It is the future of investment, e trade, and fundraising for small businesses and individuals.
>
> Your security is our priority. UFUND uses cutting-edge security measures to help protect investor assets and personal information. All funds are protected through advanced security and at no point does UFUND trade or exchange your information with anyone
>
> With our support, investors and campaigners will have the possibility to e trade tokens P2P and crypto using the UFUND app and Exchange solution. Even if you are an investor, you will be allowed to campaign on the app which in turn can help you earn on both sides.

## Analysis 

- We registered with the app using our Google sign-in. 
- We found what appears to be the "wallet", but it did not have a QR code and no BTC address was provided. We asked them on [twitter](https://twitter.com/BitcoinWalletz/status/1666357448704397312) about this. 
- Subsequently, since we did not find a BTC address that is generated, we also did not find any options to backup the private keys. 
- We also emailed their developer contact: info@ufund.online
- We're sort of in a quandary, since the app is meant to have a wallet, but in our case doesn't. It could be in the verge of folding up. It could be a bug. Or it could be that the app's functions aren't 100% provided to us. Nevertheless, while we wait for their reply, we'll mark this app as a **work-in-progress**.

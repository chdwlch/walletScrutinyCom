---
wsId: Pintu
title: 'Pintu: Buy & Invest Crypto'
altTitle: 
authors:
- danny
users: 1000000
appId: com.valar.pintu
appCountry: us
released: 2020-01-25
updated: 2024-04-25
version: 3.47.0
stars: 4.3
ratings: 28130
reviews: 149
size: 
website: https://pintu.co.id/
repository: 
issue: 
icon: com.valar.pintu.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive: 
twitter: pintuid
social:
- https://www.facebook.com/pintucrypto
redirect_from: 
developerName: PT. Pintu Kemana Saja
features: 

---

Found in the [FAQ:](https://pintu.co.id/en/faq/private-keys)
> **Do I hold a Private Key?**<br>
  Pintu is a custodial crypto exchange, which means Pintu acts as a custodian/keeper of the users’ private keys.  Therefore, Pintu doesn’t share private keys with its users.

Pintu is **custodial** and **not verifiable.**


---
wsId: yChanger
title: Ychanger – обмен криптовалют
altTitle: 
authors:
- danny
users: 1000
appId: com.xchange.ychanger
appCountry: 
released: 2021-06-08
updated: 2023-10-06
version: 4.0.1
stars: 4
ratings: 
reviews: 
size: 
website: https://ychanger.net/
repository: 
issue: 
icon: com.xchange.ychanger.png
bugbounty: 
meta: ok
verdict: custodial
date: 2023-10-06
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: XchangeCapitalGroup OU
features: 

---

## App Description from Google Play 

> You can buy Bitcoin, Ethereum, Monero, Tether, Zcash, Dogecoin, Dash, Litecoin, Bitcoin Cash, Ethereum Classic, as well as sell, quickly and easily using the Ychanger service application.

## Analysis 

- [Screenshots](https://twitter.com/BitcoinWalletz/status/1667475586548219906)
- [Section 3.3 of the Terms](https://ychanger.net/en/rules/) details all the ways the platform can exercise control over transactions, including freezing user funds and suspension. More telling is the ability of the provider to **block the user** from using the app. 
- We did not find the option to backup private keys 
- This is a **custodial service**.

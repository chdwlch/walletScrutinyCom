---
wsId: xfrXTrade
title: Xtrade - Online Trading
altTitle: 
authors:
- danny
users: 1000000
appId: com.xfr.xtrade
appCountry: 
released: 2014-10-02
updated: 2024-04-07
version: 6.1.0
stars: 4.3
ratings: 37358
reviews: 68
size: 
website: http://www.xtrade.com
repository: 
issue: 
icon: com.xfr.xtrade.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive: 
twitter: XTrade
social:
- https://www.linkedin.com/company/xtrade
- https://www.facebook.com/XTrade
redirect_from: 
developerName: XTRADE.com
features: 

---

> Enjoy the ultimate mobile trading experience when you access your Xtrade trading account from your phone

Like most trading platforms it seems to have a **custodial** wallet integrated. Therefore it is **not verifiable.**

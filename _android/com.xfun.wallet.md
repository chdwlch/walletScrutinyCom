---
wsId: xFunWallet
title: XFUN Wallet
altTitle: 
authors:
- danny
users: 10000
appId: com.xfun.wallet
appCountry: 
released: 2022-02-08
updated: 2023-07-17
version: 2.0.1
stars: 4.1
ratings: 
reviews: 9
size: 
website: https://xfun.io
repository: 
issue: 
icon: com.xfun.wallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-06-29
signer: 
reviewArchive: 
twitter: FUNtoken_io
social:
- https://t.me/officialFUNToken
- https://discord.com/invite/e7vfgKbEKU
redirect_from: 
developerName: Funtech Ltd
features: 

---

## App Description from Google Play

> The XFUN Wallet is a safe and easy-to-use cryptocurrency wallet for storing, sending, and receiving Bitcoin (BTC), FUN, XFUN, and other ERC-20 tokens on the Ethereum and Polygon networks.
>
> Because it is a non-custodial wallet, you have full control of your private key, and therefore your digital assets.

## Analysis

- The app claims to support BTC and is non-custodial.
- The app can send/receive BTC.
- There is a BTC wallet with a SegWit address that can send/receive.
- The app provided the seed phrases.
- There are no claims that the app is source-available.
- A [search on GitHub for the app ID](https://github.com/search?q=com.xfun.wallet&type=code), does not result in a relevant repository.
- This app is **not source-available**.

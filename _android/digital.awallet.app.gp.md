---
wsId: 
title: AWallet
altTitle: 
authors:
- danny
users: 1000
appId: digital.awallet.app.gp
appCountry: 
released: 2022-03-09
updated: 2023-08-24
version: 1.3.5
stars: 5
ratings: 
reviews: 15
size: 
website: https://awallet.digital
repository: 
issue: 
icon: digital.awallet.app.gp.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-06-12
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Metaverse Network LLC
features: 

---

## App Description from Google Play 

> AWallet is designed to allow users to manage digital encrypted assets easily and enjoy the blockchain world. it supports the mainstream public chains such as Bitcoin, Ethereum, Polkadot, BSC, Heco, OEC and their token assets. Hundreds of DApps such as BSC, Heco , OK, NFT, etc. have been launched on AWallet's DApp zone, covering many application scenarios such as DeFi, DEX, NFT, games, mining, and data, leading users to experience the latest DApps.

## Analysis 

- We installed the app
- We were given the mnemonic phrase and options to backup the private keys 
- There was a Bitcoin address that can send and receive. 
- There are **no claims or proofs that the project is source-available**. Searching on GitHub for the appID of this app, does not find any related repositories.
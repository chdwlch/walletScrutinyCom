---
wsId: kleverK5FinanceCrypto
title: 'Klever Wallet: Bitcoin, Crypto'
altTitle: 
authors:
- danny
users: 100000
appId: finance.klever.bitcoin.wallet
appCountry: 
released: 2023-01-16
updated: 2024-04-26
version: 5.31.4
stars: 4.4
ratings: 
reviews: 105
size: 
website: https://klever.org/
repository: 
issue: 
icon: finance.klever.bitcoin.wallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-11
signer: 
reviewArchive: 
twitter: klever_io
social:
- https://discord.gg/klever-io
- https://www.instagram.com/klever.io
- https://www.facebook.com/klever.io
redirect_from: 
developerName: 'Klever Wallet: Bitcoin, Ethereum, TRX & Crypto'
features: 

---

From the same developer of the apps:

- {% include walletLink.html wallet='android/cash.klever.blockchain.wallet' verdict='true' %}
- {% include walletLink.html wallet='iphone/cash.klever.blockchain.wallet' verdict='true' %}

## App Description from Google Play

> Klever Wallet is everything you need in a crypto wallet. Using our crypto wallet app, you have access to over 17 Blockchain networks and can store coins, tokens, and crypto assets such as Bitcoin (BTC), TRON (TRX), Ethereum (ETH), Klever (KLV), Binance Coin (BNB), Tether (USDT) and other top crypto assets safely and anonymously.
>
> Klever Wallet is built on Klever OS, which completely protects the user's private keys and makes sensitive data available only on the user's specific device using the latest state-of-the-art encryption technology.

## Analysis

- The first thing we did was to choose the option, 'Create Wallet'.
- We then assigned a 6-digit pin.
- We were provided with a 12-word mnemonic phrase.
- The app supports multiple cryptocurrencies, among them, BTC.
- We were provided with a Bech32 BTC address.
- There are no claims regarding source-availability and [0 results](https://github.com/search?q=finance.klever.bitcoin.wallet&type=code) when searching GitHub code for the app ID.
- This app is **not source-available**.

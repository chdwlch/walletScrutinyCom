---
wsId: adamanyMessenger
title: ADAMANT Messenger
altTitle: 
authors:
- danny
users: 10000
appId: im.adamant.adamantmessengerpwa
appCountry: 
released: 2020-06-13
updated: 2024-03-23
version: 4.6.2.0
stars: 4.2
ratings: 
reviews: 15
size: 
website: https://adamant.im
repository: 
issue: 
icon: im.adamant.adamantmessengerpwa.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-04-15
signer: 
reviewArchive: 
twitter: adamant_im
social:
- https://vk.com/adamant_im
- https://t.me/adamant_eng
- https://www.youtube.com/c/ADAMANTMessenger
redirect_from: 
developerName: ADAMANT Foundation
features: 

---

## App Description from [Google Play](https://play.google.com/store/apps/details?id=im.adamant.adamantmessengerpwa) 

> CRYPTO WALLET. Just a single password for all the internal cryptocurrencies: ADAMANT, Bitcoin, Ethereum, Doge, Dash, Binance coin, Bit-Z token, KuCoin token, Resfinex token, Stably Dollar. You have full control over private keys.

## Analysis 

The app has multiple features integrated in 1 app. It has messenger, wallet, a GPT powered chat and an exchange among other things. 

We took a look at its repository and found 21 of these component parts - however, the Android repository has notably been archived since 2021. 

This goes to say that while it may have been publicly available for a time, the Android app's **source code hasn't been for a long time**. What's noteworthy about this is that their Google Play app has recently been updated on March 2023.


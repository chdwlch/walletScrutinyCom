---
wsId: 
title: 'Status: Crypto Wallet, Messeng'
altTitle: 
authors:
- leo
users: 1000000
appId: im.status.ethereum
appCountry: 
released: 2020-02-05
updated: 2023-07-18
version: 1.20.4
stars: 4.3
ratings: 2069
reviews: 59
size: 
website: https://status.im
repository: 
issue: 
icon: im.status.ethereum.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-01-09
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Status Research and Development GmbH
features: 

---

This app appears to not support BTC.

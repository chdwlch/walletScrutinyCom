---
wsId: aquaWallet
title: AQUA Wallet
altTitle: 
authors:
- danny
users: 10000
appId: io.aquawallet.android
appCountry: 
released: 2024-01-02
updated: 2024-04-22
version: 0.1.30
stars: 4.5
ratings: 
reviews: 8
size: 
website: https://aquawallet.io
repository: https://github.com/AquaWallet/aqua-wallet
issue: https://github.com/AquaWallet/aqua-wallet/issues/9
icon: io.aquawallet.android.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-02-06
signer: 
reviewArchive: 
twitter: AquaBitcoin
social:
- https://www.instagram.com/aquabitcoin
- https://www.facebook.com/profile.php?id=100095180887605
- https://www.linkedin.com/products/jan3-aqua
redirect_from: 
developerName: JAN3
features:
- ln
- liquid

---

## App Description from Google Play

> ⚡️Native Bitcoin and Layer 2: Seamlessly transact in Bitcoin, Lightning, and Liquid assets including Tether USDt. AQUA is your passport to financial inclusion, designed for Latin America and embraced by Bitcoin Maximalists globally.
>
> 🔒 Non-Custodial Core: Own your keys and your assets. AQUA is built on the cornerstone principle of self-sovereignty, giving you control over your Bitcoin and your money as a whole.
>
> 🌊 Liquid Network Pioneer: AQUA was one of the first wallets to support Liquid sidechain functionality, enabling native support for Tether USDt and in the future, new assets like Bitcoin Bonds. 👀
>
> 💵 Tether USDt on Liquid: Seamlessly transact with USDt > - there is no second-best stablecoin. AQUA allows you to send and receive your USDt on Liquid as well as altcoin chains, all while storing your funds securely on the Liquid Network.
>
> 🤝 User-Friendly Interface: AQUA's intuitive design ensures a streamlined user experience, making it the ideal wallet for both beginners and Bitcoin enthusiasts. AQUA is the wallet you can orange pill your grandma with!
>
> 💧 Purified All-In-One: AQUA filters out the noise, holding only essential assets like Bitcoin and Tether, like an oasis of calm in the crypto jungle. Enjoy USDt without needing an altcoin for gas fees.

## Analysis 

The app provides a BTC address with seed phrases. It is now source-available,
and thus, now
**[for verification](https://github.com/AquaWallet/aqua-wallet/issues/9)**.

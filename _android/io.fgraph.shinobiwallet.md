---
wsId: shinobiWalletWeb3
title: Shinobi Wallet · DeFi & Web3
altTitle: 
authors:
- danny
users: 1000
appId: io.fgraph.shinobiwallet
appCountry: 
released: 2021-06-02
updated: 2024-03-02
version: 1.8.9
stars: 
ratings: 
reviews: 
size: 
website: 
repository: 
issue: 
icon: io.fgraph.shinobiwallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-15
signer: 
reviewArchive: 
twitter: shinobi_wallet
social:
- https://discord.com/invite/fg5fFwYYgt
- https://t.me/shinobiwallet
- https://www.instagram.com/shinobiwallet
redirect_from: 
developerName: FGRAPH OU
features: 

---

## App Description from Google Play

> Shinobi Wallet is the most secure, easy-to-use, multi-blockchain wallet. Simple yet powerful, Shinobi Wallet comes with all the DeFi functionalities you will ever need to manage your digital assets.
> 
> With Shinobi Wallet, our vision is to bring decentralized finance to everyone.
>
> Use Shinobi wallet, and avoid the frustration of other over-complicated tools, to:
> - Deposit, receive, send or swap ERC20, BEP20, and Polygon chain tokens
> - View your transaction history at a glance
> - Easily manage all your assets from one place, with our next-generation UX.
> - Stake and invest your assets
> - Buy cryptocurrencies with credit/debit cards
> - Browse, purchase and manage NFTs

## Analysis

- The seed phrases were provided
- We found a legacy BTC address that can send/receive.
- There are no claims whether the app is source-availble. 
- We searched for the app ID on GitHub Code, and found [18 results](https://github.com/search?q=io.fgraph.shinobiwallet&type=code). None of these included a possible match for the app's source.
- This app is **not source-available**.

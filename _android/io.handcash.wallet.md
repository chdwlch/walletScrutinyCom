---
wsId: 
title: HandCash
altTitle: 
authors: 
users: 100000
appId: io.handcash.wallet
appCountry: 
released: 2019-09-10
updated: 2024-03-22
version: 5.5.7
stars: 4.6
ratings: 581
reviews: 92
size: 
website: https://handcash.io
repository: 
issue: 
icon: io.handcash.wallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2019-12-28
signer: 
reviewArchive: 
twitter: handcashapp
social: 
redirect_from:
- /io.handcash.wallet/
- /posts/io.handcash.wallet/
developerName: Handcash Tech
features: 

---

A BSV wallet.

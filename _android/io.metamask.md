---
wsId: metamask
title: MetaMask - Blockchain Wallet
altTitle: 
authors:
- leo
users: 10000000
appId: io.metamask
appCountry: 
released: 2020-09-01
updated: 2024-04-19
version: v7.20.1
stars: 4.4
ratings: 57131
reviews: 4735
size: 
website: https://metamask.io
repository: 
issue: 
icon: io.metamask.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-05-01
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: MetaMask Web3 Wallet
features: 

---

This is an ETH-only app and thus not a Bitcoin wallet.

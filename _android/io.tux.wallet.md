---
wsId: tuxWallet
title: TUX Wallet
altTitle: 
authors:
- danny
users: 1000
appId: io.tux.wallet
appCountry: 
released: 2020-01-28
updated: 2024-04-28
version: 2.0.1
stars: 
ratings: 
reviews: 
size: 
website: https://coinyex.com/
repository: 
issue: 
icon: io.tux.wallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-18
signer: 
reviewArchive: 
twitter: Coinyexdotcom
social:
- https://t.me/coinyexchannel
redirect_from: 
developerName: Coinyex Co., Ltd.
features: 

---

## App Description from Google Play

> An ultra-secure multi-cryptocurrency wallet that supports BTC, ETH, XRP, ADA and ERC-20 tokens.

## Analysis

- We found a P2SH BTC address that can send/receive.
- There is an option to back up the private keys.
- We did not find any claims regarding source-availability. 
- We searched for the app ID in GitHub Code, but found [0 results](https://github.com/search?q=io.tux.wallet&type=code). There was 1 commit, but it did not lead to the source code for the Android app.
- This app is **not source-available**.
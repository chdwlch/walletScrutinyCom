---
wsId: wallacyCrypto
title: 'Wallacy: Crypto & BTC Wallet'
altTitle: 
authors:
- danny
users: 50000
appId: io.wallacy.cryptowallet
appCountry: 
released: 2023-07-19
updated: 2024-03-20
version: 0.1.13
stars: 4.7
ratings: 
reviews: 27
size: 
website: https://wallacy.io/
repository: 
issue: 
icon: io.wallacy.cryptowallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-08-30
signer: 
reviewArchive: 
twitter: WallacyWallet
social:
- https://www.facebook.com/Wallacy.io
- https://discord.com/invite/QpEHcR3DUW
- https://www.youtube.com/@WallacyWallet
- https://t.me/wallacywallet
redirect_from: 
developerName: Wallacy
features: 

---

## App Description from Google Play

> Wallacy is a hybrid and gamified crypto wallet that not only offers secure management of crypto assets but also delivers easy and enjoyable experiences to help users explore the potential of the crypto world.
>
> With Wallacy Crypto & Bitcoin wallet, you don't have to worry about the security of your crypto assets or deal with boring wallets. Our secure and private wallet application combines the advantages of Decentralized Exchange (DEX) and Centralized Exchange (CEX), providing you with a fully featured platform for managing your assets, participating in trading systems, and enjoying unique entertainment experiences.

## Analysis 

- Despite the app name and Google Play description, we were **not able to find a Bitcoin wallet**. This is a [screenshot](https://twitter.com/BitcoinWalletz/status/1696715546753171578) of the available networks for the app. We took a [video](https://twitter.com/BitcoinWalletz/status/1696719368510677464) and reached out to the provider.

We would welcome any input from the provider in the event that we are mistaken.
---
wsId: zebedee
title: 'ZBD: Bitcoin, Games, Rewards'
altTitle: 
authors:
- leo
users: 500000
appId: io.zebedee.wallet
appCountry: 
released: 2020-10-20
updated: 2024-04-25
version: 3.85.2
stars: 4.5
ratings: 358
reviews: 2216
size: 
website: https://zbd.one/help
repository: 
issue: 
icon: io.zebedee.wallet.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-04-12
signer: 
reviewArchive: 
twitter: zebedeeio
social: 
redirect_from: 
developerName: ZEBEDEE
features:
- ln

---

This app is very gamer focused and does no mention at all security aspects or
who's the custodian to your coins:

> The ZEBEDEE Wallet is basically everything you need to start playing games for
  Bitcoin, participating in Bitcoin-powered esports events or collecting Bitcoin
  tips on your live streams.

It is lightning network focused and apparently the counterpart for an sdk the
company is promoting for Bitcoin integration in games.

For lack of a better source I went on [their discord](https://zeb.gg/zebedeeiodiscord)
and asked, so ... according to JC on Discord, this app is custodial. As such it
is **not verifiable**.

---
wsId: vite
title: Vite Wallet
altTitle: 
authors:
- danny
users: 100000
appId: net.vite.wallet
appCountry: 
released: 2019-01-02
updated: 2022-09-13
version: v4.3.0
stars: 3.7
ratings: 
reviews: 77
size: 
website: https://vite.org/
repository: 
issue: 
icon: net.vite.wallet.png
bugbounty: 
meta: stale
verdict: nobtc
date: 2023-12-15
signer: 
reviewArchive: 
twitter: vitelabs
social:
- https://www.youtube.com/channel/UC8qft2rEzBnP9yJOGdsJBVg
redirect_from: 
developerName: Vite Labs
features: 

---

## App Description from Google Play

> Vite cross-chain wallet has switched into the Mainnet and now supported Vite coin and Vite tokens, ETH and ERC20 tokens, GRIN. Features of Vite wallet include:
>
> - Fully Control Private Keys and Wallet Access
> - Transact Rapidly Without Fees
> - View Real-Time Transaction Records
> - Multiple Coins and Tokens Supported
> - Dex included

## Analysis 

There is **no BTC support.**
---
wsId: 
title: Electron Cash wallet for BCH
altTitle: 
authors: 
users: 10000
appId: org.electroncash.wallet
appCountry: 
released: 2018-12-08
updated: 2023-03-21
version: 4.2.14-6
stars: 4.2
ratings: 191
reviews: 16
size: 
website: https://electroncash.org
repository: 
issue: 
icon: org.electroncash.wallet.png
bugbounty: 
meta: stale
verdict: nobtc
date: 2024-03-19
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from:
- /org.electroncash.wallet/
- /posts/org.electroncash.wallet/
developerName: XULU.TECH LLC
features: 

---


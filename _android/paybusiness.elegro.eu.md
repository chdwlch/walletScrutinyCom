---
wsId: payBusinessElegro
title: elegro Business online banking
altTitle: 
authors:
- danny
users: 1000
appId: paybusiness.elegro.eu
appCountry: 
released: 2020-07-15
updated: 2024-02-05
version: 1.3.4
stars: 
ratings: 
reviews: 
size: 
website: https://business.elegro.eu/elegro-business-wallet
repository: 
issue: 
icon: paybusiness.elegro.eu.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-06-12
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Niko Technologies
features: 

---

This app will be reviewed once it has a sufficient user base.

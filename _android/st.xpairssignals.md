---
wsId: 
title: Forex indicator signals trend
altTitle: 
authors:
- danny
users: 100000
appId: st.xpairssignals
appCountry: 
released: 2017-10-07
updated: 2023-08-29
version: '4.8'
stars: 4.2
ratings: 931
reviews: 36
size: 
website: 
repository: 
issue: 
icon: st.xpairssignals.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-08-29
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Massy Art
features: 

---

## App Description
From Google Play's description:

> Forex signals are providing 2000+ Different currency pairs from Forex and 4000+ from digital cryptocurrency. Our forex trading experts check each market trade opportunities and hands over to you with easy to watch trading signals.

It's an app that is meant to provide forex trading signals. It does not sound like a bitcoin wallet.


## The App and Verdict
Downloading the app, we see forex signals and no way to receive or send bitcoin. This confirms that the app is **not a wallet.**


---
wsId: 
title: Bitkit Wallet
altTitle: 
authors:
- danny
users: 1000
appId: to.synonym.bitkit.wallet
appCountry: 
released: 
updated: 2024-04-01
version: VARY
stars: 
ratings: 
reviews: 
size: 
website: https://bitkit.to
repository: https://github.com/synonymdev/bitkit
issue: https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/501
icon: to.synonym.bitkit.wallet.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-09-06
signer: 
reviewArchive: 
twitter: bitkitwallet
social:
- https://discord.com/invite/DxTBJXvJxn
- https://t.me/bitkitchat
- https://medium.com/synonym-to
- https://www.youtube.com/channel/UCyNruUjynpzvQXNTxbJBLmg
redirect_from: 
developerName: Synonym
features: 

---

## App Description from Google Play

> Bitkit hands you the keys to your money, profile, contacts,and web accounts.
>
> Pay anyone, anywhere, any timeand spend your Bitcoin on the things you value in life.
>
> Send Bitcoin faster than ever. Enjoy instant transactions with friends, family and merchants.
>
> Experience the web without limits: portable profiles & feeds, dynamic contacts, passwordless accounts.

## Analysis 

- The app has a Bech32 BTC address
- The private keys can be backed up in the settings.
- They have a link to their GitHub repository. 

This app is [**for verification**](https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/501)

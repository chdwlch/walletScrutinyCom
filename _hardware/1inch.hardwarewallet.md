---
title: 1inch Hardware Wallet
appId: 1inch.hardwarewallet
authors:
- danny
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions:
- 87
- 54
- 4
weight: 70
provider: Alexander Devyatkin, Mikhail Svetkin, Igor Zarochintsev, and others.
providerWebsite: 
website: https://hw.1inch.io/
shop: 
country: 
price: 
repository: https://github.com/HRDWLT
issue: 
icon: 1inch.hardwarewallet.png
bugbounty: 
meta: ok
verdict: unreleased
date: 2023-03-01
signer: 
reviewArchive:
- date: 2022-04-29
  version: 
  appHash: 
  gitRevision: 8762a3742
  verdict: unreleased
twitter: hrdwlt
social:
- https://medium.com/@hrdwlt
- https://www.linkedin.com/company/hwlt/
features: 

---

## Updated Review 2023-03-01

1inch has updated its website's design and listed some technical specifications. Below is a tweet from 1inch's social media account that suggests that the product is nearing an official release.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">If you&#39;re at <a href="https://twitter.com/EthereumDenver?ref_src=twsrc%5Etfw">@EthereumDenver</a>, catch us showing something 🤫 <a href="https://t.co/BBef8IhIj4">https://t.co/BBef8IhIj4</a></p>&mdash; 1inch Hardware Wallet @ ETH Denver (@hrdwlt) <a href="https://twitter.com/hrdwlt/status/1628708912034611202?ref_src=twsrc%5Etfw">February 23, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

## Previous Review 2022-04-29

## Product Description 

1inch's offical website states that it is a multi-seed wallet. Wallet set creation involves the usage of the HD Wallet algorithm along with BIP44.

Images of the product show that it will have a touchscreen display, QR code scanner, and the user will be asked to write down and save the generated seed phrase. Users will also be able to use NFC cards to sign transactions.

1inch has an official Github account so there is a chance that the firmware will be made available in the future. However, we cannot make a conclusive statement on reproducibility until the product is officially released. 
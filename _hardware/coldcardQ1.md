---
title: Coldcard Q
appId: coldcardQ1
authors:
- danny
released: 2022-10-05
discontinued: 
updated: 2024-04-02
version: v1.1.0Q
binaries: https://coldcard.com/downloads/
dimensions: 
weight: 
provider: Coinkite
providerWebsite: 
website: https://coinkite.com/
shop: https://store.coinkite.com/store/cc-q1
country: CA
price: 199USD
repository: https://github.com/Coldcard/firmware
issue: 
icon: coldcardQ1.png
bugbounty: 
meta: ok
verdict: unreleased
date: 2024-04-09
signer: 
reviewArchive: 
twitter: COLDCARDwallet
social:
- https://t.me/coldcard
features: 

---

## Product Description 

The ColdCard Q is the newest edition of Coinkite's hardware wallets. [On its website](https://coldcard.com/docs/coldcard-q/#unique-q-highlights), it advertises the addition of new features to its series that include:

- A QWERTY keyboard as opposed to its predecessors numeric keypad.
- A larger screen
- Dual MicroSD slots
- A QR scanner

While the interface has been updated, ColdCard Q's functionality shares similarities with Mk4 such as its CPU speed, BTC transaction limit, and memory size.

> All the features of the Mk4 work the same on the Q, except many are easier to access due to dedicated keys.


You can set a PIN code for the device and are offered four options in creating a Seed Phrase:

- 12 Words
- 24 Words
- 12 Word Dice Roll
- 24 Word Dice Roll

## Reproducibility 

ColdCard Q is still in a pre-order phase as the date of this review, as stated in this [pull request](https://github.com/Coldcard/firmware/pull/332).

> This PR will hold the Q related firmware changes until they are merged.
> 
> Coldcard Q is not yet shipping, but you can reserve your unit at our store.coinkite.com.
>
> Built and signed binaries for the lucky few who have test Q hardware can be found here: https://coldcard.com/downloads
>
> This branch will build both Mk4 and Q binaries with the same features (subject to hardware differences) but we have not yet tested the many changes on Mk4.

Currently, this product is **unreleased**.
---
title: Sentinel Wallet
appId: sentinel.wallet
authors:
- danny
released: 2022-11-27
discontinued: 
updated: 
version: 
binaries: 
dimensions: 
weight: 
provider: Sentinel Wallet LLC
providerWebsite: 
website: https://sentinelwallet.com
shop: 
country: US
price: 
repository: 
issue: 
icon: sentinel.wallet.png
bugbounty: 
meta: ok
verdict: unreleased
date: 2023-03-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 

---

## Product Description from the {{ page.title }} [Website](https://sentinelwallet.com/)

> All sensitive transactions, digital signing, and computations take place inside the powerful microprocessor contained within the wallet, separated from the outside world.
> 
> ...we enroll, store, and match the user’s biometric data solely within the Sentinel Wallet itself.
> 
> Keys never leave the Security Perimeter; Trusted Execution Environment is designed to use “safe-curves” ECC algorithms.

### Some Specifications 

- Illuminated OLED Screen
- Biometric Fingerprint Sensor
- Future Proof Crypto Algorithms
- CC EAL5+ Software Stack
- CC EAL6+ Secure Element

### From the {{ page.title }} [Press Release](https://sentinelwallet.com/sentry-enterprises-announces-the-worlds-first-security-lab-certified-biometric-cold-storage-crypto-wallet/)

> The Sentinel Wallet dwarfs existing wallet solutions, employing a biometrically protected smartcard (biometrics are enrolled, stored, and matched solely within the wallet’s security perimeter) making it purpose built for securing transactions while protecting the user’s privacy. All sensitive crypto operations, block signing, and data handling take place within a Secure Element inside a tamper-proof card, physically isolated from the outside world when not in use. Powered by proven hardware and a cutting-edge software stack with the most advanced crypto algorithms and “secure curves” the Sentinel Wallet delivers a uniquely attractive and highly secure end-to-end cryptocurrency solution.
> 
> In addition to the privacy and protection afforded by decentralized biometric authentication, the Sentinel Wallet offers true three-factor authentication between the user, the hardware wallet, and a mobile device.

## Analysis 

While the provider makes a lot of very interesting claims, a lot of information is actually not publicly available about the product. What we do know is that **Sentry Enterprises**, what we assume to be the parent company of Sentinel Wallet LLC, plans to sell the latter: 

> Sentry Enterprises intends to sell Sentinel Wallet LLC via a formal investment banking process in early 2023.  

Sentry Enterprises is a provider of Biometric authentication devices. 

We could not find any option to purchase the device, more detailed technical specifications, GitHub repositories or anything to indicate that the product is already commercially available. 

For this reason, we'll tag this product as **not yet released**.



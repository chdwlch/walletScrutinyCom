---
title: Stakwork Sphinx Key
appId: stakwork.sphinx.key
authors:
- danny
released: 2022-07-07
discontinued: 
updated: 2022-07-23
version: 0.2.0
binaries: 
dimensions: 
weight: 
provider: StakWork
providerWebsite: 
website: 
shop: 
country: 
price: 
repository: https://github.com/stakwork/sphinx-key/releases
issue: 
icon: 
bugbounty: 
meta: ok
verdict: unreleased
date: 2023-02-28
signer: 
reviewArchive: 
twitter: stakwork
social: 
features: 

---

## Product Description 

> A Lightning Hardware Wallet based on Validating Lightning Signer

The version 0.2.0 release offers a glimpse on the progress for the device: 

- validated lightning signing!
- SD card channel state persistence
- secure seed passing

## Analysis 

This product/device has **not yet been released** since there is no device image and pricing shown to the public yet.
---
wsId: NetDania
title: NetDania Stock & Forex Trader
altTitle: 
authors:
- danny
appId: 4KSDE4UX92.com.netdania.quotelist
appCountry: us
idd: 446371774
released: 2011-07-01
updated: 2024-04-17
version: 4.10.6
stars: 4.7
reviews: 12997
size: '83047424'
website: http://www.netdania.com
repository: 
issue: 
icon: 4KSDE4UX92.com.netdania.quotelist.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-10-10
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/NetDania-146001445410373
features: 
developerName: NetDania SRL

---

{% include copyFromAndroid.html %}

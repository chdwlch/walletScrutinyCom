---
wsId: BitWallet
title: BitWallet - Buy & Sell Bitcoin
altTitle: 
authors:
- leo
appId: Tissatech.Bit-Wallet
appCountry: 
idd: 1331439005
released: 2019-02-09
updated: 2022-12-27
version: 4.3.9
stars: 4.8
reviews: 956
size: '19835904'
website: 
repository: 
issue: 
icon: Tissatech.Bit-Wallet.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-12-28
signer: 
reviewArchive: 
twitter: bitwalletinc
social:
- https://www.facebook.com/BitWalletInc
features: 
developerName: BitWallet, Inc.

---

This appears to be primarily an exchange and as there are no claims of you being
in sole control of your funds, we have to assume it is a custodial service and
therefore **not verifiable**.

---
wsId: ECOS
title: 'ECOS: Bitcoin Mining & Wallet'
altTitle: 
authors:
- danny
appId: am.ecos.ios.production
appCountry: us
idd: 1528964374
released: 2020-11-25
updated: 2024-04-22
version: 1.45.0
stars: 3.6
reviews: 206
size: '106366976'
website: https://ecos.am/
repository: 
issue: 
icon: am.ecos.ios.production.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive: 
twitter: ecosmining
social:
- https://www.facebook.com/ecosdefi
features: 
developerName: Ecos am

---

{% include copyFromAndroid.html %}

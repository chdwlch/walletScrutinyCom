---
wsId: coinmetro
title: 'Coinmetro: Crypto Exchange'
altTitle: 
authors:
- danny
appId: app.coinmetro
appCountry: us
idd: 1397585225
released: 2018-07-25
updated: 2024-04-24
version: 5.50.765
stars: 4.4
reviews: 271
size: '55444480'
website: https://coinmetro.com/
repository: 
issue: 
icon: app.coinmetro.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: CoinMetro
social:
- https://www.linkedin.com/company/coinmetro
- https://www.facebook.com/CoinMetro
features: 
developerName: CoinMetro OU

---

{% include copyFromAndroid.html %}

---
wsId: ctpayWallet
title: CTPAY
altTitle: 
authors:
- danny
appId: app.ctmwallet
appCountry: us
idd: '1610293370'
released: 2022-02-28
updated: 2024-03-11
version: 1.0.12
stars: 0
reviews: 0
size: '28894208'
website: 
repository: 
issue: 
icon: app.ctmwallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-07-18
signer: 
reviewArchive: 
twitter: 
social:
- https://ctpay.io
features: 
developerName: CC Code Limited

---

{% include copyFromAndroid.html %}
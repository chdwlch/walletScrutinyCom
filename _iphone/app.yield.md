---
wsId: yieldApp
title: 'Yield App : Bitcoin Wallet'
altTitle: 
authors:
- danny
appId: app.yield
appCountry: ng
idd: '1591189242'
released: 2022-06-15
updated: 2024-04-11
version: '24.0411'
stars: 1
reviews: 1
size: '61825024'
website: https://yield.app/
repository: 
issue: 
icon: app.yield.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-05-03
signer: 
reviewArchive: 
twitter: yieldapp
social:
- https://www.linkedin.com/company/yieldapp/
- https://t.me/yieldapp
- https://discord.com/invite/crwD3JgMgn
features: 
developerName: Yield App

---

{% include copyFromAndroid.html %}
---
wsId: BigONE
title: BigONE
altTitle: 
authors: 
appId: base.big.one
appCountry: us
idd: 1485385044
released: 2019-11-06
updated: 2024-04-02
version: 2.4.511
stars: 4.4
reviews: 109
size: '184654848'
website: https://big.one
repository: 
issue: 
icon: base.big.one.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive: 
twitter: BigONEexchange
social:
- https://www.facebook.com/exBigONE
- https://www.reddit.com/r/BigONEExchange
features: 
developerName: Base Investing Corporation

---

 {% include copyFromAndroid.html %}

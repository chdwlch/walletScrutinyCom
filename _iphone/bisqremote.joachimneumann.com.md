---
wsId: bisqMobileNotifications
title: 'Bisq: Mobile Notifications'
altTitle: 
authors:
- danny
appId: bisqremote.joachimneumann.com
appCountry: us
idd: '1424420411'
released: 2018-09-09
updated: 2022-05-13
version: '1.5'
stars: 4.4
reviews: 16
size: '2153472'
website: https://bisq.network
repository: https://github.com/bisq-network/bisqremote_Android
issue: 
icon: bisqremote.joachimneumann.com.jpg
bugbounty: 
meta: stale
verdict: nowallet
date: 2023-05-10
signer: 
reviewArchive: 
twitter: devinbileck
social: 
features: 
developerName: Devin Bileck

---

{% include copyFromAndroid.html %}


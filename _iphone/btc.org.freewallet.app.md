---
wsId: btcFreeWallet
title: BTC Coin Wallet - Freewallet
altTitle: 
authors:
- danny
appId: btc.org.freewallet.app
appCountry: us
idd: '1126844329'
released: 2016-07-08
updated: 2022-01-14
version: 2.6.23
stars: 4
reviews: 111
size: '18610176'
website: https://freewallet.org/btc-wallet
repository: 
issue: 
icon: btc.org.freewallet.app.jpg
bugbounty: 
meta: obsolete
verdict: custodial
date: 2024-01-05
signer: 
reviewArchive: 
twitter: freewalletorg
social:
- https://www.facebook.com/freewallet.org
- https://www.reddit.com/r/Freewallet_org
features: 
developerName: Freewallet

---

{% include copyFromAndroid.html %}

---
wsId: relaiBuyBitcoin
title: 'Relai: Buy Bitcoin Easily'
altTitle: 
authors:
- danny
appId: ch.relai.relai
appCountry: ch
idd: '1513185997'
released: 2020-07-01
updated: 2024-04-25
version: 2.8.5
stars: 4.6
reviews: 1436
size: '75639808'
website: https://relai.app/
repository: 
issue: 
icon: ch.relai.relai.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-11
signer: 
reviewArchive: 
twitter: Relai_app
social:
- https://www.linkedin.com/company/relai-app
- https://www.youtube.com/channel/UCBtN1U9Aa7KgQeS_gJicUBw
- https://t.me/relai_en
- https://www.instagram.com/relai.app
features: 
developerName: Relai App GmbH

---

{% include copyFromAndroid.html %}

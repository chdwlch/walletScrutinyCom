---
wsId: swissBitcoinPay
title: Swiss Bitcoin Pay
altTitle: 
authors:
- danny
appId: ch.swissbitcoinpay.checkout
appCountry: us
idd: '6444370155'
released: 2022-11-19
updated: 2024-04-26
version: 2.0.11
stars: 0
reviews: 0
size: '30381056'
website: https://swiss-bitcoin-pay.ch
repository: https://github.com/SwissBitcoinPay/app
issue: 
icon: ch.swissbitcoinpay.checkout.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2024-02-28
signer: 
reviewArchive: 
twitter: SwissBitcoinPay
social:
- https://www.linkedin.com/company/swiss-bitcoin-pay
- https://www.youtube.com/@swissbitcoinpay
features: 
developerName: Swiss Bitcoin Pay Sarl

---

{% include copyFromAndroid.html %}
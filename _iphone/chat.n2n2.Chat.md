---
wsId: zionApp
title: Zion - Create Openly
altTitle: 
authors:
- danny
appId: chat.n2n2.Chat
appCountry: us
idd: '1556918256'
released: 2021-06-03
updated: 2023-07-20
version: 2.0.6
stars: 3
reviews: 16
size: '63107072'
website: 
repository: 
issue: 
icon: chat.n2n2.Chat.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-29
signer: 
reviewArchive: 
twitter: get_zion
social:
- https://www.instagram.com/get_zion
features: 
developerName: Modern Foundry, Inc.

---

{% include copyFromAndroid.html %}
---
wsId: phoenix
title: Phoenix Wallet
altTitle: 
authors:
- leo
- danny
appId: co.acinq.phoenix
appCountry: us
idd: 1544097028
released: 2021-07-13
updated: 2024-04-15
version: 2.2.4
stars: 4.4
reviews: 52
size: '66202624'
website: https://phoenix.acinq.co
repository: https://github.com/ACINQ/phoenix-kmm
issue: https://github.com/ACINQ/phoenix/issues/112
icon: co.acinq.phoenix.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2023-12-30
signer: 
reviewArchive: 
twitter: PhoenixWallet
social: 
features:
- ln
developerName: ACINQ

---

{% include copyFromAndroid.html %}

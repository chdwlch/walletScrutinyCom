---
wsId: bacoorhbwallet
title: Ethereum Wallet - HB Wallet
altTitle: 
authors:
- danny
appId: co.bacoor.ios.hbwallet
appCountry: us
idd: 1273639572
released: 2017-08-23
updated: 2022-04-08
version: 3.6.0
stars: 4.6
reviews: 258
size: '81591296'
website: https://www.bacoor.io
repository: 
issue: 
icon: co.bacoor.ios.hbwallet.jpg
bugbounty: 
meta: obsolete
verdict: nobtc
date: 2024-03-30
signer: 
reviewArchive: 
twitter: HBWallet_Ether
social: 
features: 
developerName: bacoor Inc.

---

{% include copyFromAndroid.html %}
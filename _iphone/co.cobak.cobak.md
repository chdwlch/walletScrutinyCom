---
wsId: cobak
title: 코박
altTitle: 
authors:
- danny
appId: co.cobak.cobak
appCountry: us
idd: 1350473579
released: 2018-03-13
updated: 2024-04-24
version: 1.8.28
stars: 4.9
reviews: 17
size: '97539072'
website: https://cobak.co.kr
repository: 
issue: 
icon: co.cobak.cobak.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive: 
twitter: CobakOfficial
social:
- https://www.linkedin.com/company/cobak
- https://www.facebook.com/coindaebak
features: 
developerName: COBAK CO., LTD.

---

{% include copyFromAndroid.html %}

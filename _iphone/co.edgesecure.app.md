---
wsId: edge
title: Edge - Crypto & Bitcoin Wallet
altTitle: 
authors:
- leo
- emanuel
appId: co.edgesecure.app
appCountry: 
idd: 1344400091
released: 2018-02-09
updated: 2024-04-24
version: 4.5.0
stars: 4.4
reviews: 1448
size: '88794112'
website: https://edge.app
repository: https://github.com/EdgeApp/edge-react-gui
issue: 
icon: co.edgesecure.app.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2022-03-13
signer: 
reviewArchive:
- date: 2019-11-10
  version: 1.10.1
  appHash: 
  gitRevision: 1707808e9efc2ab4ea3a03510ebd408811586d47
  verdict: ftbfs
twitter: edgewallet
social:
- https://www.linkedin.com/company/edgeapp
- https://www.reddit.com/r/EdgeWallet
features: 
developerName: Airbitz Inc

---

{% include copyFromAndroid.html %}

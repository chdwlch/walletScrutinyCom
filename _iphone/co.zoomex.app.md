---
wsId: zoomexCrypto
title: ZOOMEX - Trade&Invest Bitcoin
altTitle: 
authors:
- danny
appId: co.zoomex.app
appCountry: us
idd: '1601766234'
released: 2022-03-07
updated: 2024-04-27
version: 3.7.4
stars: 3.8
reviews: 12
size: '85894144'
website: 
repository: 
issue: 
icon: co.zoomex.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-01
signer: 
reviewArchive: 
twitter: zoomexofficial
social:
- https://www.zoomex.com
- https://t.me/zoomex_com
features: 
developerName: Octochain Fintech Limited

---

{% include copyFromAndroid.html %}

---
wsId: cobowallet
title: 'Cobo Crypto Wallet: BTC & DASH'
altTitle: 
authors:
- leo
appId: cobo.wallet
appCountry: 
idd: 1406282615
released: 2018-08-05
updated: 2024-03-07
version: 5.19.11
stars: 2.8
reviews: 6
size: '93387776'
website: https://cobo.com
repository: 
issue: 
icon: cobo.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: Cobo_Wallet
social:
- https://www.linkedin.com/company/coboofficial
- https://www.facebook.com/coboOfficial
features: 
developerName: Cobo Global Limited

---

 {% include copyFromAndroid.html %}

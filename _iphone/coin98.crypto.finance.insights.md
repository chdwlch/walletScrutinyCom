---
wsId: coin98
title: Coin98 Super Wallet
altTitle: 
authors:
- danny
appId: coin98.crypto.finance.insights
appCountry: us
idd: 1561969966
released: 2021-05-14
updated: 2024-04-16
version: 14.6.4
stars: 4.5
reviews: 629
size: '210327552'
website: https://coin98.com
repository: 
issue: 
icon: coin98.crypto.finance.insights.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-10
signer: 
reviewArchive: 
twitter: coin98_wallet
social:
- https://www.facebook.com/Coin98Wallet
features: 
developerName: COIN98 WALLET LTD

---

{% include copyFromAndroid.html %}
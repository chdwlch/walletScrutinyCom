---
wsId: Skilling
title: 'Skilling: Invest Forex & Trade'
altTitle: 
authors:
- danny
appId: com.FinovelCyprusLimited.Skilling
appCountry: gb
idd: 1441386723
released: 2019-05-30
updated: 2023-10-09
version: 2.2.1
stars: 4.9
reviews: 12
size: '20263936'
website: https://skilling.com
repository: 
issue: 
icon: com.FinovelCyprusLimited.Skilling.jpg
bugbounty: 
meta: removed
verdict: nosendreceive
date: 2023-12-19
signer: 
reviewArchive: 
twitter: SkillingTrading
social:
- https://www.linkedin.com/company/skilling
- https://www.facebook.com/SkillingTrading
features: 
developerName: Skilling Limited

---

{% include copyFromAndroid.html %}

---
wsId: LBank
title: LBank - Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.LBank.LBankNavApp
appCountry: us
idd: 1437346368
released: 2019-02-22
updated: 2023-08-19
version: 5.0.1
stars: 3.6
reviews: 473
size: '87407616'
website: https://www.lbank.com/
repository: 
issue: 
icon: com.LBank.LBankNavApp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: LBank_Exchange
social:
- https://www.linkedin.com/company/lbank
- https://www.facebook.com/LBank.info
features: 
developerName: LBANK EXCHANGE UAB

---

{% include copyFromAndroid.html %}

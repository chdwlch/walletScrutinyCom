---
wsId: SmartIT
title: Smart IT | Bitcoin Hosting
altTitle: 
authors:
- danny
appId: com.SmartItApp
appCountry: us
idd: 1526092476
released: 2020-10-28
updated: 2024-03-11
version: 1.0.8
stars: 0
reviews: 0
size: '21294080'
website: https://smartit.shop/
repository: 
issue: 
icon: com.SmartItApp.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2024-03-12
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: SmartItApp

---

{% include copyFromAndroid.html %}
---
wsId: metaOneNFT
title: 'Saakuru: All-in-one Crypto App'
altTitle: 
authors:
- danny
appId: com.aag.metaone
appCountry: us
idd: '1627212812'
released: 2023-01-03
updated: 2024-04-18
version: 5.4.0
stars: 4.9
reviews: 76
size: '57581568'
website: https://app.saakuru.com/
repository: 
issue: 
icon: com.aag.metaone.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-02
signer: 
reviewArchive: 
twitter: aag_ventures
social:
- https://www.linkedin.com/company/aag-ventures
- https://blog.aag.ventures
- https://t.me/aagventures
- https://www.linkedin.com/company/aag-ventures
- https://www.facebook.com/aagventures
- https://discord.com/invite/aagventures
features: 
developerName: ACHIP & ACHAIR GUILD VENTURES PTE. LTD.

---

{% include copyFromAndroid.html %}

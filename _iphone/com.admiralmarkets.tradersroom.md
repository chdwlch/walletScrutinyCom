---
wsId: AdmiralMarkets
title: Trading・Admirals stocks trade
altTitle: 
authors:
- danny
appId: com.admiralmarkets.tradersroom
appCountry: us
idd: 1222861799
released: 2017-06-28
updated: 2023-07-21
version: 5.18.0
stars: 4.5
reviews: 26
size: '98234368'
website: https://admiralmarkets.com/
repository: 
issue: 
icon: com.admiralmarkets.tradersroom.jpg
bugbounty: 
meta: removed
verdict: nosendreceive
date: 2023-08-25
signer: 
reviewArchive: 
twitter: AdmiralsGlobal
social:
- https://www.linkedin.com/company/-admiral-markets-group
- https://www.facebook.com/AdmiralsGlobal
features: 
developerName: Admiral Markets AS

---

{% include copyFromAndroid.html %}


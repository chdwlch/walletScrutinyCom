---
wsId: AladdinPro
title: Aladdin Pro
altTitle: 
authors:
- danny
appId: com.aladdin.pro
appCountry: us
idd: '1481967751'
released: 2019-11-22
updated: 2023-02-07
version: 1.15.7
stars: 4
reviews: 21
size: '24444928'
website: https://abbccoin.com
repository: 
issue: 
icon: com.aladdin.pro.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2024-02-05
signer: 
reviewArchive: 
twitter: abbcfoundation
social:
- https://www.linkedin.com/company/abbcfoundation
- https://www.facebook.com/abbcfoundation
features: 
developerName: MC ABBC IT SOLUTION

---

{% include copyFromAndroid.html %}
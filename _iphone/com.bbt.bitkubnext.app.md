---
wsId: bitkubNext
title: Bitkub NEXT
altTitle: 
authors:
- danny
appId: com.bbt.bitkubnext.app
appCountry: th
idd: '6444399387'
released: 2022-12-07
updated: 2024-04-25
version: 2.2.1
stars: 4.9
reviews: 233
size: '121300992'
website: https://www.bitkubchain.com
repository: 
issue: 
icon: com.bbt.bitkubnext.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-03-29
signer: 
reviewArchive: 
twitter: bitkubchain
social:
- https://www.facebook.com/bitkubchainofficial
features: 
developerName: Bitkub Blockchain Technology Co., Ltd.

---

{% include copyFromAndroid.html %}

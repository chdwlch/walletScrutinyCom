---
wsId: tapbitCrypto
title: Tapbit
altTitle: 
authors:
- danny
appId: com.billance.cn
appCountry: us
idd: '1610497530'
released: 2022-03-01
updated: 2024-04-26
version: 3.5.7
stars: 4.4
reviews: 15
size: '71102464'
website: 
repository: 
issue: 
icon: com.billance.cn.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-01
signer: 
reviewArchive: 
twitter: tapbitglobal
social:
- https://www.tapbit.com
- https://www.linkedin.com/company/tapbit
- https://www.facebook.com/Tapbitglobal
- https://www.reddit.com/user/tapbit
- https://www.youtube.com/c/Tapbitglobal
features: 
developerName: Tapbit LLC

---

{% include copyFromAndroid.html %}

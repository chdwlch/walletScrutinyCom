---
wsId: ceffuCustody
title: 'Ceffu: Institutional Custody'
altTitle: 
authors:
- danny
appId: com.binance.custody
appCountry: sg
idd: '1595828184'
released: 2021-12-01
updated: 2024-04-23
version: 3.8.1
stars: 4.5
reviews: 2
size: '105731072'
website: https://www.ceffu.com
repository: 
issue: 
icon: com.binance.custody.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-21
signer: 
reviewArchive: 
twitter: CeffuGlobal
social:
- https://www.linkedin.com/company/ceffu
features: 
developerName: Block Custody

---

{% include copyFromAndroid.html %}
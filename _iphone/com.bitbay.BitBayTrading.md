---
wsId: bitpaytrading
title: zondacrypto - crypto exchange
altTitle: 
authors:
- leo
appId: com.bitbay.BitBayTrading
appCountry: 
idd: 1409644952
released: 2018-11-20
updated: 2023-11-07
version: 1.4.7
stars: 3.8
reviews: 28
size: '60778496'
website: https://zondacrypto.com
repository: 
issue: 
icon: com.bitbay.BitBayTrading.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-12-19
signer: 
reviewArchive: 
twitter: BitBay
social:
- https://www.linkedin.com/company/bitbay
- https://www.facebook.com/BitBay
- https://www.reddit.com/r/BitBayExchange
features: 
developerName: BitBay Sp. z o.o.

---

This app's description loses no word on who holds the keys to your coins. Their
website is mainly about the exchange and not about the mobile appp but there is
[a site about that](https://bitbay.net/en/mobile), too. There they only talk
about exchange features, too and lose no word about who holds the keys which
probably means this app is a custodial offering and therefore **not verifiable**.

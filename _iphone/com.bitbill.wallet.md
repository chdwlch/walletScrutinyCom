---
wsId: ownbit
title: 'Ownbit: Cold & MultiSig Wallet'
altTitle: 
authors:
- leo
appId: com.bitbill.wallet
appCountry: 
idd: 1321798216
released: 2018-02-07
updated: 2024-04-21
version: 4.48.5
stars: 4.2
reviews: 51
size: '121103360'
website: http://www.bitbill.com
repository: 
issue: 
icon: com.bitbill.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: BITBILL PTY LTD

---

{% include copyFromAndroid.html %}

---
wsId: bitcoli
title: BitcoLi Lightning wallet
altTitle: 
authors:
- danny
appId: com.bitcoli.bitcoli
appCountry: us
idd: '6473613857'
released: 2024-01-10
updated: 2024-04-09
version: 1.0.28
stars: 0
reviews: 0
size: '40611840'
website: https://bitcoli.com
repository: 
issue: 
icon: com.bitcoli.bitcoli.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2024-02-28
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Jaroslav Bruzek

---

{% include copyFromAndroid.html %}

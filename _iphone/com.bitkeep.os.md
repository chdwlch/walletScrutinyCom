---
wsId: bitkeep
title: Bitget Wallet, ex BitKeep
altTitle: 
authors:
- leo
appId: com.bitkeep.os
appCountry: 
idd: 1395301115
released: 2018-09-26
updated: 2024-04-19
version: 8.14.0
stars: 4.7
reviews: 1375
size: '165947392'
website: https://bitkeep.com
repository: 
issue: 
icon: com.bitkeep.os.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: BitKeepOS
social:
- https://www.facebook.com/bitkeep
- https://github.com/bitkeepcom
features: 
developerName: BitKeep Global Inc.

---

 {% include copyFromAndroid.html %}

---
wsId: bitkubExchange
title: 'Bitkub : Buy Bitcoin & Crypto'
altTitle: 
authors:
- danny
appId: com.bitkub.app
appCountry: th
idd: 1437814700
released: 2019-04-24
updated: 2024-02-29
version: 3.29.0
stars: 4
reviews: 9948
size: '62129152'
website: https://www.bitkub.com/download
repository: 
issue: 
icon: com.bitkub.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-04
signer: 
reviewArchive: 
twitter: bitkubofficial
social:
- https://www.linkedin.com/company/bitkub
- https://www.facebook.com/bitkubofficial
features: 
developerName: Bitkub Online Co., Ltd

---

{% include copyFromAndroid.html %}


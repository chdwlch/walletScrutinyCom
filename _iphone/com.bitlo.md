---
wsId: bitlo
title: 'Bitlo: Bitcoin & Kripto Para'
altTitle: 
authors:
- danny
appId: com.bitlo
appCountry: tr
idd: '1544492069'
released: 2020-12-28
updated: 2024-04-24
version: 2.1.13
stars: 4.1
reviews: 702
size: '78699520'
website: https://www.bitlo.com/
repository: 
issue: 
icon: com.bitlo.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-02-08
signer: 
reviewArchive: 
twitter: bitlocom
social:
- https://www.linkedin.com/company/bitlo/
features: 
developerName: Bitlo Teknoloji Anonim Şirketi

---

{% include copyFromAndroid.html %}


---
wsId: bitoshiAfrica
title: Bitoshi
altTitle: 
authors:
- danny
appId: com.bitoshi
appCountry: us
idd: '1627285591'
released: 2022-10-17
updated: 2023-08-31
version: 1.3.0
stars: 4.7
reviews: 360
size: '47015936'
website: 
repository: 
issue: 
icon: com.bitoshi.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-11-01
signer: 
reviewArchive: 
twitter: Bitoshiafrica
social:
- https://bitoshi.africa
- https://www.linkedin.com/company/bitoshiafrica
- https://www.instagram.com/bitoshi.africa
- https://t.me/+c4ek89XILkc2OTk8
features: 
developerName: Bitoshi Digital Services Limited

---

{% include copyFromAndroid.html %}
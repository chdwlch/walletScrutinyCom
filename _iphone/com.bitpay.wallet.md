---
wsId: bitpaywallet
title: BitPay - Bitcoin Wallet & Card
altTitle: 
authors:
- leo
- emanuel
appId: com.bitpay.wallet
appCountry: 
idd: 1149581638
released: 2016-10-24
updated: 2024-03-18
version: 14.18.1
stars: 4.3
reviews: 2961
size: '38861824'
website: https://bitpay.com
repository: https://github.com/bitpay/bitpay-app
issue: https://github.com/bitpay/bitpay-app/issues/686
icon: com.bitpay.wallet.jpg
bugbounty: 
meta: ok
verdict: ftbfs
date: 2023-03-13
signer: 
reviewArchive:
- date: 2022-11-02
  version: 12.6.4
  appHash: 
  gitRevision: b323422a62c5d226572c32bffc8b499bbd9716a1
  verdict: nosource
- date: 2019-11-29
  version: 
  appHash: 
  gitRevision: 8a474ddd867e50ed46404ed9d81f2a893bbf6619
  verdict: ftbfs
twitter: BitPay
social:
- https://www.linkedin.com/company/bitpay-inc-
- https://www.facebook.com/BitPayOfficial
features: 
developerName: BitPay, Inc.

---

{% include copyFromAndroid.html %}

---
wsId: bitvavo
title: Bitvavo | Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.bitvavo
appCountry: be
idd: 1483903423
released: 2020-05-28
updated: 2024-04-23
version: 2.40.1
stars: 4.6
reviews: 4135
size: '106262528'
website: https://bitvavo.com
repository: 
issue: 
icon: com.bitvavo.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Bitvavo B.V.

---

{% include copyFromAndroid.html %}

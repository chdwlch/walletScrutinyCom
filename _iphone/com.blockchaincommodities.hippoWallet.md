---
wsId: hippoCrypto
title: 'Hippo: Crypto & Bitcoin Wallet'
altTitle: 
authors:
- danny
appId: com.blockchaincommodities.hippoWallet
appCountry: ae
idd: '1613041499'
released: 2022-04-01
updated: 2024-03-19
version: 1.17.1
stars: 5
reviews: 8
size: '67022848'
website: https://hippowallet.io
repository: 
issue: 
icon: com.blockchaincommodities.hippoWallet.jpg
bugbounty: 
meta: removed
verdict: nosource
date: 2024-04-19
signer: 
reviewArchive: 
twitter: HippoWallet
social:
- https://www.linkedin.com/company/hippo-wallet
- https://www.facebook.com/hippowallet
- https://www.youtube.com/@hippowallet
- https://www.instagram.com/hippo_wallet
- https://t.me/hippowallet
- https://www.reddit.com/user/hippowallet
- https://www.tumblr.com/hippowallet
- https://discord.com/invite/5KW493NV8R
- https://medium.com/@hippowallet
features: 
developerName: Blockchain Commodities

---

{% include copyFromAndroid.html %}

---
wsId: plutusWallet
title: Plutus | Bank On Crypto
altTitle: 
authors:
- danny
appId: com.blockcode.plutus.tappay
appCountry: gb
idd: '1410685948'
released: 2018-08-22
updated: 2023-06-01
version: 3.11.0
stars: 3.9
reviews: 238
size: '62606336'
website: https://plutus.it
repository: 
issue: 
icon: com.blockcode.plutus.tappay.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-12-14
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: BLOCK CODE LTD

---

{% include copyFromAndroid.html %}
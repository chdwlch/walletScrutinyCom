---
wsId: btcmarkets
title: BTC Markets
altTitle: 
authors:
- danny
appId: com.btcmarkets.btcm
appCountry: au
idd: '1546957530'
released: 2022-03-19
updated: 2024-04-23
version: 1.7.2
stars: 3.9
reviews: 63
size: '25299968'
website: https://www.btcmarkets.net
repository: 
issue: 
icon: com.btcmarkets.btcm.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-20
signer: 
reviewArchive: 
twitter: BTCMarkets
social:
- https://www.facebook.com/btcmarkets
- https://www.linkedin.com/company/btc-markets
- https://t.me/BTCMkts
- https://www.instagram.com/btcmarkets_
features: 
developerName: BTC Markets

---

{% include copyFromAndroid.html %}


---
wsId: buenbit
title: Buenbit
altTitle: 
authors:
- danny
appId: com.buengroup.buenbit
appCountry: co
idd: '1552402029'
released: 2021-11-23
updated: 2024-03-06
version: 3.7.0
stars: 4.7
reviews: 6
size: '111522816'
website: https://www.buenbit.com/
repository: 
issue: 
icon: com.buengroup.buenbit.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-12-14
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Buenbit

---

{% include copyFromAndroid.html %}
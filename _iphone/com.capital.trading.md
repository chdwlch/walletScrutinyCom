---
wsId: capitalTrading
title: 'Capital.com: Trading & Finance'
altTitle: 
authors:
- danny
appId: com.capital.trading
appCountry: 
idd: 1230088754
released: 2019-12-10
updated: 2021-12-21
version: 1.36.3
stars: 4.7
reviews: 2314
size: 98812928
website: 
repository: 
issue: 
icon: com.capital.trading.jpg
bugbounty: 
meta: removed
verdict: nosendreceive
date: 2024-03-02
signer: 
reviewArchive: 
twitter: capitalcom
social:
- https://www.linkedin.com/company/capital.com/
- https://www.facebook.com/capitalcom
- https://www.youtube.com/channel/UCn65Ma-zHYgnr56LPAwWDTw
- https://www.reddit.com/r/capitalcom
features: 
developerName: 

---

**Update 2024-02-08**: This app is back on the Apple store.

{% include copyFromAndroid.html %}

**Update 2022-01-15**: This app is not on the Store anymore.

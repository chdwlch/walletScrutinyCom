---
wsId: junoFinance
title: Juno - Buy Bitcoin & Litecoin
altTitle: 
authors:
- danny
appId: com.capitalJ.onJuno
appCountry: us
idd: '1525858971'
released: 2021-02-13
updated: 2024-04-26
version: 4.1.2
stars: 3.9
reviews: 892
size: '463587328'
website: https://juno.finance
repository: 
issue: 
icon: com.capitalJ.onJuno.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-19
signer: 
reviewArchive: 
twitter: JunoFinanceHQ
social:
- https://www.linkedin.com/company/junofinancehq
features: 
developerName: CapitalJ Inc

---

{% include copyFromAndroid.html %}

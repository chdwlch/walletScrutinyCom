---
wsId: bitcoindepot
title: Bitcoin Depot
altTitle: 
authors:
- danny
appId: com.cashtocrypto.wallet
appCountry: us
idd: 1554808338
released: 2021-03-30
updated: 2024-03-18
version: 3.0.4
stars: 4.4
reviews: 618
size: '136711168'
website: https://bitcoindepot.com/
repository: 
issue: 
icon: com.cashtocrypto.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-08
signer: 
reviewArchive: 
twitter: bitcoin_depot
social:
- https://www.facebook.com/BitcoinDepot
features: 
developerName: Bitcoin Depot

---

{% include copyFromAndroid.html %}

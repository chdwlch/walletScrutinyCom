---
wsId: ciexExchange
title: CIEX
altTitle: 
authors:
- danny
appId: com.centurion.exchange
appCountry: us
idd: '1613498290'
released: 2022-03-24
updated: 2022-03-25
version: 5.3.6
stars: 5
reviews: 1
size: '168256512'
website: 
repository: 
issue: 
icon: com.centurion.exchange.jpg
bugbounty: 
meta: obsolete
verdict: custodial
date: 2024-03-15
signer: 
reviewArchive: 
twitter: CENTURION_INVST
social:
- https://centurioninvest.com
- https://t.me/centurioninvestgroup
- https://www.youtube.com/channel/UCkjUQQCjZ5ed366GAUyRuog
- https://www.facebook.com/CenturionInvest
- https://www.linkedin.com/company/centurioninvest
- https://www.instagram.com/centurioninvest
features: 
developerName: Centurioninvest OU

---

{% include copyFromAndroid.html %}

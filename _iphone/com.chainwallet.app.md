---
wsId: chainAppWeb3
title: Chain App - Web3 Wallet
altTitle: 
authors:
- danny
appId: com.chainwallet.app
appCountry: us
idd: '6444779277'
released: 2022-12-22
updated: 2024-03-11
version: 1.0.29
stars: 4.7
reviews: 12
size: '172564480'
website: https://chain.com
repository: 
issue: 
icon: com.chainwallet.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-02
signer: 
reviewArchive: 
twitter: chain
social:
- https://t.me/chain
- https://www.youtube.com/@chain
- https://www.facebook.com/chain
features: 
developerName: Chain Global Ltd

---

{% include copyFromAndroid.html %}

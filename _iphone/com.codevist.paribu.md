---
wsId: paribu
title: Paribu | Bitcoin Alım Satım
altTitle: 
authors:
- danny
appId: com.codevist.paribu
appCountry: 
idd: 1448200352
released: 2019-05-29
updated: 2024-04-09
version: 4.2.2
stars: 2.4
reviews: 35
size: '40685568'
website: https://www.paribu.com
repository: 
issue: 
icon: com.codevist.paribu.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-12-16
signer: 
reviewArchive: 
twitter: paribucom
social:
- https://www.youtube.com/c/Paribu
- https://www.linkedin.com/company/paribucom/
- https://www.instagram.com/paribucom/
- https://www.facebook.com/paribucom
features: 
developerName: Paribu Teknoloji A.Ş.

---

{% include copyFromAndroid.html %}
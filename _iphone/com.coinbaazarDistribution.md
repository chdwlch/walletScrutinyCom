---
wsId: CoinBaazar
title: Coinbaazar - Buy/Sell Bitcoins
altTitle: 
authors:
- danny
appId: com.coinbaazarDistribution
appCountry: us
idd: 1548921926
released: 2021-01-25
updated: 2023-12-19
version: 1.2.5.1
stars: 3.5
reviews: 8
size: '71207936'
website: https://coinbaazar.com
repository: 
issue: 
icon: com.coinbaazarDistribution.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-22
signer: 
reviewArchive: 
twitter: coin_baazar
social:
- https://www.facebook.com/officialcoinbaazar
features: 
developerName: COINBAAZAR LLC

---

{% include copyFromAndroid.html %}

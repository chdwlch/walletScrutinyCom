---
wsId: coinhako
title: 'Coinhako: Buy Bitcoin & Crypto'
altTitle: 
authors:
- kiwilamb
- leo
appId: com.coinhako.app
appCountry: 
idd: 1137855704
released: 2016-09-04
updated: 2024-04-17
version: 4.6.37
stars: 3.9
reviews: 30
size: '258314240'
website: https://www.coinhako.com
repository: 
issue: 
icon: com.coinhako.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-04-24
signer: 
reviewArchive: 
twitter: coinhako
social:
- https://www.linkedin.com/company/coinhako
- https://www.facebook.com/coinhako
features: 
developerName: CoinHako

---

Having a scan over the providers website and faq articles does not reveal any
claims regarding the management of private keys.
We would have to assume this wallet is custodial.

Our verdict: This “wallet” is probably custodial and therefore is **not verifiable**.

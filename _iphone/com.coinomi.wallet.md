---
wsId: coinomi
title: Coinomi Wallet
altTitle: 
authors:
- leo
appId: com.coinomi.wallet
appCountry: 
idd: 1333588809
released: 2018-03-22
updated: 2022-03-21
version: 1.14.0
stars: 4.5
reviews: 1793
size: '140368896'
website: https://www.coinomi.com
repository: 
issue: 
icon: com.coinomi.wallet.jpg
bugbounty: 
meta: obsolete
verdict: nosource
date: 2024-03-12
signer: 
reviewArchive: 
twitter: CoinomiWallet
social:
- https://www.linkedin.com/company/coinomi
- https://www.facebook.com/coinomi
- https://www.reddit.com/r/COINOMI
features: 
developerName: Coinomi Limited

---

This wallet claims to be non-custodial but
[went closed-source](https://github.com/bitcoin-dot-org/bitcoin.org/issues/1622)
in early 2016
[according to them](https://twitter.com/CoinomiWallet/status/945048682927394817)

> **coinomi**<br>
  @CoinomiWallet<br>
  No, we moved to closed source to protect the users from getting ripped off by
  scammers. Our website is open source and there are at least 10 clones at the
  time of the writing stealing users' funds (by stealing their private keys).<br>
  6:48 PM · Dec 24, 2017

Our verdict: This app is **not verifiable**.

---
wsId: coinpassMobile
title: coinpass Mobile
altTitle: 
authors:
- danny
appId: com.coinpasslimited.coinpassinstant
appCountry: bg
idd: '1530358553'
released: 2020-11-20
updated: 2024-01-03
version: 2.0.13
stars: 0
reviews: 0
size: '55623680'
website: https://coinpass.com/
repository: 
issue: 
icon: com.coinpasslimited.coinpassinstant.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2024-02-05
signer: 
reviewArchive: 
twitter: coinpassglobal
social:
- https://www.facebook.com/coinpassglobal
- https://www.youtube.com/channel/UCfs5WkT1mecmB0YWJipJLdw
- https://www.linkedin.com/company/coinpass/
features: 
developerName: Oanda Coinpass Limited

---

{% include copyFromAndroid.html %}
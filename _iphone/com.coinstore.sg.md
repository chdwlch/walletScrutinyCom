---
wsId: coinStore
title: Coinstore:Trade Crypto&Futures
altTitle: 
authors:
- danny
appId: com.coinstore.sg
appCountry: us
idd: '1567160644'
released: 2021-05-12
updated: 2024-04-23
version: 2.8.0
stars: 4.1
reviews: 82
size: '160393216'
website: https://www.coinstore.com
repository: 
issue: 
icon: com.coinstore.sg.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-06-24
signer: 
reviewArchive: 
twitter: coinstore_en
social:
- https://www.linkedin.com/company/coinstore
- https://coinstore.medium.com
- https://www.facebook.com/coinstoreglobal
- https://t.me/coinstore_english
features: 
developerName: COINSTORE PTE. LTD.

---

{% include copyFromAndroid.html %}
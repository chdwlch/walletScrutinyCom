---
wsId: CoinSwitch
title: 'CoinSwitch: Trade Crypto & BTC'
altTitle: 
authors:
- danny
appId: com.coinswitch.kuber
appCountry: in
idd: 1540214951
released: 2020-12-01
updated: 2024-04-16
version: 6.4.1
stars: 4.5
reviews: 52766
size: '118425600'
website: https://coinswitch.co
repository: 
issue: 
icon: com.coinswitch.kuber.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-21
signer: 
reviewArchive: 
twitter: CoinSwitchKuber
social:
- https://www.linkedin.com/company/coinswitch
- https://www.facebook.com/coinswitch
features: 
developerName: BITKUBER INVESTMENTS PRIVATE LIMITED

---

{% include copyFromAndroid.html %}

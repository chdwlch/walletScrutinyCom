---
wsId: coinWBuyCrypto
title: CoinW
altTitle: 
authors:
- danny
appId: com.coinwapp.legend
appCountry: us
idd: '1494077068'
released: 2020-01-14
updated: 2024-04-28
version: 9.10.2
stars: 4.3
reviews: 3
size: '186626048'
website: 
repository: 
issue: 
icon: com.coinwapp.legend.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-07
signer: 
reviewArchive: 
twitter: CoinWOfficial
social:
- https://www.coinw.com
- https://t.me/CoinwExchangeEnglish
- https://www.linkedin.com/company/coinw-exchange
- https://medium.com/@coinwglobal23
- https://www.instagram.com/coinw_exchange
features: 
developerName: COINW SP Z O O

---

{% include copyFromAndroid.html %}

---
wsId: criptanWallet
title: Criptan. Complement your bank.
altTitle: 
authors:
- danny
appId: com.criptan.app
appCountry: es
idd: '1497960991'
released: 2020-06-18
updated: 2024-04-26
version: 2.16.25
stars: 4.6
reviews: 301
size: '108246016'
website: https://criptan.es
repository: 
issue: 
icon: com.criptan.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-12-14
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Criptan Trade SL

---

{% include copyFromAndroid.html %}
---
wsId: croptyWallet
title: Crypto wallet – Bitcoin & USDT
altTitle: 
authors:
- danny
appId: com.cropty.app
appCountry: us
idd: '1624901793'
released: 2022-08-04
updated: 2024-03-21
version: 1.5.8
stars: 4.9
reviews: 90
size: '17009664'
website: https://cropty.io/
repository: 
issue: 
icon: com.cropty.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-30
signer: 
reviewArchive: 
twitter: cropty_app
social:
- https://www.youtube.com/@croptytv
features: 
developerName: Coinscatch

---

{% include copyFromAndroid.html %}
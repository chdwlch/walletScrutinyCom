---
wsId: cryptnoxCompanion
title: Cryptnox Wallet
altTitle: 
authors:
- danny
appId: com.cryptnox.companion
appCountry: us
idd: '1583011693'
released: 2021-10-10
updated: 2024-04-07
version: 2.4.1
stars: 3.7
reviews: 3
size: '62369792'
website: https://www.cryptnox.com
repository: 
issue: 
icon: com.cryptnox.companion.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-11-15
signer: 
reviewArchive: 
twitter: CryptnoxTech
social:
- https://github.com/Cryptnox-Software
- https://www.linkedin.com/company/cryptnox/
features: 
developerName: Cryptnox

---

{% include copyFromAndroid.html %}
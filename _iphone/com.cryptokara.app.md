---
wsId: cryptoKara
title: CryptoKara
altTitle: 
authors:
- danny
appId: com.cryptokara.app
appCountry: us
idd: '1581610129'
released: 2021-10-19
updated: 2024-04-08
version: 7.4.3
stars: 4
reviews: 43
size: '58036224'
website: https://crypto-kara-site.vercel.app/
repository: 
issue: 
icon: com.cryptokara.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-15
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: AUTOMATED CHAIN LIMITED

---

{% include copyFromAndroid.html %}

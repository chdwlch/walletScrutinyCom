---
wsId: stackWallet
title: Stack Wallet by Cypher Stack
altTitle: 
authors:
- danny
appId: com.cypherstack.stackwallet
appCountry: 
idd: '1634811534'
released: 2022-08-26
updated: 2024-04-22
version: 1.10.4
stars: 3.9
reviews: 14
size: '164116480'
website: https://stackwallet.com/
repository: 
issue: 
icon: com.cypherstack.stackwallet.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-09-08
signer: 
reviewArchive: 
twitter: stack_wallet
social:
- https://discord.com/invite/mRPZuXx3At
- https://t.me/stackwallet
- https://www.reddit.com/r/stackwallet
- https://www.youtube.com/channel/UCqCtpXsLyNIle1uOO2DU7JA
features: 
developerName: Cypher Stack LLC

---

{% include copyFromAndroid.html %}
---
wsId: defexaWallet
title: Defexa - Bitcoin Crypto Wallet
altTitle: 
authors:
- danny
appId: com.defexa.CryptoWallet-Defexa
appCountry: us
idd: '6446314571'
released: 2023-05-29
updated: 2024-04-18
version: 2.1.1
stars: 3
reviews: 2
size: '94432256'
website: https://defexa.io/wallet
repository: 
issue: 
icon: com.defexa.CryptoWallet-Defexa.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-11-02
signer: 
reviewArchive: 
twitter: DefexaCrypto
social:
- https://discord.com/invite/JaSnYuXYS5
- https://t.me/defexa
- https://www.linkedin.com/company/defexa
- https://www.quora.com/profile/Defexa
features: 
developerName: FPS Global LTD

---

{% include copyFromAndroid.html %}
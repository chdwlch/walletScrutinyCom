---
wsId: defiatoExchange
title: DeFiato
altTitle: 
authors:
- danny
appId: com.defiato
appCountry: us
idd: '1591008049'
released: 2021-10-27
updated: 2022-12-30
version: 1.2.8
stars: 5
reviews: 2
size: '64485376'
website: 
repository: 
issue: 
icon: com.defiato.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-12-29
signer: 
reviewArchive: 
twitter: 
social:
- https://defiato.com
features: 
developerName: Beowulf Network Inc

---

{% include copyFromAndroid.html %}

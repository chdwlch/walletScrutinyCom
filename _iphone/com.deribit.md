---
wsId: deribitOptions
title: 'Deribit: BTC Options & Futures'
altTitle: 
authors:
- danny
appId: com.deribit
appCountry: gb
idd: '1293674041'
released: 2017-11-17
updated: 2023-10-27
version: 3.6.0
stars: 4.9
reviews: 12
size: '35704832'
website: http://www.deribit.com
repository: 
issue: 
icon: com.deribit.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2024-02-05
signer: 
reviewArchive: 
twitter: deribitexchange
social:
- https://www.linkedin.com/company/deribit/
- https://www.reddit.com/r/DeribitExchange/
- https://t.me/deribit
- https://www.youtube.com/channel/UCbHKjlFogkOD0lUVeb5CsGA
features: 
developerName: Deribit

---

{% include copyFromAndroid.html %}
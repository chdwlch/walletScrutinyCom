---
wsId: ember
title: Ember Fund - Invest in Crypto
altTitle: 
authors:
- danny
appId: com.emberfund.ember
appCountry: us
idd: 1406211993
released: 2018-08-04
updated: 2024-04-01
version: '33.18'
stars: 4.5
reviews: 2310
size: '100742144'
website: https://emberfund.io/
repository: 
issue: 
icon: com.emberfund.ember.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: Ember_Fund
social:
- https://github.com/ember-fund
features: 
developerName: Ember Fund LLC

---

{% include copyFromAndroid.html %}

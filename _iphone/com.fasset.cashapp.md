---
wsId: fassetCrypto
title: Fasset - Buy Digital Assets
altTitle: 
authors:
- danny
appId: com.fasset.cashapp
appCountry: gb
idd: '1599893605'
released: 2021-12-14
updated: 2023-09-27
version: '2.4'
stars: 5
reviews: 1
size: '84147200'
website: https://fasset.io/
repository: 
issue: 
icon: com.fasset.cashapp.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2024-02-05
signer: 
reviewArchive: 
twitter: fasset_official
social:
- https://www.linkedin.com/company/fassethq
- https://www.facebook.com/FassetOfficial
- https://www.instagram.com/fasset_official
- https://t.me/+WHV71rgNlWEchWKr
features: 
developerName: Fasset Ltd.

---

{% include copyFromAndroid.html %}

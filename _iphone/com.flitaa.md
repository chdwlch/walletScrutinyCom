---
wsId: flitaa
title: flitaa
altTitle: 
authors:
- danny
appId: com.flitaa
appCountry: ng
idd: 1566777501
released: 2021-05-25
updated: 2023-06-23
version: 1.34.0
stars: 3.5
reviews: 65
size: '83151872'
website: https://flitbase.com
repository: 
issue: 
icon: com.flitaa.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-03
signer: 
reviewArchive: 
twitter: getflitaa
social:
- https://www.facebook.com/getflitaa
features: 
developerName: FLITBASE

---

{% include copyFromAndroid.html %}

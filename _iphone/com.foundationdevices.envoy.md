---
wsId: envoyFoundation
title: Envoy by Foundation
altTitle: 
authors:
- danny
appId: com.foundationdevices.envoy
appCountry: us
idd: '1584811818'
released: 2022-07-14
updated: 2024-04-25
version: 1.6.2
stars: 5
reviews: 18
size: '158135296'
website: 
repository: https://github.com/Foundation-Devices/envoy
issue: 
icon: com.foundationdevices.envoy.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2024-01-02
signer: 
reviewArchive: 
twitter: FOUNDATIONdvcs
social:
- https://foundationdevices.com
- https://www.linkedin.com/company/foundationdevices
- https://www.youtube.com/@foundationdevices
- https://www.reddit.com/r/FoundationDevices
- https://t.me/foundationdevices
features: 
developerName: Foundation Devices, Inc.

---

{% include copyFromAndroid.html %}
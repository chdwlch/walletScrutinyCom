---
wsId: frontierDeFi
title: 'Frontier: Crypto & DeFi Wallet'
altTitle: 
authors:
- danny
appId: com.frontierwallet
appCountry: in
idd: 1482380988
released: 2019-11-05
updated: 2024-02-04
version: 5.10.2
stars: 4.8
reviews: 46
size: '123332608'
website: https://frontier.xyz
repository: 
issue: 
icon: com.frontierwallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-01-13
signer: 
reviewArchive: 
twitter: FrontierDotXYZ
social: 
features: 
developerName: Ohana Labs Pte. Ltd.

---

{% include copyFromAndroid.html %}

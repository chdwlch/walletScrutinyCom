---
wsId: gate.io
title: Gate.io - Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.gateio.app.gateio-app
appCountry: id
idd: 1294998195
released: 2017-11-03
updated: 2024-04-28
version: 6.2.5
stars: 3.9
reviews: 400
size: '565329920'
website: https://gate.io
repository: 
issue: 
icon: com.gateio.app.gateio-app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-05
signer: 
reviewArchive: 
twitter: gate_io
social:
- https://www.facebook.com/gateioglobal
- https://www.reddit.com/r/GateioExchange
features: 
developerName: GATE GLOBAL UAB

---

{% include copyFromAndroid.html %}

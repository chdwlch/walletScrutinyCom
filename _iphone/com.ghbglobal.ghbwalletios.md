---
wsId: ghbGlobalWallet
title: GHB Wallet
altTitle: 
authors:
- danny
appId: com.ghbglobal.ghbwalletios
appCountry: sa
idd: '1507339655'
released: 2020-04-23
updated: 2021-12-01
version: 1.1.8
stars: 0
reviews: 0
size: '87882752'
website: 
repository: 
issue: 
icon: com.ghbglobal.ghbwalletios.jpg
bugbounty: 
meta: obsolete
verdict: nobtc
date: 2023-11-25
signer: 
reviewArchive: 
twitter: CorporationGhb
social:
- http://ghbwallet.com
- https://www.facebook.com/wallet.ghb.7
- https://t.me/ghbinfo
- https://www.youtube.com/channel/UCmtoOoY_KrU-12ke92zhG4Q
features: 
developerName: GHB Co.ltd

---

% include copyFromAndroid.html %}

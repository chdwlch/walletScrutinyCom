---
wsId: gleecCard
title: Gleec Card Crypto-powered card
altTitle: 
authors:
- danny
appId: com.gleecard.ios
appCountry: ph
idd: '1532231032'
released: 2021-02-21
updated: 2024-03-06
version: '3.6'
stars: 1
reviews: 1
size: '92052480'
website: https://gleec.com/
repository: 
issue: 
icon: com.gleecard.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-05-04
signer: 
reviewArchive: 
twitter: GleecOfficial
social:
- https://www.facebook.com/gleecofficial
- https://t.me/officialgleecoin
features: 
developerName: Gleec-BTC OU

---

{% include copyFromAndroid.html %}

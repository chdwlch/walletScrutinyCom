---
wsId: globianceApp
title: Globiance
altTitle: 
authors:
- danny
appId: com.globiance.iosapp
appCountry: us
idd: '1584923932'
released: 2021-10-05
updated: 2024-01-24
version: '2.06'
stars: 4.3
reviews: 82
size: '58161152'
website: https://globiance.com
repository: 
issue: 
icon: com.globiance.iosapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-20
signer: 
reviewArchive: 
twitter: globiance
social:
- https://www.facebook.com/Globiance
- https://www.youtube.com/c/GLOBIANCE
- https://www.instagram.com/globiance
- https://t.me/globiancegroup
- https://www.linkedin.com/company/globiance
features: 
developerName: Globiance Holdings Limited

---

{% include copyFromAndroid.html %}
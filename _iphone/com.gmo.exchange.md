---
wsId: ZcomEx
title: Z.com EX - Buy/Sell Bitcoin
altTitle: 
authors:
- danny
appId: com.gmo.exchange
appCountry: th
idd: 1525862502
released: 2020-08-09
updated: 2024-04-11
version: 3.0.8
stars: 3.7
reviews: 29
size: '108779520'
website: https://ex.z.com/
repository: 
issue: 
icon: com.gmo.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-08
signer: 
reviewArchive: 
twitter: ZcomExchange
social:
- https://www.facebook.com/ZcomCrypto
features: 
developerName: GMO-Z.Com Cryptonomics (Thailand) Co.,Ltd

---

{% include copyFromAndroid.html %}

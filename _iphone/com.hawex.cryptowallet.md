---
wsId: hawexCrypto
title: 'Hawex: Crypto Cards & Wallet'
altTitle: 
authors:
- danny
appId: com.hawex.cryptowallet
appCountry: cl
idd: '1660970455'
released: 2023-03-21
updated: 2023-10-06
version: 2.0 (23)
stars: 0
reviews: 0
size: '70159360'
website: 
repository: 
issue: 
icon: com.hawex.cryptowallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-11-02
signer: 
reviewArchive: 
twitter: hawexdao
social:
- https://t.me/hawex
- https://discord.gg/CeFH7YaMXe
features: 
developerName: HAWEX INVESTMENT L.L.C

---

{% include copyFromAndroid.html %}
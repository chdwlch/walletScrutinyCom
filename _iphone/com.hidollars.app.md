---
wsId: hidollars
title: 'hi: Web3 Wallet-Crypto Neobank'
altTitle: 
authors:
- danny
appId: com.hidollars.app
appCountry: US
idd: '1583215766'
released: 2021-10-06
updated: 2023-01-19
version: 2.4.6
stars: 3.7
reviews: 181
size: '187669504'
website: https://hi.com/
repository: 
issue: 
icon: com.hidollars.app.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-05-10
signer: 
reviewArchive: 
twitter: hi_com_official
social:
- https://www.facebook.com/hi.com.official
- https://www.instagram.com/hi.com.official
- https://www.reddit.com/r/hi_official
features: 
developerName: 

---

{% include copyFromAndroid.html %}
---
wsId: Ibandirect
title: Ibandirect
altTitle: 
authors:
- danny
appId: com.ibandirect.cards
appCountry: sg
idd: 1538001175
released: 2020-11-09
updated: 2023-05-19
version: 1.8.0
stars: 0
reviews: 0
size: '85158912'
website: https://ibandirect.com/
repository: 
issue: 
icon: com.ibandirect.cards.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-08-04
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Ibandirect

---

{% include copyFromAndroid.html %}

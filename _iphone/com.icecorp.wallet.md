---
wsId: icewalWallet
title: Icewal
altTitle: 
authors:
- danny
appId: com.icecorp.wallet
appCountry: tr
idd: '1629607532'
released: 2022-11-28
updated: 2024-04-24
version: 3.0.7
stars: 5
reviews: 8
size: '36554752'
website: 
repository: 
issue: 
icon: com.icecorp.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-28
signer: 
reviewArchive: 
twitter: icewalofficial
social:
- https://www.icewal.com
- https://www.facebook.com/icewalofficial
- https://www.linkedin.com/company/icecorp
- https://www.instagram.com/icewalofficial
features: 
developerName: Ice Corporation

---

{% include copyFromAndroid.html %}

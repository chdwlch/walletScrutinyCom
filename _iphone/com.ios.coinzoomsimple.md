---
wsId: coinZoom
title: CoinZoom feat. ZoomMe
altTitle: 
authors:
- danny
appId: com.ios.coinzoomsimple
appCountry: us
idd: '1575983875'
released: 2022-01-21
updated: 2024-04-22
version: 3.0.15
stars: 4.7
reviews: 109
size: '63702016'
website: http://www.coinzoom.com
repository: 
issue: 
icon: com.ios.coinzoomsimple.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-30
signer: 
reviewArchive: 
twitter: GetCoinZoom
social:
- https://www.facebook.com/CoinZoom
- https://www.linkedin.com/company/coinzoomhq/
features: 
developerName: CoinZoom

---

{% include copyFromAndroid.html %}

---
wsId: bitcointradingcapital
title: Bitcoin trading - Capital.com
altTitle: 
authors:
- danny
appId: com.kapital.trade.crypto
appCountry: cz
idd: 1487443266
released: 2019-11-26
updated: 2024-04-22
version: 1.80.2
stars: 4.7
reviews: 779
size: '96759808'
website: https://capital.com/
repository: 
issue: 
icon: com.kapital.trade.crypto.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2024-02-08
signer: 
reviewArchive: 
twitter: capitalcom
social: 
features: 
developerName: CAPITAL BULGARIA

---

{% include copyFromAndroid.html %}

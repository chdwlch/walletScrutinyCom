---
wsId: kinesismoney
title: Kinesis - Buy gold and silver
altTitle: 
authors:
- danny
appId: com.kinesis.kinesis-app
appCountry: us
idd: 1490483608
released: 2020-02-28
updated: 2023-03-21
version: 1.12.4
stars: 4
reviews: 51
size: '100733952'
website: https://kinesis.money/
repository: https://github.com/KinesisNetwork/wallet-mobile
issue: 
icon: com.kinesis.kinesis-app.jpg
bugbounty: 
meta: removed
verdict: nosource
date: 2023-12-19
signer: 
reviewArchive: 
twitter: KinesisMonetary
social:
- https://www.linkedin.com/company/kinesismoney
- https://www.facebook.com/kinesismoney
- https://www.reddit.com/r/Kinesis_money
features: 
developerName: Kinesis AG

---

{% include copyFromAndroid.html %}

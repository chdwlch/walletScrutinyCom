---
wsId: 
title: KoinKoin Exchange
altTitle: 
authors:
- danny
appId: com.koinkoin.app
appCountry: ng
idd: '1556014433'
released: 2021-03-04
updated: 2023-12-21
version: 2.3.4
stars: 0
reviews: 0
size: '51896320'
website: 
repository: 
issue: 
icon: com.koinkoin.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2024-01-18
signer: 
reviewArchive: 
twitter: 
social:
- https://www.reddit.com/r/KoinKoin_io/
- https://www.facebook.com/Koinkoinn/
- https://www.linkedin.com/company/koinkoin-ltd/
features: 
developerName: KoinKoin Ltd

---

{% include copyFromAndroid.html %}
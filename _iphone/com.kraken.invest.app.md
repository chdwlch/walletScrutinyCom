---
wsId: krakenInvest
title: Kraken - Buy Crypto & Bitcoin
altTitle: 
authors:
- danny
appId: com.kraken.invest.app
appCountry: us
idd: '1481947260'
released: 2021-06-01
updated: 2024-04-22
version: 2.14.0
stars: 4.7
reviews: 6244
size: '127416320'
website: https://kraken.com
repository: 
issue: 
icon: com.kraken.invest.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-12-15
signer: 
reviewArchive: 
twitter: krakenfx
social:
- https://www.linkedin.com/company/krakenfx
- https://www.facebook.com/KrakenFX
- https://www.reddit.com/r/Kraken
features: 
developerName: Kraken

---

{% include copyFromAndroid.html %}

---
wsId: kriptradeExchange
title: Kriptrade
altTitle: 
authors:
- danny
appId: com.kriptrade.mobile
appCountry: tr
idd: '1591104692'
released: 2021-10-28
updated: 2024-04-18
version: 2.4.8
stars: 4.1
reviews: 16
size: '39919616'
website: https://kriptrade.com
repository: 
issue: 
icon: com.kriptrade.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-09-06
signer: 
reviewArchive: 
twitter: kriptradetr
social:
- https://www.facebook.com/KriptradeTR
- https://www.instagram.com/kriptrade
- https://www.youtube.com/channel/UCJxN4di4kXY89NxOiv3K6BA
features: 
developerName: Kriptrade

---

{% include copyFromAndroid.html %}
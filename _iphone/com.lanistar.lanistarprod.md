---
wsId: lanistar
title: Lanistar
altTitle: 
authors:
- danny
appId: com.lanistar.lanistarprod
appCountry: us
idd: '1535627210'
released: 2020-11-04
updated: 2024-04-04
version: 2.0.60
stars: 3.3
reviews: 108
size: '105161728'
website: https://www.lanistar.com
repository: 
issue: 
icon: com.lanistar.lanistarprod.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-11-17
signer: 
reviewArchive: 
twitter: iamlanistar
social:
- https://www.instagram.com/lanistar
features: 
developerName: Lanistar Limited

---

{% include copyFromAndroid.html %}

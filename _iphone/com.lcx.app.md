---
wsId: lcxExchange
title: 'LCX: Regulated Crypto Exchange'
altTitle: 
authors:
- danny
appId: com.lcx.app
appCountry: us
idd: '1604594068'
released: 2022-06-27
updated: 2023-04-21
version: 2.0.3
stars: 4.3
reviews: 36
size: '57239552'
website: 
repository: 
issue: 
icon: com.lcx.app.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-05-30
signer: 
reviewArchive: 
twitter: lcx
social:
- https://www.youtube.com/channel/UCNj78acRE-ywQPim-wZ8RTQ
- https://www.instagram.com/lcx.exchange/
features: 
developerName: 

---

{% include copyFromAndroid.html %}


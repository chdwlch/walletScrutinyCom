---
wsId: ledgerLive
title: 'Ledger Live: Crypto & NFT App'
altTitle: 
authors:
- danny
appId: com.ledger.live
appCountry: us
idd: '1361671700'
released: 2019-01-28
updated: 2024-03-26
version: '3.42'
stars: 4.9
reviews: 9818
size: '149698560'
website: https://www.ledger.com/pages/ledger-live
repository: 
issue: 
icon: com.ledger.live.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-12-15
signer: 
reviewArchive: 
twitter: Ledger
social:
- https://www.ledger.com/ledger-live
- https://www.linkedin.com/company/ledgerhq
- https://www.facebook.com/Ledger
features: 
developerName: Ledger SAS

---

{% include copyFromAndroid.html %}

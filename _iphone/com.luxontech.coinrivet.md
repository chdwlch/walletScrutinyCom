---
wsId: coinRivet
title: Coin Rivet
altTitle: 
authors:
- danny
appId: com.luxontech.coinrivet
appCountry: ca
idd: '1617049863'
released: 2022-05-11
updated: 2024-03-07
version: 1.3.4
stars: 0
reviews: 0
size: '23662592'
website: 
repository: 
issue: 
icon: com.luxontech.coinrivet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2024-04-02
signer: 
reviewArchive: 
twitter: CoinRivet
social:
- https://coinrivet.com
- https://www.instagram.com/CoinRivet
- https://www.facebook.com/CoinRivet
features: 
developerName: GALIAS SERVICES UAB

---

{% include copyFromAndroid.html %}
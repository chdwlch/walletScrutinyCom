---
wsId: margex100X
title: Margex – Up to 100x Leverage
altTitle: 
authors:
- danny
appId: com.margex.mobile
appCountry: us
idd: '1607974744'
released: 2022-02-09
updated: 2024-04-25
version: 4.3.0
stars: 4.3
reviews: 78
size: '61583360'
website: 
repository: 
issue: 
icon: com.margex.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-08
signer: 
reviewArchive: 
twitter: margexcom
social:
- https://margex.com
- https://www.facebook.com/margexcom
- https://www.youtube.com/c/margex
- https://t.me/margex_official
features: 
developerName: Margex Trading Solutions LTD

---

{% include copyFromAndroid.html %}

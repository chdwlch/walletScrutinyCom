---
wsId: maskEX
title: MaskEX Crypto & Privacy Wallet
altTitle: 
authors:
- danny
appId: com.maskexnnew.app
appCountry: us
idd: '1600880394'
released: 2021-12-22
updated: 2023-09-13
version: 2.0.7
stars: 3.7
reviews: 6
size: '77642752'
website: https://maskex.com/en
repository: 
issue: 
icon: com.maskexnnew.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-07
signer: 
reviewArchive: 
twitter: MaskexGlobal
social:
- https://www.facebook.com/MaskEXGlobal/
- https://www.instagram.com/maskexglobal/
- https://www.youtube.com/channel/UClSeew7MEmHi6i19GBEun9w
- https://t.me/maskextg
- https://www.linkedin.com/company/maskex-global/
features: 
developerName: Mask Global Market Co., Ltd

---

{% include copyFromAndroid.html %}


---
wsId: mathWallet5
title: MathWallet - Web3 Wallet
altTitle: 
authors:
- danny
appId: com.mathglobal.mathwallet5
appCountry: us
idd: '1582612388'
released: 2021-08-30
updated: 2024-04-18
version: 5.3.3
stars: 3.9
reviews: 29
size: '80836608'
website: https://www.mathwallet.org
repository: 
issue: 
icon: com.mathglobal.mathwallet5.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2024-01-05
signer: 
reviewArchive: 
twitter: Mathwallet
social:
- https://discord.com/invite/gXSfnk5eP5
- https://t.me/mathwalletnews
- https://www.youtube.com/c/MathWallet
features:
- segwit
- taproot
- multiwallet
developerName: MATH GLOBAL FOUNDATION LTD.

---

{% include copyFromAndroid.html %}
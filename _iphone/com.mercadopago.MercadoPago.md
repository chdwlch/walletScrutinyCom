---
wsId: mercadopago
title: 'Mercado Pago: cuenta digital'
altTitle: 
authors:
- leo
appId: com.mercadopago.MercadoPago
appCountry: br
idd: 925436649
released: 2014-12-17
updated: 2024-04-27
version: 2.323.3
stars: 4.8
reviews: 1375178
size: '321376256'
website: http://www.mercadopago.com
repository: 
issue: 
icon: com.mercadopago.MercadoPago.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-12-26
signer: 
reviewArchive: 
twitter: mercadopago
social:
- https://www.facebook.com/mercadopago
features: 
developerName: MercadoLibre

---

{% include copyFromAndroid.html %}
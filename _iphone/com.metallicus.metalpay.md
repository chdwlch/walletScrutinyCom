---
wsId: MetalPay
title: 'Metal Pay: Buy Bitcoin'
altTitle: 
authors:
- danny
appId: com.metallicus.metalpay
appCountry: us
idd: 1345101178
released: 2018-09-14
updated: 2024-04-23
version: 3.1.137
stars: 4.3
reviews: 4300
size: '118659072'
website: https://metalpay.com
repository: 
issue: 
icon: com.metallicus.metalpay.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: metalpaysme
social:
- https://www.facebook.com/metalpaysme
- https://www.reddit.com/r/MetalPay
features: 
developerName: Metallicus, Inc.

---

{% include copyFromAndroid.html %}


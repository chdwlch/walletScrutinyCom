---
wsId: hotXexchange
title: HotX：Buy & Sell BTC,ETH,Doge
altTitle: 
authors:
- danny
appId: com.metasigns.hotx
appCountry: us
idd: '1611301863'
released: 2022-03-02
updated: 2023-04-22
version: 1.0.18
stars: 5
reviews: 1
size: '83215360'
website: https://themetasigns.com/
repository: 
issue: 
icon: com.metasigns.hotx.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2024-02-05
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: METASIGNS INC.

---

{% include copyFromAndroid.html %}

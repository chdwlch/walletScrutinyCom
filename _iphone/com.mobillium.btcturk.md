---
wsId: BtcTurk
title: BtcTurk | Bitcoin Alım Satımı
altTitle: 
authors:
- danny
appId: com.mobillium.btcturk
appCountry: tr
idd: 1503482896
released: 2020-04-09
updated: 2023-11-30
version: 1.29.1
stars: 4.6
reviews: 26140
size: '225712128'
website: https://www.btcturk.com
repository: 
issue: 
icon: com.mobillium.btcturk.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive: 
twitter: btcturk
social:
- https://www.linkedin.com/company/btcturk
- https://www.facebook.com/btcturk
features: 
developerName: ELIPTIK YAZILIM VE TICARET ANONIM SIRKETI

---

{% include copyFromAndroid.html %}

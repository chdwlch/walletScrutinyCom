---
wsId: Moneybrain
title: Moneybrain Financial SuperApp
altTitle: 
authors:
- danny
appId: com.moneybrain.moneybrain
appCountry: gb
idd: 1476827262
released: 2019-10-15
updated: 2024-03-13
version: 2.3.74
stars: 4.9
reviews: 8
size: '30353408'
website: https://www.moneybrain.com
repository: 
issue: 
icon: com.moneybrain.moneybrain.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-22
signer: 
reviewArchive: 
twitter: MoneybrainBiPS
social: 
features: 
developerName: Moneybrain LTD

---

{% include copyFromAndroid.html %}
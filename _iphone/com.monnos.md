---
wsId: monnos
title: Monnos | Buy Bitcoin
altTitle: 
authors:
- danny
appId: com.monnos
appCountry: br
idd: 1476884342
released: 2019-09-30
updated: 2024-04-23
version: 5.10.9
stars: 4.5
reviews: 208
size: '167341056'
website: https://monnos.com
repository: 
issue: 
icon: com.monnos.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: monnosGlobal
social:
- https://www.linkedin.com/company/monnosglobal
- https://www.facebook.com/MonnosGlobal
features: 
developerName: KRYEG SOLUCOES DIGITAIS EIRELI

---

{% include copyFromAndroid.html %}
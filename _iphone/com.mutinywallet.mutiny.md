---
wsId: mutiny
title: Mutiny Wallet
altTitle: 
authors: 
appId: com.mutinywallet.mutiny
appCountry: us
idd: '6471030760'
released: 2024-03-01
updated: 2024-04-20
version: 1.6.7
stars: 5
reviews: 11
size: '43394048'
website: https://www.mutinywallet.com/
repository: 
issue: 
icon: com.mutinywallet.mutiny.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2024-03-07
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Mutiny Wallet, Inc.

---


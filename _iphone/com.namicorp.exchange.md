---
wsId: NamiExchange
title: 'Nami Exchange: Buy BTC, Crypto'
altTitle: 
authors:
- danny
appId: com.namicorp.exchange
appCountry: us
idd: '1480302334'
released: 2019-09-30
updated: 2024-03-12
version: 2.2.1
stars: 3.9
reviews: 20
size: '115950592'
website: https://nami.io/
repository: 
issue: 
icon: com.namicorp.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-07-20
signer: 
reviewArchive: 
twitter: NamiTrade
social:
- https://www.reddit.com/r/NAMIcoin/
- https://www.facebook.com/groups/800880653641607/
- https://medium.com/nami-io
features: 
developerName: NAMI TRADE PTE. LTD.

---

{% include copyFromAndroid.html %}
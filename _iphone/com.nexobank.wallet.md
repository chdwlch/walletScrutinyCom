---
wsId: nexo
title: 'Nexo: Buy Bitcoin & Crypto'
altTitle: 
authors:
- leo
appId: com.nexobank.wallet
appCountry: 
idd: 1455341917
released: 2019-06-30
updated: 2024-04-25
version: 4.3.1
stars: 3.9
reviews: 1196
size: '107394048'
website: https://nexo.com
repository: 
issue: 
icon: com.nexobank.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-22
signer: 
reviewArchive: 
twitter: NexoFinance
social:
- https://www.facebook.com/nexofinance
- https://www.reddit.com/r/Nexo
features: 
developerName: Nexo Capital Inc.

---

{% include copyFromAndroid.html %}

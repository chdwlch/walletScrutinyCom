---
wsId: ooBit
title: Oobit
altTitle: 
authors:
- danny
appId: com.oobit
appCountry: ru
idd: '1598882898'
released: 2022-02-05
updated: 2023-04-06
version: 1.4.7
stars: 5
reviews: 1
size: '90810368'
website: 
repository: 
issue: 
icon: com.oobit.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-08-04
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Oobit Technologies

---

{% include copyFromAndroid.html %}
---
wsId: osmoWallet
title: Osmo Wallet
altTitle: 
authors:
- danny
appId: com.osmowallet.app
appCountry: us
idd: '1610776134'
released: 2022-06-14
updated: 2024-04-25
version: 3.0.5
stars: 4.5
reviews: 17
size: '100470784'
website: https://www.osmowallet.com/
repository: 
issue: 
icon: com.osmowallet.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-24
signer: 
reviewArchive: 
twitter: osmowallet
social:
- https://www.linkedin.com/company/osmo-wallet
- https://discord.com/invite/9qmKes8drZ
- https://www.instagram.com/osmowallet
- https://www.facebook.com/osmoenvios
features:
- ln
developerName: Hodl Group Inc.

---

{% include copyFromAndroid.html %}

---
wsId: blackcatcard
title: Blackсatсard
altTitle: 
authors:
- danny
appId: com.papaya.blackcatcard
appCountry: lv
idd: 1449352913
released: 2019-03-07
updated: 2024-04-25
version: 1.2.55
stars: 3.7
reviews: 71
size: '282747904'
website: https://blackcatcard.com
repository: 
issue: 
icon: com.papaya.blackcatcard.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Papaya Ltd

---

{% include copyFromAndroid.html %}
---
wsId: phemex
title: 'Phemex: Crypto & BTC Trading'
altTitle: 
authors:
- kiwilamb
- leo
appId: com.phemex.submit
appCountry: 
idd: 1499601684
released: 2020-02-20
updated: 2024-04-27
version: 5.5.13
stars: 4.6
reviews: 2131
size: '121880576'
website: https://phemex.com/
repository: 
issue: 
icon: com.phemex.submit.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-04-20
signer: 
reviewArchive: 
twitter: phemex_official
social:
- https://www.linkedin.com/company/phemex
- https://www.facebook.com/Phemex.official
features: 
developerName: Phemex

---

The Phemex mobile app claims to hold funds in cold storage...

> All assets are 100% stored in cold wallets. Each withdrawal is thoroughly
  monitored and requires two-person approval with offline signatures.

leads us to conclude the wallet funds are in control of the provider and hence
custodial.

Our verdict: This 'wallet' is **not verifiable**.

---
wsId: phemexTR
title: 'Phemex TR: TL ile Bitcoin Al'
altTitle: 
authors:
- danny
appId: com.phemex.tr
appCountry: us
idd: '1587847973'
released: 2021-10-10
updated: 2022-09-28
version: 1.2.5
stars: 5
reviews: 1
size: '104222720'
website: 
repository: 
issue: 
icon: com.phemex.tr.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-12-19
signer: 
reviewArchive: 
twitter: 
social:
- https://phemex.com.tr
features: 
developerName: PHEMEX TURKEY TEKNOLOJİ LİMİTED ŞİRKETİ

---

{% include copyFromAndroid.html %}

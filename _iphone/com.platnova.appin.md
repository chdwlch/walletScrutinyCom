---
wsId: platnovaApp
title: Platnova
altTitle: 
authors:
- danny
appId: com.platnova.appin
appCountry: us
idd: '1619003446'
released: 2022-04-26
updated: 2024-04-18
version: 0.9.0
stars: 3.4
reviews: 38
size: '155793408'
website: https://platnova.com
repository: 
issue: 
icon: com.platnova.appin.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-11-02
signer: 
reviewArchive: 
twitter: getplatnova
social:
- https://www.facebook.com/getplatnova
- https://www.instagram.com/getplatnova
- https://www.linkedin.com/company/platnova
features: 
developerName: Platnova

---

{% include copyFromAndroid.html %}
---
wsId: powerTrade
title: PowerTrade
altTitle: 
authors:
- danny
appId: com.pt.powertrade
appCountry: id
idd: '1548085709'
released: 2021-04-15
updated: 2024-01-19
version: 3.0.8
stars: 5
reviews: 1
size: '447149056'
website: https://power.trade/
repository: 
issue: 
icon: com.pt.powertrade.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-11-17
signer: 
reviewArchive: 
twitter: powertradehq
social: 
features: 
developerName: Power Trade

---

{% include copyFromAndroid.html %}

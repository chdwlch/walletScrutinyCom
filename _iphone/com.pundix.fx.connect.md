---
wsId: functionX
title: FX Wallet by Function X
altTitle: 
authors:
- danny
appId: com.pundix.fx.connect
appCountry: us
idd: 1504798360
released: 2021-03-25
updated: 2024-04-28
version: 3.9.0
stars: 3.2
reviews: 64
size: '141694976'
website: https://functionx.io
repository: https://github.com/FunctionX/fx-wallet-android
issue: 
icon: com.pundix.fx.connect.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-26
signer: 
reviewArchive: 
twitter: FUNCTIONX_IO
social:
- https://www.linkedin.com/company/functionx
- https://www.facebook.com/FunctionX.io
features: 
developerName: PUNDI X LABS

---

{% include copyFromAndroid.html %}

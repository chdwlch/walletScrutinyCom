---
wsId: quidaxLite
title: Quidax - Buy & Sell Bitcoin
altTitle: 
authors:
- danny
appId: com.quidax.lite
appCountry: ng
idd: '1603997707'
released: 2022-01-13
updated: 2024-02-28
version: 1.14.5
stars: 4.5
reviews: 1491
size: '49253376'
website: 
repository: 
issue: 
icon: com.quidax.lite.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-21
signer: 
reviewArchive: 
twitter: quidaxglobal
social:
- https://www.facebook.com/QuidaxGlobal
- https://www.instagram.com/quidaxglobal
features: 
developerName: Quidax Technologies

---

{% include copyFromAndroid.html %}

---
wsId: Quppy
title: 'Quppy: Bitcoin Crypto Wallet'
altTitle: 
authors:
- leo
appId: com.quppy.wallet
appCountry: 
idd: 1417802076
released: 2018-08-09
updated: 2022-12-23
version: 2.0.55
stars: 2.7
reviews: 32
size: '76879872'
website: https://quppy.com
repository: 
issue: 
icon: com.quppy.wallet.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-08-16
signer: 
reviewArchive: 
twitter: QuppyPay
social:
- https://www.linkedin.com/company/quppy
- https://www.facebook.com/quppyPay
features: 
developerName: Quppy Europe OU

---

This provider loses no word on security or where the keys are stored. We assume
it is a custodial offering and therefore **not verifiable**.

---
wsId: rainfinancial
title: 'Rain: Buy & Sell Bitcoin'
altTitle: 
authors:
- danny
appId: com.rainmanagement.rain
appCountry: bh
idd: 1414619890
released: 2018-09-02
updated: 2024-04-13
version: 3.4.12
stars: 4.7
reviews: 2430
size: '78009344'
website: https://www.rain.bh/
repository: 
issue: 
icon: com.rainmanagement.rain.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: rainfinancial
social:
- https://www.linkedin.com/company/rainfinancial
- https://www.facebook.com/rainfinancial
features: 
developerName: Rain Management

---

 {% include copyFromAndroid.html %}

---
wsId: balanceWalletApp
title: Balance Wallet App
altTitle: 
authors:
- danny
appId: com.reacnative.js.balancenetwork.wallet
appCountry: tr
idd: '1658310376'
released: 2022-12-12
updated: 2023-12-22
version: 1.1.0
stars: 5
reviews: 10
size: '36255744'
website: https://wallet-balancenetwork.io/
repository: 
issue: 
icon: com.reacnative.js.balancenetwork.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-16
signer: 
reviewArchive: 
twitter: balancenetwork_
social:
- https://www.linkedin.com/company/balancenetwork
- https://www.facebook.com/BalanceNetworkOfficial
- https://balancenetwork.medium.com
- https://www.tiktok.com/@balancenetwork_
- https://www.reddit.com/r/BalanceNetwork
- https://t.me/BalanceNetwork
- https://www.instagram.com/balancenetwork
features: 
developerName: BALANCE NETWORK SP ZOO

---

{% include copyFromAndroid.html %}

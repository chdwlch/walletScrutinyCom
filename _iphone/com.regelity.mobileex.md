---
wsId: exnodeExchange
title: 'Exnode: Финансовый помощник'
altTitle: 
authors:
- danny
appId: com.regelity.mobileex
appCountry: ru
idd: '1663262329'
released: 2023-01-16
updated: 2024-04-24
version: 2.0.6
stars: 4.5
reviews: 24
size: '52563968'
website: https://exnode.by
repository: 
issue: 
icon: com.regelity.mobileex.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-07-17
signer: 
reviewArchive: 
twitter: 
social:
- https://t.me/exnoderu
- https://www.youtube.com/channel/UCBuAFCwmL2xzz6x0NPBCSUw
- https://vk.com/exnoderu
features: 
developerName: Exnode

---

{% include copyFromAndroid.html %}

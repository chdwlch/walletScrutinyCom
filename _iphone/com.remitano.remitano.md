---
wsId: Remitano
title: Remitano
altTitle: 
authors:
- leo
appId: com.remitano.remitano
appCountry: 
idd: 1116327021
released: 2016-05-28
updated: 2022-05-19
version: 6.46.0
stars: 4.8
reviews: 8710
size: '58142720'
website: https://remitano.com
repository: 
issue: 
icon: com.remitano.remitano.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-01-02
signer: 
reviewArchive: 
twitter: remitano
social:
- https://www.linkedin.com/company/Remitano
- https://www.facebook.com/remitano
features: 
developerName: 

---

This app is an interface to an exchange which holds your coins. On the App Store
and their website there is no claim to a non-custodial part to the app. As a
custodial app it is **not verifiable**.

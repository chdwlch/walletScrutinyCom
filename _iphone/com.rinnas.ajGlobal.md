---
wsId: AjGlobalV
title: AJGlobalV
altTitle: 
authors:
- danny
appId: com.rinnas.ajGlobal
appCountry: us
idd: 1519859460
released: 2020-09-08
updated: 2023-11-05
version: 2.0.20
stars: 3
reviews: 32
size: '48907264'
website: https://ajglobalv.com/
repository: 
issue: 
icon: com.rinnas.ajGlobal.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-19
signer: 
reviewArchive: 
twitter: ajglobalv
social:
- https://www.facebook.com/AJGLOBALVENTURES
features: 
developerName: AJ TRUST MERCHANTS GLOBAL LIMITED

---

{% include copyFromAndroid.html %}

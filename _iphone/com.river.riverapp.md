---
wsId: river
title: River – Buy Bitcoin
altTitle: 
authors:
- danny
appId: com.river.riverapp
appCountry: us
idd: '1536176542'
released: 2021-01-19
updated: 2024-04-26
version: 3.8.12
stars: 4.9
reviews: 1853
size: '67833856'
website: https://river.com/buy-bitcoin
repository: 
issue: 
icon: com.river.riverapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-12-15
signer: 
reviewArchive: 
twitter: River
social:
- https://www.linkedin.com/company/riverfinancial
features: 
developerName: River Financial Inc.

---

{% include copyFromAndroid.html %}
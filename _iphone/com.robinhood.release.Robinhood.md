---
wsId: Robinhood
title: 'Robinhood: Investing for All'
altTitle: 
authors:
- danny
appId: com.robinhood.release.Robinhood
appCountry: us
idd: 938003185
released: 2014-12-11
updated: 2024-04-22
version: 2024.16.0
stars: 4.2
reviews: 4298385
size: '411918336'
website: https://robinhood.com/
repository: 
issue: 
icon: com.robinhood.release.Robinhood.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-09-15
signer: 
reviewArchive: 
twitter: RobinhoodApp
social:
- https://www.linkedin.com/company/robinhood
- https://www.facebook.com/robinhoodapp
features: 
developerName: Robinhood Markets, Inc.

---

{% include copyFromAndroid.html %}

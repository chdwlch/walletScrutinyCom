---
wsId: rockwalletApp
title: 'RockWallet: Buy and Swap'
altTitle: 
authors:
- danny
appId: com.rockwallet.app
appCountry: us
idd: '6444194230'
released: 2022-11-16
updated: 2024-04-16
version: 5.14.0
stars: 4.7
reviews: 280
size: '103164928'
website: https://www.rockwallet.com
repository: https://github.com/rockwalletcode/wallet-ios
issue: https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/498
icon: com.rockwallet.app.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2023-08-28
signer: 
reviewArchive: 
twitter: rockwallet
social:
- https://www.facebook.com/rockwalletofficial
- https://www.instagram.com/rockwallet
- https://www.linkedin.com/company/rockwallet
features: 
developerName: RockWallet, LLC

---

The Android app is slated for verification, but the iOS is non-verifiable.

{% include copyFromAndroid.html %}

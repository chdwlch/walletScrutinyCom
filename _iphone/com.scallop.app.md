---
wsId: scallopApp
title: Scallop
altTitle: 
authors:
- danny
appId: com.scallop.app
appCountry: bg
idd: '1599717690'
released: 2021-12-10
updated: 2023-09-04
version: 1.2.7
stars: 0
reviews: 0
size: '123216896'
website: https://scallopx.com/
repository: 
issue: 
icon: com.scallop.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-18
signer: 
reviewArchive: 
twitter: ScallopOfficial
social:
- https://www.linkedin.com/company/scallopx
- https://t.me/Scallop_Official
- https://medium.com/scallopx
- https://www.instagram.com/scallop.official
features: 
developerName: Scallop Group

---

{% include copyFromAndroid.html %}
---
wsId: xverse
title: Xverse - Bitcoin Wallet
altTitle: 
authors:
- danny
appId: com.secretkeylabs.xverse
appCountry: gt
idd: 1552272513
released: 2021-10-15
updated: 2024-04-25
version: v1.29.1
stars: 5
reviews: 1
size: '30088192'
website: https://twitter.com/xverseApp
repository: 
issue: 
icon: com.secretkeylabs.xverse.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-13
signer: 
reviewArchive: 
twitter: secretkeylabs
social: 
features: 
developerName: Secret Key Labs

---

{% include copyFromAndroid.html %}


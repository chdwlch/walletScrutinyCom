---
wsId: ivanFGWallet
title: FG Wallet
altTitle: 
authors:
- danny
appId: com.sfc.fgwallet
appCountry: us
idd: '1338808692'
released: 2018-02-27
updated: 2024-04-16
version: 3.8.6
stars: 5
reviews: 2
size: '34845696'
website: https://fg-wallet.com/
repository: 
issue: 
icon: com.sfc.fgwallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-06-30
signer: 
reviewArchive: 
twitter: srsfintech
social:
- https://www.linkedin.com/company/srsfintech/
features: 
developerName: SRS FINTECH COMMERCE LTD

---

{% include copyFromAndroid.html %}

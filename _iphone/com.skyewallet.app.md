---
wsId: SkyeWallet
title: 'Skye Wallet: Buy & Sell Crypto'
altTitle: 
authors:
- danny
appId: com.skyewallet.app
appCountry: us
idd: 1587180582
released: 2021-09-27
updated: 2023-03-23
version: 2.12.8
stars: 3
reviews: 33
size: '51435520'
website: https://skyewallet.com/
repository: 
issue: 
icon: com.skyewallet.app.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2024-03-19
signer: 
reviewArchive: 
twitter: skyewallet
social: 
features: 
developerName: Skye Innovation Technology Limited

---

{% include copyFromAndroid.html %}

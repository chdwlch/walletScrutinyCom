---
wsId: cTrader
title: cTrader
altTitle: 
authors:
- danny
appId: com.spotware.ct
appCountry: my
idd: '767428811'
released: 2013-12-05
updated: 2024-03-26
version: 4.8.54457
stars: 4.8
reviews: 310
size: '208032768'
website: https://ctrader.com/forum
repository: 
issue: 
icon: com.spotware.ct.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-07-01
signer: 
reviewArchive: 
twitter: cTrader
social:
- https://www.linkedin.com/company/ctrader
- https://www.youtube.com/spotware
- https://t.me/cTrader_Official
features: 
developerName: Spotware

---

{% include copyFromAndroid.html %}

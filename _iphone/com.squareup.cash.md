---
wsId: CashApp
title: Cash App
altTitle: 
authors:
- leo
appId: com.squareup.cash
appCountry: 
idd: 711923939
released: 2013-10-16
updated: 2024-04-22
version: '4.44'
stars: 4.8
reviews: 6221661
size: '358440960'
website: https://cash.app
repository: 
issue: 
icon: com.squareup.cash.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2020-12-22
signer: 
reviewArchive: 
twitter: cashapp
social: 
features: 
developerName: Block, Inc.

---

On their website the provider claims:

> **Coin Storage**<br>
  Your Bitcoin balance is securely stored in our offline system

which means it is custodial.

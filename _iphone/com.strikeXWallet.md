---
wsId: StrikeXDefi
title: StrikeX | DeFi Crypto Wallet
altTitle: 
authors:
- danny
appId: com.strikeXWallet
appCountry: gb
idd: '6443517613'
released: 2022-10-21
updated: 2024-04-02
version: '2.6'
stars: 4.9
reviews: 213
size: '66319360'
website: 
repository: 
issue: 
icon: com.strikeXWallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-06-30
signer: 
reviewArchive: 
twitter: TradeStrikeBVI
social:
- https://tradestrike.io/strikex-wallet
- https://www.linkedin.com/companystrikex
- https://www.facebook.com/TradeStrikeBVI
- https://www.youtube.com/channel/UCZ_gCf9-Tnr7JeoFiSkl7SQ
- https://www.instagram.com/tradestrike.bvi
- https://t.me/StrikeXcrypto
- https://discord.com/invite/tradestrike
features: 
developerName: StrikeXWallet

---

{% include copyFromAndroid.html %}

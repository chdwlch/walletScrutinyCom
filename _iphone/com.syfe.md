---
wsId: syfeInvest
title: 'Syfe: Stay Invested'
altTitle: 
authors:
- danny
appId: com.syfe
appCountry: sg
idd: '1497156434'
released: 2020-02-24
updated: 2024-04-22
version: 10.9.2
stars: 4.1
reviews: 608
size: '108897280'
website: https://www.syfe.com
repository: 
issue: 
icon: com.syfe.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-07-10
signer: 
reviewArchive: 
twitter: SyfeSG
social:
- https://www.linkedin.com/company/syfe
- https://www.facebook.com/SyfeSG
- https://www.instagram.com/SyfeSG
- https://www.youtube.com/c/SyfeSG
features: 
developerName: Syfe Pte. Ltd.

---

{% include copyFromAndroid.html %}

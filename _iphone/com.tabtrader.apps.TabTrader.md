---
wsId: tabtrader
title: TabTrader - crypto terminal
altTitle: 
authors:
- leo
- kiwilamb
- danny
appId: com.tabtrader.apps.TabTrader
appCountry: 
idd: 1095716562
released: 2016-09-02
updated: 2024-04-19
version: '5.0'
stars: 4.7
reviews: 4177
size: '36267008'
website: https://tabtrader.com
repository: 
issue: 
icon: com.tabtrader.apps.TabTrader.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-04-17
signer: 
reviewArchive: 
twitter: tabtraderpro
social:
- https://www.linkedin.com/company/tabtrader
- https://www.facebook.com/tabtrader
features: 
developerName: TabTrader B.V.

---

{% include copyFromAndroid.html %}
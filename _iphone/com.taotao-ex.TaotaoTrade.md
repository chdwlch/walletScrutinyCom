---
wsId: sbivctrade
title: SBI VCTRADE mobile 暗号資産(仮想通貨)
altTitle: 
authors:
- danny
appId: com.taotao-ex.TaotaoTrade
appCountry: jp
idd: '1461654946'
released: 2019-06-02
updated: 2024-03-08
version: 3.15.2
stars: 1.9
reviews: 338
size: '84567040'
website: https://www.sbivc.co.jp/
repository: 
issue: 
icon: com.taotao-ex.TaotaoTrade.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-02-26
signer: 
reviewArchive: 
twitter: sbivc_official
social:
- https://www.youtube.com/channel/UCvZUMOeEVQWp4ov77mHuDtg
features: 
developerName: TAOTAO INC.

---

{% include copyFromAndroid.html %}
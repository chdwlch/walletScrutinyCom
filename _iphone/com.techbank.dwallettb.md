---
wsId: techbankDWallet
title: TechBank Dwallet
altTitle: 
authors:
- danny
appId: com.techbank.dwallettb
appCountry: in
idd: 1535437806
released: 2020-10-15
updated: 2024-04-23
version: 1.0.61
stars: 5
reviews: 2
size: '120830976'
website: https://techbank.finance
repository: 
issue: 
icon: com.techbank.dwallettb.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-15
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: BEE INTERNATIONAL CONSULTANCY PRIVATE LIMITED

---

{% include copyFromAndroid.html %}

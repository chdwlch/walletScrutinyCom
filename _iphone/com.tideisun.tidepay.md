---
wsId: iSunOne
title: 'iSunOne: USDC Card'
altTitle: 
authors:
- danny
appId: com.tideisun.tidepay
appCountry: us
idd: 1384802533
released: 2018-06-09
updated: 2023-12-15
version: 4.0.4
stars: 3.7
reviews: 13
size: '78015488'
website: https://isun1.com
repository: 
issue: 
icon: com.tideisun.tidepay.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-10
signer: 
reviewArchive: 
twitter: isunone1
social:
- https://www.linkedin.com/company/isunone
- https://www.facebook.com/iSunOne
features: 
developerName: TIDENET LIMITED

---

{% include copyFromAndroid.html %}

---
wsId: TradeStation
title: TradeStation - Trade & Invest
altTitle: 
authors:
- danny
appId: com.tradestation.MobileTrading
appCountry: us
idd: 581548081
released: 2012-12-10
updated: 2024-04-23
version: 7.10.4
stars: 4.5
reviews: 17365
size: '125144064'
website: http://www.tradestation.com/trading-technology/tradestation-mobile
repository: 
issue: 
icon: com.tradestation.MobileTrading.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive: 
twitter: tradestation
social:
- https://www.facebook.com/TradeStation
features: 
developerName: TradeStation Technologies

---

{% include copyFromAndroid.html %}

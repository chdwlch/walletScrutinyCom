---
wsId: xBankPocket
title: PumpX -All-In-One Web3 Portal
altTitle: 
authors:
- danny
appId: com.tratao.xpocket
appCountry: us
idd: '1481024258'
released: 2019-11-08
updated: 2024-01-23
version: 4.7.0
stars: 4
reviews: 170
size: '92255232'
website: https://xbank.plus/
repository: 
issue: 
icon: com.tratao.xpocket.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-13
signer: 
reviewArchive: 
twitter: xBank_Official
social:
- https://discord.com/invite/PGBVwcaeQE
- https://www.linkedin.com/company/xbank-global
- https://medium.com/@xBankCrypto
features: 
developerName: INITIAL BLOCKCHAIN TECH PTE. LTD.

---

{% include copyFromAndroid.html %}
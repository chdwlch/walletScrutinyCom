---
wsId: Vidulum
title: Vidulum
altTitle: 
authors:
- leo
appId: com.vidulum.app
appCountry: 
idd: 1505859171
released: 2020-07-28
updated: 2024-02-07
version: 1.6.1
stars: 3.9
reviews: 15
size: '71109632'
website: https://vidulum.app
repository: 
issue: 
icon: com.vidulum.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: VidulumApp
social:
- https://www.facebook.com/VidulumTeam
- https://www.reddit.com/r/VidulumOfficial
features: 
developerName: Vidulum LLC

---

{% include copyFromAndroid.html %}

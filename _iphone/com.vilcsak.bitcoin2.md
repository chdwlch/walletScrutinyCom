---
wsId: coinbaseBSB
title: 'Coinbase: Buy Bitcoin & Ether'
altTitle: 
authors:
- leo
appId: com.vilcsak.bitcoin2
appCountry: 
idd: 886427730
released: 2014-06-22
updated: 2024-04-23
version: 12.15.6
stars: 4.7
reviews: 1732738
size: '217805824'
website: http://www.coinbase.com
repository: 
issue: 
icon: com.vilcsak.bitcoin2.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-12
signer: 
reviewArchive: 
twitter: coinbase
social:
- https://www.facebook.com/coinbase
features: 
developerName: Coinbase, Inc.

---

{% include copyFromAndroid.html %}

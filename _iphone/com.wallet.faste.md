---
wsId: fastEWallet
title: FastE Wallet
altTitle: 
authors:
- danny
appId: com.wallet.faste
appCountry: us
idd: '1643098777'
released: 2022-09-06
updated: 2024-01-18
version: '1.4'
stars: 0
reviews: 0
size: '64476160'
website: 
repository: 
issue: 
icon: com.wallet.faste.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2024-01-19
signer: 
reviewArchive: 
twitter: blocktechbrew
social:
- https://www.instagram.com/blocktechbrew
features: 
developerName: Code Brew Labs

---

{% include copyFromAndroid.html %}
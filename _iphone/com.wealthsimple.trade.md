---
wsId: WealthsimpleTrade
title: Wealthsimple - Grow your money
altTitle: 
authors:
- danny
appId: com.wealthsimple.trade
appCountry: ca
idd: 1403491709
released: 2019-02-26
updated: 2024-04-22
version: 2.133.0
stars: 4.6
reviews: 124599
size: '174898176'
website: https://www.wealthsimple.com/en-ca/
repository: 
issue: 
icon: com.wealthsimple.trade.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-09-03
signer: 
reviewArchive: 
twitter: Wealthsimple
social:
- https://www.facebook.com/wealthsimple
features: 
developerName: Wealthsimple

---

{% include copyFromAndroid.html %}

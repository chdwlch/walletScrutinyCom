---
wsId: wirex
title: 'Wirex: All-In-One Trading App'
altTitle: 
authors:
- danny
appId: com.wirex
appCountry: us
idd: 1090004654
released: 2016-03-22
updated: 2024-04-01
version: 3.48.03
stars: 3.7
reviews: 693
size: '215274496'
website: https://wirexapp.com/
repository: 
issue: 
icon: com.wirex.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-01-10
signer: 
reviewArchive: 
twitter: wirexapp
social:
- https://www.linkedin.com/company/wirex-limited
- https://www.facebook.com/wirexapp
features: 
developerName: Wirex Limited

---

{% include copyFromAndroid.html %}

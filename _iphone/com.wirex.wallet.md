---
wsId: wirexwalletdefi
title: 'COCA: Crypto and DeFi'
altTitle: 
authors:
- danny
appId: com.wirex.wallet
appCountry: nz
idd: 1594165139
released: 2021-12-07
updated: 2024-03-25
version: 0.5.11
stars: 4
reviews: 9
size: '86567936'
website: https://www.coca.xyz/
repository: 
issue: 
icon: com.wirex.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-10
signer: 
reviewArchive: 
twitter: wirexapp
social:
- https://github.com/wirexapp
features: 
developerName: CCA LABS - FZCO

---

{% include copyFromAndroid.html %}

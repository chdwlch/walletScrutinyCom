---
wsId: sekiappCrypto
title: SekiApp
altTitle: 
authors:
- danny
appId: com.wiseki.sekiapp.new
appCountry: ng
idd: '1622624126'
released: 2022-06-22
updated: 2024-04-05
version: 2.1.2
stars: 4.3
reviews: 29
size: '110120960'
website: 
repository: 
issue: 
icon: com.wiseki.sekiapp.new.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-14
signer: 
reviewArchive: 
twitter: Seki_APP
social:
- https://www.linkedin.com/company/sekiapp
- https://www.youtube.com/channel/UCHxxbmOdcfyAmXUfutFfBXA
- https://www.instagram.com/seki_app
- https://www.facebook.com/sekiapp
- https://www.tiktok.com/@seki_app
features: 
developerName: Wiseki Technologies Limited

---

{% include copyFromAndroid.html %}
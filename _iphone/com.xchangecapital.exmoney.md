---
wsId: exMoney
title: EX-money
altTitle: 
authors:
- danny
appId: com.xchangecapital.exmoney
appCountry: ru
idd: '1512922324'
released: 2020-05-14
updated: 2022-12-29
version: 1.0.4
stars: 5
reviews: 15
size: '79970304'
website: 
repository: 
issue: 
icon: com.xchangecapital.exmoney.jpg
bugbounty: 
meta: stale
verdict: nowallet
date: 2023-12-29
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: XChangeCapitalGroup

---

{% include copyFromAndroid.html %}
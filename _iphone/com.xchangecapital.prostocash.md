---
wsId: prostocash
title: Prostocash - buy Биткоин
altTitle: 
authors:
- danny
appId: com.xchangecapital.prostocash
appCountry: ru
idd: '1477838562'
released: 2019-09-09
updated: 2022-12-22
version: 2.0.3
stars: 3.7
reviews: 30
size: '70281216'
website: https://prostocash.com
repository: 
issue: 
icon: com.xchangecapital.prostocash.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-12-18
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: XChangeCapitalGroup

---

{% include copyFromAndroid.html %}

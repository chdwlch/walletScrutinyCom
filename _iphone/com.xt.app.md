---
wsId: xtcom
title: 'XT.com: Buy Bitcoin & Ethereum'
altTitle: 
authors:
- danny
appId: com.xt.app
appCountry: us
idd: '1556596708'
released: 2021-03-08
updated: 2024-04-25
version: 4.68.0
stars: 4.2
reviews: 591
size: '245044224'
website: https://www.xt.com
repository: 
issue: 
icon: com.xt.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-02-03
signer: 
reviewArchive: 
twitter: XTexchange
social:
- https://www.linkedin.com/company/xt-com-exchange/
- https://www.facebook.com/XT.comexchange
features: 
developerName: XT LTD, LLC

---

{% include copyFromAndroid.html %}

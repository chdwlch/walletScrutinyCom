---
wsId: yibiExchange
title: YIBI
altTitle: 
authors:
- danny
appId: com.yibi
appCountry: us
idd: '1638288204'
released: 2022-08-12
updated: 2024-04-12
version: 1.1.8
stars: 3.7
reviews: 3
size: '179464192'
website: 
repository: 
issue: 
icon: com.yibi.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2024-04-14
signer: 
reviewArchive: 
twitter: OfficialYibi
social:
- https://yibi.co
- https://t.me/yibioffical00
- https://officialyibi.medium.com
- https://www.reddit.com/r/yibiOffical
- https://www.youtube.com/@YibiOfficial/video
features: 
developerName: YIBI LTD

---

{% include copyFromAndroid.html %}

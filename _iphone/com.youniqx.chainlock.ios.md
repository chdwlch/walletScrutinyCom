---
wsId: chainlockApp
title: Chainlock
altTitle: 
authors:
- danny
appId: com.youniqx.chainlock.ios
appCountry: us
idd: '1442543217'
released: 2018-11-22
updated: 2023-08-08
version: 1.47.0
stars: 5
reviews: 2
size: '35381248'
website: https://www.chainlock.com
repository: 
issue: 
icon: com.youniqx.chainlock.ios.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-11-02
signer: 
reviewArchive: 
twitter: youniqx
social:
- https://www.facebook.com/YOUNIQXbyOeSD
- https://www.linkedin.com/company/youniqx-identity-ag
features: 
developerName: youniqx Identity AG

---

{% include copyFromAndroid.html %}

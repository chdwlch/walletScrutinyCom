---
wsId: zedPayExchange
title: Zed-Pay
altTitle: 
authors:
- danny
appId: com.zed-pay.app.ios
appCountry: us
idd: '6444008269'
released: 2022-11-07
updated: 2024-01-28
version: 1.4.0
stars: 5
reviews: 2
size: '36731904'
website: 
repository: 
issue: 
icon: com.zed-pay.app.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-16
signer: 
reviewArchive: 
twitter: ZedPayCo
social:
- https://www.linkedin.com/company/zedpaycompany
- https://www.facebook.com/zedpayco
- https://www.instagram.com/zedpay.co
features: 
developerName: ZEDPAY

---

{% include copyFromAndroid.html %}
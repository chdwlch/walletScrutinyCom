---
wsId: zumoPay
title: 'Zumo: Buy Bitcoin and Ether'
altTitle: 
authors:
- danny
appId: com.zumopay.core
appCountry: gb
idd: '1449986847'
released: 2019-11-07
updated: 2024-04-10
version: 5.8.1
stars: 4.3
reviews: 293
size: '83654656'
website: https://app.zumo.tech
repository: 
issue: 
icon: com.zumopay.core.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-12-15
signer: 
reviewArchive: 
twitter: zumopay
social:
- https://www.linkedin.com/company/zumomoney
- https://www.facebook.com/zumo.money
features: 
developerName: Blockstar Developments Limited

---

{% include copyFromAndroid.html %}
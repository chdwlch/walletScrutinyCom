---
wsId: BitcoinWalletBitBucks
title: Bitcoin Wallet BitBucks
altTitle: 
authors:
- leo
appId: de.fuf.bitbucks-io
appCountry: 
idd: 1453167599
released: 2019-06-15
updated: 2023-12-13
version: 1.5.4
stars: 1
reviews: 1
size: '26564608'
website: https://www.bitbucks.io
repository: 
issue: 
icon: de.fuf.bitbucks-io.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-12-13
signer: 
reviewArchive: 
twitter: bit_bucks
social:
- https://www.facebook.com/bitbucks.io
features: 
developerName: FUF // Frank und Freunde

---

According to their website:

> **Pay safely and securely**<br>
  Your Bitcoin is multi-signature protected and will be securely stored in the
  safest wallets. Even if you lose your mobile phone, you will not lose your
  credit.

  This is a custodial app and thus **not verifiable**

---
wsId: börseFinanzen
title: Börse & Aktien - finanzen.net
altTitle: 
authors:
- danny
appId: de.smarthouse.finanzen
appCountry: us
idd: '291973577'
released: 2008-11-21
updated: 2024-04-15
version: 7.6.9
stars: 4.7
reviews: 258
size: '210050048'
website: http://www.finanzen.net/apps
repository: 
issue: 
icon: de.smarthouse.finanzen.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2022-06-23
signer: 
reviewArchive: 
twitter: FinanzenNet
social:
- https://www.facebook.com/finanzen.net
- https://www.youtube.com/channel/UC0SfuDptovS05L3JjXSHjBg
features: 
developerName: finanzen.net GmbH

---

{% include copyFromAndroid.html %}

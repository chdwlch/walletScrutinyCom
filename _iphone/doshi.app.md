---
wsId: doshiWallet
title: Doshi Wallet
altTitle: 
authors:
- danny
appId: doshi.app
appCountry: us
idd: '1613531073'
released: 2022-09-01
updated: 2023-05-31
version: 1.0.130
stars: 3.6
reviews: 16
size: '86651904'
website: https://doshi.app
repository: 
issue: 
icon: doshi.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-17
signer: 
reviewArchive: 
twitter: doshi_wallet
social:
- https://discord.com/invite/xQkdtmZm5V
features: 
developerName: Doshi App Limited

---

{% include copyFromAndroid.html %}

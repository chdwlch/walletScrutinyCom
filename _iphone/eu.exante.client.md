---
wsId: ExanteTrading
title: EXANTE Trading
altTitle: 
authors:
- danny
appId: eu.exante.client
appCountry: am
idd: 892856882
released: 2014-07-06
updated: 2024-04-09
version: '4.56'
stars: 0
reviews: 0
size: '59564032'
website: https://exante.eu/
repository: 
issue: 
icon: eu.exante.client.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-10-16
signer: 
reviewArchive: 
twitter: EXANTE_EU
social:
- https://www.linkedin.com/company/exante-ltd
- https://www.facebook.com/exante.global
features: 
developerName: XNT Ltd

---

{% include copyFromAndroid.html %}


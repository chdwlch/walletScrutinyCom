---
wsId: ExodusCryptoBitcoinWallet
title: 'Exodus: Crypto Bitcoin Wallet'
altTitle: 
authors:
- leo
appId: exodus-movement.exodus
appCountry: 
idd: 1414384820
released: 2019-03-23
updated: 2024-04-22
version: 24.17.6
stars: 4.6
reviews: 21654
size: '63803392'
website: https://exodus.com/mobile
repository: 
issue: 
icon: exodus-movement.exodus.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-01-23
signer: 
reviewArchive: 
twitter: exodus_io
social:
- https://www.facebook.com/exodus.io
features: 
developerName: Exodus Movement, Inc.

---

Just like {% include walletLink.html wallet='android/exodusmovement.exodus' %} on Android, this app is
closed source and thus **not verifiable**.

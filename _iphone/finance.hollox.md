---
wsId: holloxWallet
title: Hollox
altTitle: 
authors:
- danny
appId: finance.hollox
appCountry: us
idd: '6444396340'
released: 2022-11-21
updated: 2022-11-22
version: '1.0'
stars: 4.2
reviews: 28
size: '42294272'
website: 
repository: 
issue: 
icon: finance.hollox.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-11-25
signer: 
reviewArchive: 
twitter: Holloxintl
social:
- https://hollox.finance/
- https://www.facebook.com/holloxintl
- https://www.instagram.com/holloxintl
features: 
developerName: XOXO TECHNOLOGIES LIMITED

---

{% include copyFromAndroid.html %}
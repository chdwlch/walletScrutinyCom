---
wsId: fizenWallet
title: Fizen Super App
altTitle: 
authors:
- danny
appId: fizen.io.wallet
appCountry: us
idd: '1621269508'
released: 2022-04-28
updated: 2024-04-25
version: 2.2.0
stars: 5
reviews: 1
size: '138731520'
website: https://fizen.io
repository: 
issue: 
icon: fizen.io.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-03-31
signer: 
reviewArchive: 
twitter: fizenwallet
social:
- https://www.linkedin.com/company/fizen-io/
- https://www.facebook.com/web3fizen
- https://www.instagram.com/fizen.io/
- https://t.me/fizen_io
features: 
developerName: Fizen GmbH

---

{% include copyFromAndroid.html %}

---
wsId: kunaio
title: Kuna.io — buy sell crypto
altTitle: 
authors:
- danny
appId: icecream.group.kuna.fintech
appCountry: us
idd: 1457062155
released: 2019-03-27
updated: 2023-06-10
version: 5.4.8
stars: 4.5
reviews: 55
size: '162667520'
website: https://kuna.io
repository: 
issue: 
icon: icecream.group.kuna.fintech.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-08-04
signer: 
reviewArchive: 
twitter: KunaExchange
social:
- https://www.facebook.com/kunaexchange
features: 
developerName: Kuna Fintech Limited

---

{% include copyFromAndroid.html %}

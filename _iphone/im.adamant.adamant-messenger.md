---
wsId: adamanyMessenger
title: ADAMANT Messenger
altTitle: 
authors:
- danny
appId: im.adamant.adamant-messenger
appCountry: us
idd: '1341473829'
released: 2018-08-04
updated: 2024-03-13
version: 3.6.0
stars: 4.9
reviews: 19
size: '48064512'
website: https://adamant.im
repository: 
issue: 
icon: im.adamant.adamant-messenger.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-04-15
signer: 
reviewArchive: 
twitter: adamant_im
social:
- https://vk.com/adamant_im
- https://t.me/adamant_eng
- https://www.youtube.com/c/ADAMANTMessenger
features: 
developerName: ADAMANT TECH LABS LP

---

{% include copyFromAndroid.html %}


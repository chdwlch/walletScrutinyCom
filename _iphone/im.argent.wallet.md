---
wsId: argent
title: Argent — Starknet Wallet
altTitle: 
authors:
- danny
appId: im.argent.wallet
appCountry: us
idd: 1358741926
released: 2018-10-25
updated: 2024-04-15
version: 4.26.0
stars: 4.5
reviews: 2186
size: '137820160'
website: https://www.argent.xyz
repository: 
issue: 
icon: im.argent.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-01-10
signer: 
reviewArchive: 
twitter: argentHQ
social: 
features: 
developerName: Argent

---

{% include copyFromAndroid.html %}
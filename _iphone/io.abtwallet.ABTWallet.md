---
wsId: dIDWallet
title: DID Wallet
altTitle: 
authors:
- danny
appId: io.abtwallet.ABTWallet
appCountry: us
idd: '1460083542'
released: 2019-05-19
updated: 2024-04-22
version: 5.2.0
stars: 4.9
reviews: 54
size: '102592512'
website: https://www.didwallet.io
repository: 
issue: 
icon: io.abtwallet.ABTWallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-07-06
signer: 
reviewArchive: 
twitter: ArcBlock_io
social:
- https://t.me/ArcBlock
features: 
developerName: ArcBlock, Inc.

---

{% include copyFromAndroid.html %}

---
wsId: aquaWallet
title: AQUA Wallet
altTitle: 
authors:
- danny
appId: io.aquawallet.ios
appCountry: us
idd: '6468594241'
released: 2024-01-03
updated: 2024-04-24
version: 0.1.51
stars: 4.6
reviews: 25
size: '90558464'
website: https://aquawallet.io
repository: https://github.com/AquaWallet/aqua-wallet
issue: 
icon: io.aquawallet.ios.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2024-01-05
signer: 
reviewArchive: 
twitter: AquaBitcoin
social:
- https://www.facebook.com/profile.php?id=100095180887605
- https://www.instagram.com/aquabitcoin
- https://www.linkedin.com/products/jan3-aqua
features: 
developerName: Jan3 LLC

---

{% include copyFromAndroid.html %}

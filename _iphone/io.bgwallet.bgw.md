---
wsId: bgWallet
title: WalletGo
altTitle: 
authors:
- danny
appId: io.bgwallet.bgw
appCountry: us
idd: '1628519503'
released: 2022-10-31
updated: 2024-04-24
version: 0.3.4
stars: 0
reviews: 0
size: '74785792'
website: https://walletgo.ai/
repository: 
issue: 
icon: io.bgwallet.bgw.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-11-01
signer: 
reviewArchive: 
twitter: BG_Wallet
social:
- https://www.facebook.com/bgwallet
- https://discord.com/channels/1008584420619456584
- https://www.linkedin.com/company/bg-wallet
- https://www.instagram.com/bg.wallet
features: 
developerName: BG Technologies Limited

---

{% include copyFromAndroid.html %}
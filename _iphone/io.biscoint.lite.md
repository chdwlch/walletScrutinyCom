---
wsId: biscoint
title: 'Bitybank: Bitcoin e Crypto'
altTitle: 
authors:
- danny
appId: io.biscoint.lite
appCountry: br
idd: '1588152503'
released: 2022-02-09
updated: 2024-04-17
version: 2.5.5
stars: 4.8
reviews: 3185
size: '85679104'
website: https://bitybank.com.br/
repository: 
issue: 
icon: io.biscoint.lite.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-03-02
signer: 
reviewArchive: 
twitter: BityOficial
social:
- https://www.facebook.com/bitybankoficial
features: 
developerName: Biscoint

---

{% include copyFromAndroid.html %}


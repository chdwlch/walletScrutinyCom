---
wsId: bluewallet
title: BlueWallet - Bitcoin wallet
altTitle: 
authors:
- leo
appId: io.bluewallet.bluewallet
appCountry: 
idd: 1376878040
released: 2018-05-27
updated: 2024-04-14
version: 6.6.2
stars: 4
reviews: 636
size: '49654784'
website: https://bluewallet.io
repository: https://github.com/bluewallet/bluewallet
issue: https://github.com/BlueWallet/BlueWallet/issues/758
icon: io.bluewallet.bluewallet.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2023-12-30
signer: 
reviewArchive:
- date: 2020-07-14
  version: 6.3.2
  appHash: 
  gitRevision: 0f9bcb13a75554cb34a522e07aa2cfeb4048480c
  verdict: custodial
- date: 2020-01-08
  version: 4.9.1
  appHash: 
  gitRevision: 21cb412a4e74b14bd6124c3e3be855d6b96ef589
  verdict: nonverifiable
twitter: bluewalletio
social:
- https://www.reddit.com/r/bluewallet
features:
- ln
developerName: Bluewallet Services, S. R. L.

---

{% include copyFromAndroid.html %}

---
wsId: cryptex24
title: C24 mobile application
altTitle: 
authors:
- danny
appId: io.cryptex24.mobile
appCountry: ua
idd: '1580215134'
released: 2022-07-06
updated: 2022-12-05
version: '1.6'
stars: 0
reviews: 0
size: '16461824'
website: https://www.cryptex24.io
repository: 
issue: 
icon: io.cryptex24.mobile.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-12-01
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: C24 WORLD LTD

---

{% include copyFromAndroid.html %}



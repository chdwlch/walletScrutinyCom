---
wsId: elDoradoWalletExchange
title: El Dorado - Wallet & P2P
altTitle: 
authors:
- danny
appId: io.eldorado.app.v1
appCountry: ve
idd: '1591303547'
released: 2022-04-20
updated: 2024-04-27
version: 1.3.855
stars: 4.9
reviews: 1455
size: '78245888'
website: https://eldorado.io/
repository: 
issue: 
icon: io.eldorado.app.v1.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-18
signer: 
reviewArchive: 
twitter: eldoradoio
social:
- https://www.linkedin.com/company/eldoradoio
- https://www.instagram.com/eldoradoio
- https://t.me/+400YDvBLAXhmOTQx
features: 
developerName: eldorado.io

---

{% include copyFromAndroid.html %}

---
wsId: emcdPoolWallet
title: 'EMCD: Crypto mining pool app'
altTitle: 
authors:
- danny
appId: io.emcd.app
appCountry: us
idd: '1606903304'
released: 2022-01-29
updated: 2024-02-22
version: 1.44.1
stars: 3.6
reviews: 17
size: '163244032'
website: https://emcd.io/about
repository: 
issue: 
icon: io.emcd.app.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2024-04-19
signer: 
reviewArchive: 
twitter: emcd_io
social:
- https://t.me/emcd_community
- https://vk.com/emcd_io
- https://weixin.qq.com/g/Atjew2g9jgbSq6bq
- https://discord.gg/yjk7RVr
features: 
developerName: Emcd Tech Limited

---

{% include copyFromAndroid.html %}

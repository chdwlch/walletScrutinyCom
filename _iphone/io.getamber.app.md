---
wsId: amberAppWallet
title: AmberApp
altTitle: 
authors:
- danny
appId: io.getamber.app
appCountry: us
idd: '1410639317'
released: 2021-06-21
updated: 2023-11-29
version: 5.3.4
stars: 3.7
reviews: 26
size: '115300352'
website: http://amber.app/
repository: 
issue: 
icon: io.getamber.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-17
signer: 
reviewArchive: 
twitter: theamberapp
social:
- https://www.facebook.com/theamberapp
- https://www.instagram.com/amber.app
- https://t.me/theamberapp
features: 
developerName: Amber Labs

---

{% include copyFromAndroid.html %}

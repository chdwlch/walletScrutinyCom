---
wsId: Unstoppable
title: Unstoppable Crypto Wallet
altTitle: 
authors:
- leo
appId: io.horizontalsystems.bank-wallet
appCountry: 
idd: 1447619907
released: 2019-01-10
updated: 2024-04-18
version: '0.38'
stars: 4.7
reviews: 865
size: '102467584'
website: https://unstoppable.money/
repository: https://github.com/horizontalsystems/unstoppable-wallet-ios
issue: 
icon: io.horizontalsystems.bank-wallet.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2020-12-19
signer: 
reviewArchive: 
twitter: unstoppablebyhs
social:
- https://www.reddit.com/r/UNSTOPPABLEWallet
features: 
developerName: Horizontal Systems

---

The provider claims:

> A non-custodial wallet without third party risk.

and we found the source code
[here](https://github.com/horizontalsystems/unstoppable-wallet-ios)
but so far nobody reproduced the build, so the claim is **not verifiable**.

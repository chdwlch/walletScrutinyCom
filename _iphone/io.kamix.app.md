---
wsId: kamixApp
title: Kamix
altTitle: 
authors:
- danny
appId: io.kamix.app
appCountry: fr
idd: '1496578894'
released: 2020-06-05
updated: 2023-12-21
version: 2.2.17
stars: 2.8
reviews: 8
size: '60400640'
website: http://kamix.fr
repository: 
issue: 
icon: io.kamix.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-11-17
signer: 
reviewArchive: 
twitter: KamixApp
social:
- https://www.linkedin.com/company/kamixapp
- https://www.facebook.com/Kamixapp
- https://www.instagram.com/kamixapp
features: 
developerName: Kamix

---

{% include copyFromAndroid.html %}

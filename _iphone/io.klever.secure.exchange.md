---
wsId: kleverexchange
title: Bitcoin.me
altTitle: 
authors:
- danny
appId: io.klever.secure.exchange
appCountry: us
idd: 1553486059
released: 2021-09-25
updated: 2023-05-02
version: 1.8.1
stars: 4.6
reviews: 59
size: '132121600'
website: https://bitcoin.me
repository: 
issue: 
icon: io.klever.secure.exchange.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2024-04-28
signer: 
reviewArchive: 
twitter: klever_io
social:
- https://www.linkedin.com/company/klever-app
- https://www.facebook.com/klever.io
features: 
developerName: Klever Exchange

---

{% include copyFromAndroid.html %}

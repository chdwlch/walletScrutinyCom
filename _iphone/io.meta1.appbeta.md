---
wsId: meta1Wallet
title: META 1 Wallet
altTitle: 
authors:
- danny
appId: io.meta1.appbeta
appCountry: us
idd: '1626884236'
released: 2022-07-05
updated: 2023-10-25
version: 2.1.9
stars: 4.5
reviews: 4
size: '25075712'
website: https://meta-exchange.vision/home/
repository: 
issue: 
icon: io.meta1.appbeta.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-09-05
signer: 
reviewArchive: 
twitter: META1Coin
social:
- https://www.facebook.com/meta1coin
- https://www.instagram.com/meta1coin
- https://www.linkedin.com/company/meta-1-coin
features: 
developerName: Meta Vault Association, LLC

---

{% include copyFromAndroid.html %}

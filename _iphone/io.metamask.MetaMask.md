---
wsId: metamask
title: MetaMask - Blockchain Wallet
altTitle: 
authors:
- leo
appId: io.metamask.MetaMask
appCountry: 
idd: 1438144202
released: 2020-09-03
updated: 2024-04-23
version: 7.20.1
stars: 4.7
reviews: 51394
size: '69939200'
website: https://metamask.io/
repository: 
issue: 
icon: io.metamask.MetaMask.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-05-01
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: MetaMask

---

This is an ETH-only app and thus not a Bitcoin wallet.

---
wsId: Moonstake
title: Moonstake Wallet
altTitle: 
authors:
- danny
appId: io.moonstake.wallet
appCountry: us
idd: 1502532651
released: 2020-03-25
updated: 2024-04-25
version: 2.27.1
stars: 3.1
reviews: 17
size: '124588032'
website: http://moonstake.io
repository: 
issue: 
icon: io.moonstake.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: moonstake
social:
- https://www.linkedin.com/company/moonstake
- https://www.facebook.com/moonstakekorea
features: 
developerName: Moonstake Limited

---

{% include copyFromAndroid.html %}
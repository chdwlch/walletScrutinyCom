---
wsId: sideswap
title: SideSwap
altTitle: 
authors:
- danny
appId: io.sideswap.app
appCountry: us
idd: '1556476417'
released: 2021-03-22
updated: 2024-04-05
version: 1.4.10
stars: 4.6
reviews: 10
size: '90358784'
website: 
repository: 
issue: 
icon: io.sideswap.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-08-19
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: SideSwap Limited

---

{% include copyFromAndroid.html %}
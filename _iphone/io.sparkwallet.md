---
wsId: sparkWallet
title: 'Spark Wallet: Earn Rewards'
altTitle: 
authors:
- danny
appId: io.sparkwallet
appCountry: us
idd: '1606418661'
released: 2022-02-23
updated: 2023-12-13
version: '3.2'
stars: 3.7
reviews: 11
size: '31199232'
website: https://sparkwallet.io/
repository: 
issue: 
icon: io.sparkwallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-09-06
signer: 
reviewArchive: 
twitter: sparkwalletapp
social: 
features: 
developerName: Lumasoft, LLC

---

{% include copyFromAndroid.html %}
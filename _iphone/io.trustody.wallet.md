---
wsId: ammerWallet
title: Ammer Wallet
altTitle: 
authors:
- danny
appId: io.trustody.wallet
appCountry: nz
idd: '1599698329'
released: 2022-03-04
updated: 2024-04-18
version: '7.2'
stars: 0
reviews: 0
size: '135654400'
website: https://ammer.cards
repository: 
issue: 
icon: io.trustody.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-20
signer: 
reviewArchive: 
twitter: AmmerCards
social: 
features: 
developerName: Ammer Technologies AG

---

{% include copyFromAndroid.html %}

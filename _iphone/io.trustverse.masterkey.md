---
wsId: masterKey
title: 'MasterKey: Protect your assets'
altTitle: 
authors:
- danny
appId: io.trustverse.masterkey
appCountry: gb
idd: '1559508470'
released: 2021-08-10
updated: 2022-11-28
version: '2.0'
stars: 0
reviews: 0
size: '71185408'
website: https://www.the-masterkey.com
repository: 
issue: 
icon: io.trustverse.masterkey.jpg
bugbounty: 
meta: stale
verdict: nowallet
date: 2023-11-25
signer: 
reviewArchive: 
twitter: SafestMasterKey
social: 
features: 
developerName: trustverse

---

{% include copyFromAndroid.html %}

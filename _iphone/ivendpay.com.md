---
wsId: ivendPayPOS
title: ivendPay POS
altTitle: 
authors:
- danny
appId: ivendpay.com
appCountry: us
idd: '1644875364'
released: 2023-02-02
updated: 2023-11-11
version: 2.0.3
stars: 0
reviews: 0
size: '34193408'
website: https://ivendpay.com
repository: 
issue: 
icon: ivendpay.com.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2024-04-04
signer: 
reviewArchive: 
twitter: ivendpay
social:
- https://www.linkedin.com/company/ivendpay
- https://www.instagram.com/ivendpay_company
- https://www.youtube.com/@ivendpay_company
features: 
developerName: ivendPay

---

{% include copyFromAndroid.html %}
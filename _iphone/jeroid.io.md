---
wsId: Jeroid
title: Jeroid
altTitle: 
authors:
- danny
appId: jeroid.io
appCountry: us
idd: 1539278280
released: 2021-02-27
updated: 2024-02-12
version: 2.0.5
stars: 2.7
reviews: 335
size: '57069568'
website: https://jeroid.ng/
repository: 
issue: 
icon: jeroid.io.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-04
signer: 
reviewArchive: 
twitter: jeroidng
social: 
features: 
developerName: JeroidNG Ltd

---

{% include copyFromAndroid.html %}

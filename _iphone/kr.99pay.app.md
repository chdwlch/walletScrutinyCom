---
wsId: 99Recharge
title: 99pay Mobile recharge
altTitle: 
authors:
- leo
appId: kr.99pay.app
appCountry: kr
idd: 1229582503
released: 2017-05-05
updated: 2024-04-24
version: 5.3.1
stars: 4.7
reviews: 550
size: '81746944'
website: http://www.99pay.kr
repository: 
issue: 
icon: kr.99pay.app.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-12-26
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: QQTRADE Co., Ltd.

---

You probably are looking for the other 99pay app:

{% include walletLink.html wallet='iphone/com.pay99' verdict='true' %}
---
wsId: bither
title: Bither - Bitcoin Wallet
altTitle: 
authors: 
appId: net.bither
appCountry: 
idd: 899478936
released: 2014-07-25
updated: 2023-11-28
version: 2.1.1
stars: 3.2
reviews: 55
size: '13291520'
website: https://bither.net
repository: https://github.com/bither/bither-ios
issue: 
icon: net.bither.jpg
bugbounty: 
meta: ok
verdict: obfuscated
date: 2021-03-05
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: BITHER Limited

---

At the risk of putting it into the wrong **not verifiable** category, we assume
the App Store version is no better and refer to their Android app
{% include walletLink.html wallet='android/net.bither' %} for our analysis.

---
wsId: primalNostr
title: Primal
altTitle: 
authors:
- danny
appId: net.primal.iosapp.Primal
appCountry: us
idd: '1673134518'
released: 2023-12-01
updated: 2024-04-25
version: 1.6.7
stars: 4.9
reviews: 250
size: '30832640'
website: https://primal.net
repository: 
issue: 
icon: net.primal.iosapp.Primal.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2024-03-02
signer: 
reviewArchive: 
twitter: 
social: 
features:
- ln
developerName: Primal Systems Incorporated.

---

{% include copyFromAndroid.html %}
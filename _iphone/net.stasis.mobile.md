---
wsId: STASISStablecoinWallet
title: STASIS Stablecoin Wallet
altTitle: 
authors:
- leo
appId: net.stasis.mobile
appCountry: 
idd: 1371949230
released: 2018-07-06
updated: 2023-07-28
version: '8.1'
stars: 3
reviews: 4
size: '85911552'
website: https://stasis.net
repository: 
issue: 
icon: net.stasis.mobile.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2024-04-03
signer: 
reviewArchive: 
twitter: stasisnet
social:
- https://www.linkedin.com/company/stasisnet
- https://www.facebook.com/stasisnet
features: 
developerName: STSS LTD

---

**Update 2021-05-09**: The review is based on the Play Store app. The App Store
app is currently not available and if that remains, the app will be considered
defunct.

On their website there is no mention of being non-custodial and
certainly there is no source code available. Until we hear opposing claims
we consider it a custodial app and therefore **not verifiable**.

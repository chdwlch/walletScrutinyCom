---
wsId: wxNetwork
title: WX Network
altTitle: 
authors:
- danny
appId: network.wx.app
appCountry: us
idd: '1639310291'
released: 2022-11-02
updated: 2023-06-08
version: 1.10.0
stars: 2.6
reviews: 5
size: '77881344'
website: 
repository: 
issue: 
icon: network.wx.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-06-13
signer: 
reviewArchive: 
twitter: WXNetwork
social:
- https://waves.exchange
- https://www.facebook.com/profile.php?id=100083514754568
- https://medium.com/wavesexchange
- https://t.me/wxnetwork
features: 
developerName: WX DEVELOPMENT LTD.

---

{% include copyFromAndroid.html %}
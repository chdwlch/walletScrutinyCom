---
wsId: knakenCrypto
title: 'Knaken: Buy crypto safely'
altTitle: 
authors: 
appId: nl.knaken.ios
appCountry: nl
idd: '1566004838'
released: 2021-06-10
updated: 2024-04-19
version: 3.16.2
stars: 4.2
reviews: 35
size: '50929664'
website: https://knaken.eu/
repository: 
issue: 
icon: nl.knaken.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-24
signer: 
reviewArchive: 
twitter: knaken_eu
social:
- https://www.facebook.com/knakeneu
- https://www.instagram.com/knaken_eu
features: 
developerName: Knaken Cryptohandel B.V.

---

{% include copyFromAndroid.html %}
---
wsId: coreCryptoWallet
title: Core | Crypto Wallet & NFTs
altTitle: 
authors:
- danny
appId: org.avalabs.corewallet
appCountry: us
idd: '6443685999'
released: 2022-11-28
updated: 2024-04-05
version: 0.14.7
stars: 4.4
reviews: 45
size: '44794880'
website: https://www.core.app/
repository: 
issue: 
icon: org.avalabs.corewallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-02
signer: 
reviewArchive: 
twitter: coreapp
social:
- https://t.me/avalancheavax
- https://discord.com/invite/RwXY7P6
- https://www.youtube.com/avalancheavax
- https://medium.com/@coreapp
- https://www.reddit.com/r/Avax
- https://www.facebook.com/corewallet
features: 
developerName: Ava Labs, Incorporated

---

% include copyFromAndroid.html %}

---
wsId: payBusinessElegro
title: Elegro Business Wallet
altTitle: 
authors:
- danny
appId: paybusiness.elegro.eu
appCountry: us
idd: '1526117414'
released: 2020-08-14
updated: 2024-02-06
version: '3.7'
stars: 0
reviews: 0
size: '70161408'
website: https://business.elegro.eu/elegro-business-wallet
repository: 
issue: 
icon: paybusiness.elegro.eu.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2023-06-12
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Niko Technologies

---

{% include copyFromAndroid.html %}

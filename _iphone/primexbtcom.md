---
wsId: primeXBTCryptoTrading
title: 'PrimeXBT: Trading & Investing'
altTitle: 
authors:
- danny
appId: primexbtcom
appCountry: us
idd: '1522267195'
released: 2021-10-20
updated: 2024-04-17
version: '3.6'
stars: 4.6
reviews: 211
size: '93339648'
website: 
repository: 
issue: 
icon: primexbtcom.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-01
signer: 
reviewArchive: 
twitter: primexbt
social:
- https://primexbt.com
- https://www.facebook.com/primexbt
- https://t.me/PrimeXBT_English
- https://www.reddit.com/r/PrimeXBT
- https://discord.com/invite/yEr8p72pxu
- https://www.youtube.com/channel/UCzH0C03Gy8uHyKr-Y59cwJg
features: 
developerName: Prime XBT Trading Services Ltd.

---

{% include copyFromAndroid.html %}

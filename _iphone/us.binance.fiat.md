---
wsId: BinanceUS
title: 'Binance.US: Buy Bitcoin & ETH'
altTitle: 
authors:
- leo
appId: us.binance.fiat
appCountry: 
idd: 1492670702
released: 2020-01-05
updated: 2024-04-03
version: 3.14.0
stars: 4.2
reviews: 108185
size: '285901824'
website: https://www.binance.us/en/home
repository: 
issue: 
icon: us.binance.fiat.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-01-10
signer: 
reviewArchive: 
twitter: binanceus
social:
- https://www.linkedin.com/company/binance-us
- https://www.facebook.com/BinanceUS
features: 
developerName: BAM Trading Services, Inc.

---

This is the iPhone version of {% include walletLink.html wallet='android/com.binance.us' %} and we
come to the same conclusion for the same reasons. This app is **not verifiable**.

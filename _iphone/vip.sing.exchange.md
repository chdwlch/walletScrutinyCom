---
wsId: starEx
title: StarEx - Buy Bitcoin
altTitle: 
authors:
- danny
appId: vip.sing.exchange
appCountry: us
idd: '1551746664'
released: 2021-03-31
updated: 2024-04-13
version: 3.9.1
stars: 3.4
reviews: 7
size: '240410624'
website: 
repository: 
issue: 
icon: vip.sing.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-11-14
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: STAR EXCHANGE INTERNATIONAL PTE. LTD.

---

{% include copyFromAndroid.html %}
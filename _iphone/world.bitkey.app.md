---
wsId: bitkeyBlock
title: Bitkey - Bitcoin Wallet
altTitle: 
authors:
- danny
appId: world.bitkey.app
appCountry: us
idd: '6476990471'
released: 2024-03-13
updated: 2024-04-24
version: 2024.55.0
stars: 4.7
reviews: 34
size: '74518528'
website: https://bitkey.world
repository: https://github.com/proto-at-block/bitkey
issue: 
icon: world.bitkey.app.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2024-03-14
signer: 
reviewArchive: 
twitter: Bitkeyofficial
social:
- https://www.linkedin.com/company/bitkey-official
- https://www.facebook.com/profile.php?id=100088526238789
- https://www.instagram.com/ownbitkey
features:
- multiSignature
developerName: Block, Inc.

---

{% include copyFromAndroid.html %}
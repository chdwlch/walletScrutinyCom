---
wsId: unbankBitcoin
title: 'Unbank: Buy & Sell Bitcoin'
altTitle: 
authors:
- danny
appId: world.unbank.unbankmobileapp
appCountry: us
idd: '1587374229'
released: 2022-05-03
updated: 2024-04-05
version: 2.4.0
stars: 3.7
reviews: 26
size: '88893440'
website: https://www.unbank.com/
repository: 
issue: 
icon: world.unbank.unbankmobileapp.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-08-30
signer: 
reviewArchive: 
twitter: unbankworld
social:
- https://www.facebook.com/unbankworld
- https://www.instagram.com/unbankworld
features: 
developerName: KALBAS INC

---

{% include copyFromAndroid.html %}

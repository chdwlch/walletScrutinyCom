---
wsId: Luno
title: Luno Bitcoin & Cryptocurrency
altTitle: 
authors:
- leo
appId: za.co.Bitx
appCountry: 
idd: 927362479
released: 2014-11-03
updated: 2024-04-22
version: 8.57.1
stars: 4.2
reviews: 3527
size: '156461056'
website: https://www.luno.com
repository: 
issue: 
icon: za.co.Bitx.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-12
signer: 
reviewArchive: 
twitter: LunoGlobal
social:
- https://www.linkedin.com/company/lunoglobal
- https://www.facebook.com/luno
features: 
developerName: Luno Pte Ltd

---

This app's Android version had a clear statement about being custodial in the
Play Store description but on the App Store, no claims are made that let you
infer the type of custody.

On the website under [security](https://www.luno.com/en/security) we found the
same claim as in the Android review though:

> **Deep freeze storage**<br>
  The majority of customer Bitcoin funds are kept in what we call “deep freeze”
  storage. These are multi-signature wallets, with private keys stored in
  different bank vaults.

which again is a clear statement of them holding your funds. As a custodial
service, this app is **not verifiable**.

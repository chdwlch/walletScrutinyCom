---
title: Pey PoS
appId: pey.pos
authors:
- danny
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions:
weight: 
provider: Ricardo Ferrer Rivero
providerWebsite: https://pey.de
website: 
shop: 
country: 
price: 
repository: 
issue: 
icon: pey.pos.png
bugbounty: 
meta: ok
verdict: vapor
date: 2024-01-15
signer: 
reviewArchive: 
twitter: PEY
social:
- https://www.facebook.com/PEYapp

---

## Background 

The Pey point of sale device was publicized on [January 2015](https://3dprint.com/36781/bitcoin-3d-prints-pos-terminal/).

There hasn't been any updates about the project since then. 

> The current version of the terminal is based on a Nexus S smartphone embedded into a 3D-printed case that features a Bluetooth Low Energy (BLE) beacon provided by Estimote as well as a modified NFC antenna.

The device is based on an old Nexus-S smartphone.

The modified smartphone allowed merchants to accept cryptocurrencies as payment. We could no longer find the specific app for this.

The last tweet on twitter was made in July 2020. Today, the website still exists, advertising "Blockchain solutions" with no links, shop, or any mention of the point-of-sale solution. This is **vaporware.**

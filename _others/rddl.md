---
title: RDDL
appId: rddl
authors:
- danny
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions: 
weight: 
provider: Tom Fuerstner
providerWebsite: 
website: https://www.rddl.io/
shop: 
country: 
price: 
repository: 
issue: 
icon: rddl.png
bugbounty: 
meta: ok
verdict: unreleased
date: 2024-01-05
signer: 
reviewArchive: 
twitter: RDDLNetwork
social:
- https://www.linkedin.com/company/rddl
- https://discord.com/invite/uy4CA2Xw54

---

# Device Description

> All machine identities can be defined via simple DID (decentralized identifiers), but wherever possible they are defined by TA (trust anchors), dedicated hardware components consisting of combination of three functions - a hardware wallet, a data logger and where necessary an energy meter. A machine’s data and metadata are represented as CID (content identifier)

[Source](https://www.linkedin.com/pulse/rddl-network-introduction-thomas-fuerstner/)

>
> What is the RDDL Network
>
> A revolutionary blockchain-based protocol that offers machines unique, incorruptible identities, enabling them to communicate and transact securely and autonomously.
>
> The RDDL Network interconnects machines & extends them into a new Decentralized Physical Infrastructure Network (DePIN)
>
> With its unique Proof of Productivity (PoP) consensus mechanism, the RDDL Network provides a solution that solves the oracle problem.

## Analysis 

The device is **not yet commercially available**.
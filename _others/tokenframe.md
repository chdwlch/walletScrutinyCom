---
title: Token Frame
appId: tokenFrame
authors:
- danny
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions: 
weight: 
provider: Token Frame, LLC
providerWebsite: https://tokenframe.com/
website: 
shop: https://tokenframe.com/cart
country: US
price: 
repository: 
issue: 
icon: tokenframe.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-12-06
signer: 
reviewArchive: 
twitter: tokenframeNFT
social:
- https://www.instagram.com/tokenframe.eth 

---

## Product Description 

  **Connect your wallet**
  >
  > Supporting 50+ Wallets
  > 
  > Sign in with your Metamask, Coinbase Wallet, Ledger, WalletConnect (50+ wallets), or Phantom wallet for a uniquely authentic experience. Currently supporting Eth, Polygon and Solana with Tezos and more on the way.  

  > **Patented Web3 Technology**
  >
  > Tokenframe runs on a patented IoT system fully integrated with web3 technology. Control all of your Tokenframes from a single dashboard.
  > 
  > Rotate the Tokenframe™ 90° and the artwork displayed on it will automatically switch orientation, just like your smartphone. The rotating wall mount is included with every mountable Tokenframe™. Experience true quality with stereo speakers, aux port, and robust hardware.

The product comes in different sizes. 

**It does not come with a wallet.**
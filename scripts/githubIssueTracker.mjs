/**
 * This script lists all open issues of products that are "meta: ok" and had no
 * update in more than three months. Issues are sorted oldest to newest.
 * This script can be started with
 * $ node scripts/githubIssueTracker.mjs $GITHUB_API_KEY
 **/

import fs from 'fs';
import axios from 'axios';
import path from 'path';

// Define the folder paths to search for .md files
const folderPaths = ['./_android', './_iphone', './_bearer', './_hardware'];

// Regular expression pattern to match issue URLs
const issuePattern = /issue:\s+(https:\/\/github\.com\/[^/]+\/[^/]+\/issues\/(\d+))/g;

const metaOkPattern = /meta: ok/;

// List to store extracted project names, issue numbers, and file names
const issueInfo = [];

// Function to search for .md files and extract issue information
function extractIssueInfo(filePath) {
  const content = fs.readFileSync(filePath, 'utf-8');
  if (!metaOkPattern.test(content)) {
    return
  }
  const matches = content.matchAll(issuePattern);
  for (const match of matches) {
    const [issueUrl, issueNumber] = match;
    const [, projectOwner, projectName] = issueUrl.match(/github\.com\/([^/]+)\/([^/]+)\/issues/); // Extract project name
    issueInfo.push({ projectOwner, projectName, issueUrl: issueUrl.replace('issue: ', ''), issueNumber: issueNumber.split('/').pop(), fileName: path.resolve(filePath) });
  }
}

// Function to check if a GitHub issue is active and get its last update date
async function checkGitHubIssue(projectOwner, projectName, issueNumber, githubAccessToken) {
  const url = `https://api.github.com/repos/${projectOwner}/${projectName}/issues/${issueNumber}`;
  const headers = {
    Authorization: `token ${githubAccessToken}`,
  };

  try {
    const response = await axios.get(url, { headers });
    const issueData = response.data;
    const issueState = issueData.state || 'unknown';
    const lastUpdateDate = issueData.updated_at.split('T')[0]; // Extract and format last update date
    return { state: issueState, lastUpdateDate };
  } catch (error) {
    console.error(`Error checking https://github.com/${projectOwner}/${projectName}/issues/${issueNumber} : ${error.message}`);
    return { state: 'error', lastUpdateDate: 'unknown' };
  }
}

// Prepare the output
let output = [];

// Check the status of each GitHub issue and append to the output text
(async () => {
  const githubAccessToken = process.argv[2];
  if (githubAccessToken === undefined) {
    console.error('Provide your GitHub Personal Access Token (https://github.com/settings/tokens) as parameter!');
    process.exit(1);
  }

  // Loop through each folder path and search for .md files
  folderPaths.forEach((folderPath) => {
    fs.readdirSync(folderPath).forEach((file) => {
      if (file.endsWith('.md')) {
        extractIssueInfo(`${folderPath}/${file}`);
      }
    });
  });
  
  const threeMonthsAgo = new Date();
  threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3);
  for (const { projectOwner, projectName, issueUrl, issueNumber, fileName } of issueInfo) {
    const { state, lastUpdateDate } = await checkGitHubIssue(projectOwner, projectName, issueNumber, githubAccessToken);
    const csvLine = `${fileName},${state},${projectOwner}/${projectName},${lastUpdateDate},${issueUrl}\n`;
    if (new Date(lastUpdateDate) < threeMonthsAgo) {
      output.push({
        update: new Date(lastUpdateDate),
        filename: fileName,
        issue: issueUrl,
        state: state,
      })
    }
  }
  output.sort((a, b) => b.update - a.update);
  output.forEach((o) => {
    const daysSince = Math.floor((new Date() - o.update) / 1000 / 60 / 60 / 24)
    console.log(`${daysSince} days ago: file://${o.filename} ${o.issue} ${o.state}`)
  })
})();
